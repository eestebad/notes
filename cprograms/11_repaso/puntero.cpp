#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[]) {

    char nombre [30]; //una cadena
    char *pc;  //un puntero a char

    strcpy (nombre, "Pedro");

    pc=nombre;  //el puntero apunta a la cadena que está en nombre

    printf (" %s\n", nombre); //imprime Pedro
    printf (" %s\n", pc); //Imprime Pedro también

    return EXIT_SUCCESS;
}
