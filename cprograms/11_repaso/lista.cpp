#include <stdio.h>
#include <stdlib.h>

const char *lista[]= {"Hola", "Buenas noches", NULL};

int main () {

    char const **p= lista;
    char frase[18];
    int celdas = sizeof(lista)/sizeof(char*);

    printf ("Introduzca una palabra: \n");
    scanf (" %s", frase); //si ponemos [^\n] para que te lea una frase

    printf ("Su palabra es: %s\n", frase);

    printf ("El nº de bytes del array lista es: %lu bytes\n", sizeof(lista)); //Imprimir la cantidad de bytes que ocupa lista.

    // Calcular e imprimir la cantidad de celdas de lista.
    printf ("El nº de celdas del array lista es: %i\n", celdas);

    /*  Sabiendo la cantidad de celdas que tiene lista, imprimir todas las palabras que tenga cada celda*/
    for (int i = 0; i < celdas; i++)
        printf ("Este es el contenido del array lista: %s\n", lista[i]);

    //Imprimimos las dos cadenas una a continuación de otra.
    printf (" %s", lista[0]);
    printf (" %s\n", lista[1]);

    //Poniendo un valor centinela como última palabra y usando un puntero y un while, imprimimos todas las palabras del array lista.
    while (*p != NULL) {
        printf (" %s\n", *p);
        p++;
    }
        return EXIT_SUCCESS;
    }
