#ifndef __QUEUE_H__
#define __QUEUE_H__

#define M 0x15

struct TQueue {
    int data[M];
    int head;      // First occupied position.
    int summit;    // First empty cell.
    int failed;    // State variable indicating las op status.
};

#ifdef __cplusplus
extern "C"
{
#endif
    void init (struct TQueue *s);
    void push (struct TQueue *s, int ndata);
    int shift (struct TQueue *s); // En lugar de pop. Shift sacará el elemento por la cabeza de la cola y lo devolverá al final.
#ifdef __cplusplus
}
#endif

#endif
