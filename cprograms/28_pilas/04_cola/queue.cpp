#include "queue.h"

/* FUNCIONES DE LA MÁQUINA DE DATOS */
void init (struct TQueue *s) {
    s->head   = 0; // 1ª posición ocupada.
    s->summit = 0; // 1ª celda libre.
    s->failed = 0; // Variable de estado.
}

void push (struct TQueue *s, int ndata) {
    s->failed = 0;
    if (s->summit >= M) {
        s->failed = 1;
        return;
    }
    s->data[s->summit++] = ndata;
}

int shift (struct TQueue *s) {
    s->failed = 0;
    if (s->head >= s->summit) { // Si la cabeza es mayor o igual que la cima, no hay elementos en la cola.
        s->failed = 1;
        return -666;
    }
    return s->data[s->head++];
}
