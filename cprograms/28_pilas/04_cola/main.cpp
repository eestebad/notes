#include <stdlib.h>
#include <time.h>

#include "queue.h"
#include "interface.h"

//Programa que mete en la cola números aletorios, te dice qué nº es la cabecera y cuando pulsas una tecla, lo saca de la cola. Después mete un 7. En las colas se inserta por el final y se sacan los elementos por el principio.

int main (int argc, char *argv[]) {
    struct TQueue queue;
    int lim = M < 5 ? M : 5; // Variable para imprimir en el for hasta lim y que no llene la cola entera.

    init (&queue);

    srand (time (NULL));

    for (int i=0; i<lim; i++) { //Mete números en la cola mientras i sea menor que lim
        push (&queue, rand () % 10 - 3);
        if (queue.failed)
            show_error ("Stack push operation failed.");
    }

    show_queue (queue);

    printf ("\t=> %i\n", shift (&queue));
    if (queue.failed)
        show_error ("Stack pop operation failed.");

    printf ("Pulse una tecla para continuar.");
    getchar ();

    show_queue (queue);

    printf ("Pulse una tecla para continuar.");
    getchar (); // Para que dé tiempo a verlo.

    push (&queue, 7); // Empuja un 7 a la cola, como últimoelemento.
    show_queue (queue);

    printf ("Pulse una tecla para continuar.");
    getchar ();

    return EXIT_SUCCESS;
}
