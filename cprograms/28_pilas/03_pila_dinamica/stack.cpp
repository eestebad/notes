#include "stack.h"

#include <stdlib.h> //Incluye malloc.

/* Pila dinámica, una pila si la limitación de M*/

#define BLCKSZ 5U //Reserva de memoria para el malloc

void allocate (struct TStack *s) { //Función para manejar la gestión de la memoria. Recibe pila por referencia para modificarla
    s->data = (int *) realloc (s->data, // Vamos a hacer un realloc porque en init haría malloc y en push realloc y realloc con NULL funciona como malloc.
            (s->size + BLCKSZ) * sizeof(int) ); //Ampliamos el realloc por elementos con BLCKSZ.
    s->size += BLCKSZ;//Después decimos que size es igual a lo que vale size, más el nuevo elemento.

}

/* FUNCIONES DE LA MÁQUINA DE DATOS */
void init (struct TStack *s) {
    s->data   = NULL; //PUntero NULL para el realloc
    allocate (s);
    s->summit = 0;
    s->failed = 0;
    s->size   = 0;
    allocate (s);
}

void destroy (const struct TStack *s) {//Esta función libera la memoria reservada en el malloc. Recibe por referencia porque es más rápido y es const para que no se pueda cambiar
    free (s->data);
}

void push (struct TStack *s, int ndata) {
    s->failed = 0;
    while (s->summit >= s->size) // summit y size deben ser el mismo tipo de dato. Si summit es mayor = que size aloja otro bloque de memoria.
        allocate (s);//Aloja otro bloque de memoria.

    s->data[s->summit++] = ndata;
}

int pop (struct TStack *s) {
    s->failed = 0;
    if (s->summit <= 0) {
        s->failed = 1;
        return -666;
    }
    return s->data[--s->summit];
}
