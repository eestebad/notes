#ifndef __STACK_H__
#define __STACK_H__

#define MAX_SIZE 0x800 //Tamaño máximo de la estructura de datos

struct TStack {
    int *data; // Nos quedamos con el puntero en lugar de asignarle celdas para que la asignación de memoria sea dinámica.
    unsigned summit;// summit y size deben ser el mismo tpo  de dato porque si no, da error en la función push. 
    unsigned size;
    int failed;
};

#ifdef __cplusplus
extern "C"
{
#endif
    void init (struct TStack *s);
    void destroy (const struct TStack *s); //Hace paso por referencia para que sea más rápido y pone el const para que no se pueda cmabiar.
    void push (struct TStack *s, int ndata);
    int pop (struct TStack *s);
#ifdef __cplusplus
}
#endif

#endif
