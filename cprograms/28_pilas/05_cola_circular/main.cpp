#include <stdlib.h>
#include <time.h>

#include "queue.h"
#include "interface.h"

void show_data (const struct TQueue *queue) { // Es const y puntero para que no se copien los parámetros.
    show_queue (*queue);
    printf ("Pulse una tecla para continuar.");
    getchar ();
}

void fill (struct TQueue *q, unsigned s) { //Le pasamos una cola por referencia y mete un nº. Si no puede, muestra error y sale del bucle
    // s es el nº de elementos que vamos a empujar a la cola.
    for (unsigned i=0; i<s; i++) {
        push (q, rand () % 10 - 3);
        if (q->failed) {
            show_error ("Stack push operation failed.");
            break;
        }
    }

    show_data (q);
}

int extract (struct TQueue *q) { //Si hay error, se muestran las cosas por pantalla.
    int yield = shift (q);

    if (q->failed)
        show_error ("Stack pop operation failed.");

    show_data (q);

    return yield;
}

int main (int argc, char *argv[]) {
    struct TQueue queue;

    init (&queue);

    srand (time (NULL));
    fill (&queue, 4); // Meto 4 elementos en la pila.

    for (int i=0; i<3; i++) // Saco 3 elementos.
        printf ("\t=> %i\n", extract (&queue));

    fill (&queue, 3); // Empujo otros 3 elementos a la pila. Debería quedar una pila con 2 huecos libres.

    return EXIT_SUCCESS;
}
