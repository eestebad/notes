#include "queue.h"

/* FUNCIONES DE LA MÁQUINA DE DATOS */
void init (struct TQueue *s) {
    s->head   = 0;
    s->summit = 0;
    s->failed = 0;
}

void push (struct TQueue *s, int ndata) {
    s->failed = 0;
    if (s->summit - s->head >= M) { // Protección: si s-h es >= que el total de nº de celdas de array, no podemos meter más elementos.
        s->failed = 1;
        return;
    }

    s->data[s->summit % M] = ndata; // Mete un elemento en la celda de array 1ª posición libre % total celdas. Así si 9%9 summit está en 0.
    s->summit++; // Después incrementa summit, para que apunte a la 1ª celda libre de nuevo.
}

int shift (struct TQueue *s) {
    int ret;
    s->failed = 0;
    if (s->head >= s->summit) { // No permitimos que head rebase a summit porque se saldría de la cola.
        s->failed = 1;
        return -666;
    }

    ret = s->data[s->head % M]; // Utilizamos también % para que no se salga
    s->head++;

    return ret;
}
