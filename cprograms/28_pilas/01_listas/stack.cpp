#include <stdio.h>
#include <stdlib.h>
#include <time.h> //Contiene srand

/* Crear pila. Array donde se pueden ir añadiendo objetos o los datos que queramos y sólo se pueden sacar datos del final. Obliga a tener un entero que diga cuál es la 1ª posición libre, que se llama cima. Se empaqueta el array con la cima en una estructura */

#define M 0x05 //Tamaño máximo para la pila.

struct TStack { // Estructura de la pila.
    int data[M]; //Array de datos.
    int summit; //Cima. Irá apuntando a la primera posición libre.
    int failed; // Fallo de la operación
};

/* FUNCIONES DE LA MÁQUINA DE DATOS */
void init (struct TStack *s) { //se le pasa stack por referencia
    s->summit = 0; //Inicializamos la pila
    s->failed = 0; // El 0 significa que no hay error
}

void push (struct TStack *s, int ndata) { // Cambia la cima de la pila. Recibe la pila por referencia y el dato que quiero empujar.
    s->failed = 0; // Comienza cada operación indicando que no falla.
    if (s->summit >= M) { // si la pila está llena
        s->failed = 1; // si falla porque la pila está llena, mete 1 en failed.
        return;
    }
    s->data[s->summit++] = ndata; // Guarda el dato que quiero empujar e incrementa la pila. La cima se incrementa tb porque hay un dato más.
}

int pop (struct TStack *s) { // Recibe la pila por referencia y devuelve un entero, porque dará el dato de la cima.
s->failed = 0; // Ponemos fallo a 0
    if (s->summit <= 0) { // Si es <=0 no podemos hacer un pop porque no hay elementos en la pila.
        s->failed = 1; // Devolvería fallo
        return -666; // Nº que devuelve si da error, podría ser 0 .. o cualquier entero.
    }
    return s->data[--s->summit]; //Si no hay fallo, saca un dato de la pila (que será el anterior a la cima) y como la pila se hace más pequeña, decrementa la cima para que siga siendo la 1ª posición libre.
}

/* FUNCIONES DEL INTERFAZ */

void title () {
    system ("clear");
    system ("toilet -fpagga --gay 'PILA'");
    printf ("\n\n");
}

void show_error (const char *mssg) { // Da error por la salida de errores si la pila está llena.
    fprintf (stderr, "%s\n", mssg);
}

void show_stack (struct TStack s) { // Muestra la pila. No la recibe por referencia porque no la va a modificar.
    title ();
    for (int i=s.summit-1; i>=0; i--)// copia la pila, aunque sería mejor con -> y habría que pasarla por referencia. Es s.summit-1 e i se decrementa, para que muestre la pila con la cima arriba, si no, estaría la cima abajo. Summit apunta a la 1ª posición libre.
        printf ("\t%3i\n", s.data[i]); // Imprime las celdas de la pila con tres caracteres (%3i) para que los números salgan alineados uno encima de otro, ya que hay negativos y positivos.
    printf ("\n\n");
}

int main (int argc, char *argv[]) {
    struct TStack stack; //Declara la pila. Si creo personajes por ejemplo, los puedo ir echando en una pila.

    init (&stack); // Se le pasa stack por referencia para inicializarla

    srand (time (NULL)); // El primer nº que sea el que equivale al nº de segundos que han pasado desde el 1 de enero de 1960.
    for (int i=0; i<M; i++) { // For para que dé valores aleatorios y llenar toda la pila
        push (&stack, rand () % 10 - 3); // Pila por referencia y dato que quiero empujar, que será un nº aleatorio entre -3 y 7.
        if (stack.failed)
            show_error ("Stack push operation failed.");
    }

    show_stack (stack); // Va a mostrar la pila. Como no la va a cambiar, no hace falta el paso por referencia.

    printf ("\t=> %i\n", pop (&stack)); // Pop recibe la pila de la que se van a sacar los datos y por referencia porque va a cambiar la cima. Tiene que imprimirnos el dato que ha sacado.
    if (stack.failed) // si failed es 1, da error
        show_error ("Stack pop operation failed.");

    printf ("Pulse una tecla para continuar.");
    getchar ();

    show_stack (stack); //Muestra la pila.

    return EXIT_SUCCESS;
}
