#include <stdio.h>
#include <stdlib.h>

int main () {
    //Declaración de variables:
    int i; //entero de 16 bits = 2^16.
    float f; //decimal de 32 bits = 2^32.
    double d; //decimal de 64 bits = 2^64.
    char c; //carácter de 8 bits = 2^8.

    //Inicialización de variables:
    i = 5;
    f = 4.6;
    d = 78.9;
    c = 'g';

    //Imprimimos las variables:

    printf (
            "Valor de entero = %i\n Valor de float = %f\n Valor de double = %f\n Valor de char = %c\n", i, f, d, c
           );


    return EXIT_SUCCESS;
}
