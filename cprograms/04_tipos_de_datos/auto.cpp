#include <stdio.h>

//suma recibe un entero y recibe dos parámetros

int suma (int op1, int op2) {
    int resultado;

    resultado = op1 + op2;

    return resultado;
}

int main() {

    int resultado = 0;
/*2 y 3 son parámetros actuales
bp + 4 o bp + 8 son parámetros formales*/
// poniendo resultado = mete el 5 en resultado
resultado = suma (2,3);

printf ("Resultado = %i\n", resultado);

    return 0;
}
