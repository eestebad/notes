#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

/*Preguntar al usuario un nº del 1 al 10 y repetir la pregunta, mientras el dato introducido no sea válido. Opcional: vaciar la entrada antes de preguntar con `__fpurge ()` incluida en `stdio_ext.h`*/



int main () {
    //Declaro la variable número como entero
    int numero;
    /*Vacío la entrada antes de preguntar, solicito el nº al usuario y lo guardo en &numero*/
    do {
        __fpurge(stdin);
        printf ("Introduzca un nº del 1 al 10: \n");
        scanf (" %i", &numero);
    }
    /*Mientras el nº dado por el usuario sea menor que 1 ó mayor que 10, vuelve a ejecutar do*/
    while (numero < 1 || numero > 10);

    // Si el nº está comprendido entre 1 y 10, imprimo el mensaje:
    printf ("El nº introducido está comprendido entre 1 y 10");

    printf ("\n");

    return EXIT_SUCCESS;

}
