#include <stdio.h>
#include <stdlib.h>

// Dibujar el contorno de un triángulo con *


int main(){
    int ladoBase = 5;
    for(int f = 0; f < (ladoBase + 1) / 2; f++) {
        for(int c = 0; c < (ladoBase + 1) / 2-f-1; c++) { // Imprime los espacios. Dos en la 1ª vuelta de f
            // Uno en la 2ª vuelta y ninguno en la tercera. EN cada vuelta de f, uno menos.
            printf(" ");
        }
        for (int c = 0; c < 2 * f + 1; c++) { // Imprime los asteriscos. Cuando f = 0 imprime 1, f =1 
            // imprime 3, f = 2 imprime 5. En cada vuelta de f añade 2 * más.
            printf("*");
        }

        printf("\n");
    }
    return EXIT_SUCCESS;
}
