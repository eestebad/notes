#include <stdio.h>
#include <stdlib.h>

/*Imprimir n veces tu nombre, donde n es dado por el usuario*/

//Declaro la constante NOMBRE, con mi nombre.
const char* NOMBRE= "Elena";

int main () {
    //Declaro n como entero.
    int n;

    /*Se solicita al usuario el nº de veces para imprimir mi nombre*/
    printf ("Introduzca el nº de veces que desea imprimir el nombre: \n");
    scanf (" %i", &n);

    /*Se imprime mi nombre mientras veces sea menor que n*/
    for ( int veces = 0; veces < n; veces++ )
        printf (" %s\n", NOMBRE);

    return EXIT_SUCCESS;

}



