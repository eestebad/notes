#include <stdio.h>
#include <stdlib.h>

//NO FUNCIONA OK
// Con el uso de if en los bucles, haz la siguiente figura:
// *
//  *
//   *
//    *
//     *
//      *
//       *
//        *
// Declaro las variables con las dimensiones de la figura
unsigned altura = 8;
unsigned anchura = 8;

int main () {
    //Imprimo el título.
    printf ("Figura 4\n");

    for ( int f = 0; f < altura; f++ ) {
        for ( int c = 0; c < anchura; c++ ) {
            if ( f == 0 || c == 0 )
                printf ("*");

            if ( f == 1 || c == 1 )
                printf ("*");

            if ( f == 2 || c == 2 )
                printf ("*");
            else
                printf (" ");
            printf ("\n");
        }
    }
    return EXIT_SUCCESS; }
