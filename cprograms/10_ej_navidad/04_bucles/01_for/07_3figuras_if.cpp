#include <stdio.h>
#include <stdlib.h>

/*Introduciendo un if en los bucles, hacer diferentes figuras*/

// ********
// *      *
// *      *
// *      *
// ********

int main () {

    /*Declaro las variables altura y anchura, que dan el tamaño del rectángulo*/
    unsigned altura = 5;
    unsigned anchura = 8;

    //Imprimo título
    printf ("Figura 3\n");

    /*Mientras el contador f (fila) sea menor que la altura, ejecuta el bloq    ue*/
    for (int f = 0; f < altura; f++) {
        /*Mientras el contador c (columna) sea menor que la anchura, ejecuta         el bloque*/
        for (int c = 0; c < anchura; c++)

            /* Si la fila o la columna son 0, imprime *. Si la fila es igual             a la altura -1 o la columna es igual a la anchura -1, imprime * */
            if (f == 0 || c == 0 || f == altura -1 || c == anchura -1)
                printf ("*");
        /* Si if es falso, imprime un espacio*/
            else
                printf (" ");
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
