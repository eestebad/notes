#include <stdio.h>
#include <stdlib.h>

/*Introduciendo un if en los bucles, hacer diferentes figuras*/

// ********
//
//
// ********

int main () {

    /* Declaro las variables altura y anchura */
    unsigned altura = 5;
    unsigned anchura = 8;

    //Imprimo el título
    printf ("Figura 1\n");

    /* Mientras f (nº de filas) < la altura definida, imprime *, si f (el nº de fila es igual a 0 ó si es igual a 4. Imprime espacio si no lo es, con una anchura de 8 */
    for (int f = 0; f < altura; f++) {
        for (int c = 0; c < anchura; c++)
            if (f == 0 || f == 4)
                printf ("*");
            else 
                printf (" ");
        printf ("\n");
    }
    return EXIT_SUCCESS;
}
