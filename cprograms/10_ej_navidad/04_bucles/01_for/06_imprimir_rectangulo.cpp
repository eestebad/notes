#include <stdio.h>
#include <stdlib.h>

/*Imprimir f filas de c asteriscos. Rectángulo*/

int main () {
    //Defino f (filas) y c (columnas).
    int f = 5;
    int c = 8;
    /*Mientras el contador i sea menor que f, imprime *. Esto es la altura del rectángulo, el nº de filas*/
    for (int i = 0; i < f; i++) {
        /*Mientras el contador i sea menor que c, imprime *. Esto es la anchura del rectángulo, el nº de columnas*/
        for (int i = 0; i < c; i++)
            printf ("*");
        printf ("\n");
    }
    printf ("\n");

    return EXIT_SUCCESS;

}
