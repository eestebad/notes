#include <stdio.h>
#include <stdlib.h>

/*Imprimir 10 veces tu nombre*/
const char* NOMBRE = "Elena";

int main () {

    for (int veces = 0; veces < 10; veces ++)
        printf (" %s\n", NOMBRE);

    return EXIT_SUCCESS;
}
