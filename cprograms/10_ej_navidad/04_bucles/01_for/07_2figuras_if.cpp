#include <stdio.h>
#include <stdlib.h>

/*Introduciendo un if en los bucles, hacer diferentes figuras*/

// *      *
// *      *
// *      *
// *      *
// *      *


int main () {

    /* Defino las variables altura y anchura, que conforman las dimensiones de la figura */
    unsigned altura = 5;
    unsigned anchura = 8;

    //Imprimo título
    printf ("Figura 2\n");

    /* Mientras el contador f (fila) sea menor que la altura, ejecuta el bloque*/
    for ( int f = 0; f < altura; f++ ) {
        /* Mientras el contador c (columna) sea menor que la anchura, ejecuta el bloque: si la columna es 0 ó es 7, imprime asterisco */
        for ( int c = 0; c < anchura; c++ )
            if ( c == 0 || c == 7 )
                printf ("*");
        /* Si el if es falso (c no es 0 ni 7), imprime espacio*/
            else
                printf (" ");
        printf ("\n");

    }

    return EXIT_SUCCESS;

}
