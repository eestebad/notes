#include <stdio.h>
#include <stdlib.h>

//Dibujar con * el contorno de un triángulo.

int main(){
    int ladoBase = 5;
    for(int f = 0; f < ladoBase; f++) {
    //For para los espacios en blanco antes de los *
           for(int c = 0; c < ladoBase - f - 1; c++) {
            	printf(" ");
            }
           for(int j = 0; j < 2 * f + 1; j++){ // 2*f para que imprima el triángulo completo.
            //Si alguna de estas condiciones es verdadera, imprime *
            	if((f == 0)||(f == ladoBase - 1)||(j == 0)||(j == 2 * f)) {
                    printf("*");
                }
                else{printf(" ");} //Si no, imprime espacio, para hacer el hueco en blanco.
            }
    printf("\n"); // Salto de línea, para cambiar de fila del triángulo
    }
    return EXIT_SUCCESS;
}

