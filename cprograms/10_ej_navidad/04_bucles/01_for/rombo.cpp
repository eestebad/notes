#include <stdio.h>
#include <stdlib.h>

// Dibujar rombo con *


int main(){
    int ladoBase = 5;
    for(int i = 1; i <=ladoBase; i = i + 2){
	    // Añadimos los espacios necesarios delante de cada linea
       for(int j = ladoBase + 1; j >= i; j = j - 2) {
	   printf(" ");
       }
        // Mostramos los asteriscos
       for(int k = 0; k < i ;k++) {
           printf("*");
       }
       printf("\n"); }
      //parte inferior del rombo
       for(int i = ladoBase; i >= 1; i = i - 2){
            // Añadimos los espacios necesarios delante de cada linea
            for(int j = i; j <= ladoBase + 2; j = j + 2) {
                printf(" ");}
            // Mostramos los asteriscos
            for(int k = i - 2 ; k > 0; k--) {
                printf("*");}
            printf("\n");
       }
    return EXIT_SUCCESS;
}
