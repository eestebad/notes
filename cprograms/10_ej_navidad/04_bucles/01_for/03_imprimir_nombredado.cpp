#include <stdio.h>
#include <stdlib.h>

//Imprimir n veces, el nombre dado por el usuario

/*Defino la cadena de caracteres nombre y el entero n*/
char nombre [30];
int n;

int main () {

    /*Solicito el nombre que desea imprimir al usuario y lo guardo en la cadena nombre*/
    printf ("Introduzca el nombre que desea imprimir: \n");
    scanf (" %s", nombre);

    /*Solicito el nº de veces que desea imprimir el nombre y lo guardo en n*/
    printf ("Introduzca el nº de veces que desea imprimirlo: \n");
    scanf (" %i", &n);

    /*Imprime nombre mientras el contador i sea menor que n (nº de veces dado por el usuario)*/
    for ( int i = 0; i < n; i++ )
        printf (" %s\n", nombre);

    return EXIT_SUCCESS;


}
