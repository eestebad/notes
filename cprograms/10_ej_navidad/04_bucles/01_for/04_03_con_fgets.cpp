#include <stdio.h>
#include <stdlib.h>

/*Usar fgets en vez de scanf, para resolver el ejercicio 3 (Imprime n veces el nombre dado por el usuario*/

/*Defino la cadena de caracteres nombre y el entero n*/
char nombre [30];
int n;
int main () {

    /*Solicito al usuario que introduzca el nombre que desea imprimir*/
    printf ("Introduzca el nombre que desea imprimir: \n");
    /* fgets lee de un fichero o del teclado (stdin) cadenas de caracteres, por lo que sólo es apropiado para leer el nombre, pero no el nº de veces, porque es un entero.*/
    fgets (nombre, 30, stdin);
    /*Solicito al usuario que introduzca el nº de veces que desea imprimir el nombre indicado*/
    printf ("Introduzca el nº de veces que desea introducir el nombre: \n");
    scanf (" %i", &n);

    /*Imprimo el nombre mientras el contador i sea menor que n*/
    for (int i = 0; i < n; i++)
        printf (" %s", nombre);

    return EXIT_SUCCESS;

}
