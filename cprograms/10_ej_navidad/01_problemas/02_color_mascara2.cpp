#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

/*Si un color viene definido por unsigned char r, g, b; Valores cualquiera 
  definidos por el usuario. ¿Cuánto vale el XOR de r con la máscara 255? Imprime el resultado numérico y compruébalo en un editor de fotos. ¿Y si coges otra máscara? Deja unos png/jpg en el directorio para ver cómo han cambiado. [Operador XOR]*/

/* int main () {
//Defino variables
unsigned char r, g, b;
unsigned mask[3] = {2, 5, 5};
unsigned resultado = r ^ mask;

//Solicito los valores para r, g y b.
printf ("Introduzca un valor para r: ");
scanf (" %c", &r);

printf ("Introduzca un valor para g: ");
scanf (" %c", &g);

printf ("Introduzca un valor para b: ");
scanf (" %c", &b);

//Hago la operación xor con mask e imprimo el resultado
//El xor no funciona correctamente
printf ("El xor de r con mask es: %u \n", resultado);
//Imprimo el resultado en hexadecimal.
printf ("El xor de r con mask en hexadecimal: %x \n", resultado);
/*Falta imprimir un resultado numérico que sea tratable en un editor de fotos*/

//return EXIT_SUCCESS;
//} */

void titulo () {
    system ("clear");
    system ("toilet --gay -fpagga COLOR");
    printf ("\n\n");
}

unsigned char pregunta_intensidad(const char *componente) {
    unsigned int entrada;
    do {
        __fpurge (stdin);

        printf ("Intensidad de %s [0-255]: ", componente);
        scanf (" %u", &entrada);
    } while (entrada > 0xFF);
    return (unsigned char) entrada;
}

int main (){
    unsigned char r, g, b;
    unsigned char xr;

    titulo ();
    r = pregunta_intensidad ("rojo");
    g = pregunta_intensidad ("verde");
    b = pregunta_intensidad ("azul");

    xr = r ^ 0xFF; //255
    printf ("#%X%X%X => #%X%X%X\n", r, g, b, xr, g, b);//En hexadecimal
    return EXIT_SUCCESS;
}
/*printf ("Intensidad de rojo: ");
  scanf (" %i", &entrada);
  r = (unsigned char) entrada;

  printf ("Intensidad de verde: ");
  scanf (" %i", &entrada);
  g = (unsigned char) entrada;

  printf ("Intensidad de azul: ");
  scanf (" %i", &entrada);
  b = (unsigned char) entrada;

  xr = r ^ 0xFF; //255

  printf ("#%x%x%x => %x%x%x\n", r, g, b, xr, g, b);

  return EXIT_SUCCESS;
  }
  */

