#include <stdio>
#include <stdlib>

3210
1001 => 8 //hija y padre levantados
PAPUP => 8 1000
MAMUP => 4 0100
HIJOP => 2 0010
HIJAP => 1 0001

waked = PAPUP | HIJAP; //ACTIVA HIJA Y PADRE = 1001
waked = waked | PAPUP; => PONER AL PADRE A DESPIERTO.
waked = waked ^ PAPUP; => Conmutar el padre (despierto o dormido)
waked = waked & ~PAPUP;=> acostar al padre. 1001 & 0111 (padre negado)=0001


#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    mama,
    hijo,
    total_miembros
};


/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
   al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {
    int new_status;
    int bit;
    switch (miembro) {
        /* Rellena todos los casos */
        case papa:
            /* Realiza la operación a nivel de bits necesaria para
               cambiar el bit correspondiente. Usa los valores en los
               define. No uses if. */
            new_status = old_status ^ PAPUP; /*el valor de new_status, que es como un return, quedará en conmutar (waked papa) y lo guardará en waked*/
            break;
        case mama:
            new_status = old_status ^ MAMUP;
            break;
        case hijo:
            new_status = old_status ^ HIJOP;
            break;
        case hija:
            new_status = old_status ^ HIJAP;
            break;
        default:
            bit = 0;
    }

    new_status = old_status ^ bit;
    return new_status;
}

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
   al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    int bit = 1,
        pos = total miembros -1 - miembro; /* cambia el nº que corresponde a cada miembro, los invierto, para que valgan lo mismo que las constantes*/
    for (int vez = 0; vez<pos; vez++) /*bit => PAPUP | MAMUP...*/
        bit = bit << 1; //lo mismo que multiplicar por 2

    return old_status & ~bit;

}


/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
   al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
    int bit = 1

}
}

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá está %s\n", waked & PAPUP ? "levantado": "acostado"); /*si papá esta acostado en waked me dará cero y si no uno*/
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa;

    waked = levantar (waked, papa);
    waked = acostar (waked, papa);
    waked = conmutar (waked, papa);

    print (waked);

    return EXIT_SUCCESS;
}
```

