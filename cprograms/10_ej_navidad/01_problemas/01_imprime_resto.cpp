#include <stdio.h>
#include <stdlib.h>

//Imprimir el resto de la división 13.0 / 7

int main () {

    /*Declaración de variables con distinto tipo de dato. El operador % sólo funciona con enteros, así que hacemos una conversión explícita de f, dentro de la variable resultado, a entero*/
    double f = 13.0;
    int i = 7;
    int resultado = (int) f % i;

    //Imprimimos el resultado
    printf (" %i\n", resultado);

    return EXIT_SUCCESS;
}


