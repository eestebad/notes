#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    mama,
    hijo,
    hija,
    total_miembros
};

/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
   al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {  //la función conmutar va a cambiar el estado del miembro de la familia, al estado contrario. Si levantado lo acuesta y si acostado, lo levanta.
    int new_status;
    int bit;
    switch (miembro) {
        case papa:
            bit = PAPUP;
            break;
        case mama:
            bit = MAMUP;
            break;
        case hijo:
            bit = HIJOP;
            break;
        case hija:
            bit = HIJAP;
            break;
        default:
            bit = 0;
    }
    new_status = old_status ^ bit;

    return new_status;
}

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
   al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    int bit = 1,
        pos = total_miembros - 1 - miembro; /* Invierto los miembros de la familia para obtener el valor de la constante correspondiente a ese miembro*/
    for (int vez=0; vez<pos; vez++) /*bit => PAPUP | MAMUP,...*/
        bit <<= 1;//lo mismo que bit = << 1 pero en taquigráfico

    return old_status & ~bit;//devuelve el & de old_status con bit negado.
}

/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
   al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
    /*Se puede hacer con un for:
      int bit = 1,
      pos = total_miembros - 1 - levantado; // Invierto los miembros de la familia
      for (int vez=0; vez<pos; vez++) //bit => PAPUP | MAMUP,...
      bit = bit << 1;

      return old_status | bit;*/
    //O mejor aún:
    int pos = total_miembros -1 -levantado;

    return old_status | (int) pow (2, pos);
}

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá está %s\n", waked & PAPUP ? "levantado": "acostado");
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa;

    waked = levantar (waked, papa); /*llama a levantar, donde waked viene de old_status de la función levantar y papa viene de enum TMiembro levantado de la misma función. Igual en el resto de llamadas a funciones*/ 
    waked = acostar (waked, papa);
    waked = conmutar (waked, papa);
    waked = conmutar (waked, papa);

    print (waked);

    return EXIT_SUCCESS;
}

