#include <stdio.h>
#include <stdlib.h>
//Falta. No lo entiendo
/*Sea la variable de byte `unsigned waked;` que recoge cuantos miembros (papá, mamá, hijo, hija) están levantados. Se pone el bit 0(hija), 1, 2, y 3 (papá) respectivamente, se pide completar el siguiente programa siguiendo las instrucciones proporcionadas en los comentarios.
Empieza siempre a leer los programas por la sección global, y luego sigue por el main.*/

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    /* Define aquí el resto de miembros de la familia. */
    mama,
    hijo,
    hija,
    total_miembros
};


/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {
    switch (miembro) {
        /* Rellena todos los casos */
        case /*...*/
           /* Realiza la operación a nivel de bits necesaria para
           cambiar el bit correspondiente. Usa los valores en los
           define. No uses if. */
        break;
    }
}

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    switch (miembro) {
        /* Rellena todos los casos */
        case /*...*/
           /* Realiza la operación a nivel de bits necesaria para
           poner a 0 el bit correspondiente. Usa los valores en los
           define */
        break;
    }
}

/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
    switch (levantado) {
        /* Rellena todos los casos */
        case /*...*/
           /* Realiza la operación a nivel de bits necesaria para
           poner a 1 el bit correspondiente. Usa los valores en los
           define */
        break;
    }
}

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá está %s\n", /* Usa una operación de bits*/ ? "levantado": "acostado")
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa;

    waked = levantar (waked, papa);
    waked = acostar (waked, papa);
    waked = conmutar (waked, papa);

    print (waked);

    return EXIT_SUCCESS;
}
