#include <stdio.h>
#include <stdlib.h>

/* Dada una variable cuyo bit 2 indica si tiene rojo, el 1 si verde y el 0 si azul, usando un switch, imprime qué color está viendo el usuario. */
//Defino una variable tipo enum para asignar un nº a cada color.
enum TColor {
    azul,
    verde,
    rojo,
    TOTALOPCIONES
};
//Defino la constante colores con los nombres de los colores.
const char *colores[] = {
    "azul",
    "verde",
    "rojo",
    NULL
};
/*Defino función menu, que solicita un color al usuario y lo guarda en la variable opcion*/
int menu () {
    int opcion;
    printf ("Introduce un color:\n 0\n 1\n 2\n");
    scanf (" %i", &opcion);
    return opcion;
}
/* Imprime la opción numérica que ha elegido el usuario*/
int main () {
    int opcion = menu ();
    printf ("La opción elegida es: %i\n", opcion);
    /* Comprobamos con qué etiqueta coincide el nº introducido por el usuario. Si coincide con alguna de ellas, se imprime el nombre del color que corresponda y si no, se imprime el mensaje de la opción por defecto */
    switch (opcion) {
        case azul:
            printf ("El color elegido es azul\n");
            break;
        case verde:
            printf ("El color elegido es verde\n");
            break;
        case rojo:
            printf ("El color elegido es rojo\n");
            break;

        default:
            fprintf (stderr, "Ese color no existe.\n");

            return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;

}
