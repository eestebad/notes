#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

/*Si un color viene definido por unsigned char rgb[3]; y tenemos que:*/
#define R 2
#define G 1
#define B 0
#define COLS 3//la capacidad del array rgb, así si necesito cambiarlo en algún momento, lo cambio sólo aquí, no en todo el programa.

/*Si definimos una máscara unsigned char mask[3]; :
  Eres capaz de:
  1. Pedirle al usario un color y una máscara.
  1. Hacer un XOR del color con la máscara.
  1. Dejar un ejemplo de los dos colores hecho con algún programa de dibujo.*/
void titulo () {
    system ("clear");
    system ("toilet --gay -fpagga MASCARA");
    printf ("\n\n");
}

/*Defino la función pedir_color para utilizarla después en el main y no tener que preguntar por cada color*/
unsigned char pedir_color (const char *nombre) {

    int buffer;/*defino buffer que es int para que quepa cualquier nº que meta el usuario. No usamos aquí el array, porque tiene 1 byte y si alguien mete un nº mayor, me saldría del espacio de memoria que tengo reservado y podría machacar otra cosa*/
        printf ("\n");
        do {
            printf ("\x1B[1A");
                printf ("%s (0-255): ", nombre);
            printf ("          ");
            printf ("\x1B[1A");
            __fpurge (stdin);//vacía el tubo para asegurarnos de que si ha metido una letra, se extrae antes de volver a preguntar.
            scanf (" %u", &buffer);
            //Pregunta mientras lo que el usuario introduzca, no está entre 0-255.
            while (buffer < 0 || buffer > 255);
            return (unsigned char) buffer;//cambia el tipo de buffer de int que es más grande a unsigned, pq después de preguntar, ya sé que el nº está entre 0-255 y no necesito que sea tan grande, así que lo hago más pequeño, a un byte.

        }
}
    int main () {
        //Defino a la vez rgb y mask, el tamaño es 3 (COLS que está definido arriba).
        unsigned char rgb[COLS],
        mask[COLS];

        titulo ();//Llamo a título.

        printf ("COLOR:\n");
        printf ("======\n");
        //Se llama a la función pedir_color 3 veces, para pedir el color. "Rojo" "Verde" o "Azul", que lo metemos en la var nombre de la función pedir_color.
        rgb[R] = pedir_color ("Rojo");
        rgb[G] = pedir_color ("Verde");
        rgb[B] = pedir_color ("Azul");
        //Lo mismo para la máscara
        printf ("MASCARA:\n");
        printf ("========\n");
        mask[R] = pedir_color ("Rojo");
        mask[G] = pedir_color ("Verde");
        mask[B] = pedir_color ("Azul");

        resultado[B] = rgb[B] ^ mask[B];
        resultado[G] = rgb[G] ^ mask[G];
        resultado[R] = rgb[R] ^ mask[R];
      
        return EXIT_SUCCESS;
    } 
