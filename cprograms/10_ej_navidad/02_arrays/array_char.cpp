#include <stdio.h>
#include <stdlib.h>

/*Definir un array de 20 unsigned char, rellenarlo con el cuadrado de los números del 1 al 19 e imprimir lo que contiene*/

#define MAR 20

int main (int argc, char *argv[]) {

    unsigned char l[MAR]; //se define sin * si quieres que apunte al contenido.

    //Bloque cálculo
    for (int i = 0; i < MAR; i++)
        //l[i] = i + 1; => rellenaría las celdas con los nº del 1 al 19.
        l[i] = (i + 1) * (i + 1);

    //Bloque salida de datos
    for (int c = 0; c < MAR; c++)
        printf (" %i\n", l[c]);


    return EXIT_SUCCESS;
}
