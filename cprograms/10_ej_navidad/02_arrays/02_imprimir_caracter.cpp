#include <stdio.h>

//Imprime carácter a carácter la siguiente variable
//Se define N como constante que ocupa 6
#define N 6

int main () {

    /* Se define el array nombre que ocupa 6 (N)*/
    char nombre[N] = { 'V', 'i', 'c', 't', 'o', 'r' };
    /* Al no estar presente el valor centinela '\0' tenemos que recorrer todas las celdas */
    /* Imprime cada carácter del array utilizando el contador i que se va incrementando en cada vuelta, lo que permite que imprima cada carácter del array nombre*/
    for (int i = 0; i < N; i++) 
        printf ("%c\n", nombre[i]);

    return 0;
}
