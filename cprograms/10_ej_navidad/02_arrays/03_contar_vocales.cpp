#include <stdio.h>
#include <ctype.h>
//Cuenta la cantidad de vocales en un nombre.
int main () {
    char nombre[] = "SupercalifragilisticoespialidosO";
    int a, e, i, o, u;
    int n_vocales = 0;
    a = e = i = o = u = 0;

    /* Recorremos las celdas para contar la cantidad de vocales */
    for (int i = 0; nombre[i] != '\0'; i++) {
        char vocal = tolower(nombre[i]);
        if ( vocal == 'a' || vocal == 'e' || vocal == 'i' || vocal == 'o' || vocal == 'u' )
            n_vocales++;
    }
    //Imprime los resultados.
    printf ("El nº de vocales es: %i", n_vocales);
    printf ("\n");
    //NOTA: puedes mirar el man de tolower.

    return 0;
}
