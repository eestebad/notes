#include <stdio.h>
#include <stdlib.h>

/* Imprime tus 10 nombres preferidos. Usa el valor centinela para saber cuándo has llegado al final */
// Defino la constante nombre con los nombres y el valor centinela
const char *nombre[] = {"Carolina", "Claudia", "Celia", "Gonzalo", "Alba", "Estrella", "Ana", "Alicia", "Belén", "Salvador", NULL};

int main () {
    /* Mientras el contador i sea distinto del valor centinela NULL, imprime el nombre e incrementa el contador. Se usa el contador para indicar qué posición del array se imprime */
    for ( int i = 0; nombre[i] != NULL ; i++ )
        printf (" %s\n", nombre[i]);


    return EXIT_SUCCESS;
}
