#include <stdio.h>
#include <stdlib.h>
//NO COMPILA
/*Si un color viene definido por unsigned char rgb[3]; y tenemos que:*/
#define R 2
#define G 1
#define B 0
unsigned char rgb[3] = {R, G, B};
/*Si definimos una máscara unsigned char mask[3]; :
  Eres capaz de:
  1. Pedirle al usario un color y una máscara.
  1. Hacer un XOR del color con la máscara.
  1. Dejar un ejemplo de los dos colores hecho con algún programa de dibujo.*/
unsigned char mask[3];
unsigned char resultado = (rgb ^ mask);
int main () {

    printf ("Introduzca un color: R, G o B: ");
    scanf (" %s", rgb);
    printf ("Introduzca una máscara: ");
    scanf (" %s", mask);

    printf ("El xor de rgb con mask es: %u", resultado);

    return EXIT_SUCCESS;
} 
