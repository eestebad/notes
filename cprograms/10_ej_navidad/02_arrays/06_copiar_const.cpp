#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Copia la constante tipo cadena `const char *origen = "Feliz Navidad";` en el array `char destino[100];` */

#define MAXLIN 13 //defino el máximo de caracteres que quiero que copie.

//Declaro el const char y el array.
const char *origen = "Feliz Navidad y próspero año nuevo";
char destino[100];

int main () {

    //Imprimo lo que contienen en inicio el array y la constante.
    printf ("El array Destino contiene en principio: %s\n", destino);
    printf ("La constante cadena Origen contiene en principio: %s\n", origen);
    /*Copio a destino, lo que hay en origen, con el nº máximo de caracteres que he definido en la constante de preprocesador MAXLIN*/
    strncpy (destino, origen, MAXLIN);

    /*Imprimo lo que contienen ahora el array y la constante, que se mantiene. Se copia parcialmente a destino, porque he definido un nº máximo de caracteres menor que los que contiene origen*/
    printf ("El array Destino contiene ahora: %s\n", destino);
    printf ("La constante cadena Origen no cambia: %s\n", origen);

    return EXIT_SUCCESS;

}
