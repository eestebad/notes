#include <stdio.h>
#include <stdlib.h>
/* Explícate a ti mismo por qué al preguntar el valor de nombre, no hace falta el ampersand. Hazte un dibujo*/
/* Nombre no va precedido por &, porque es una cadena de caracteres y su nombre, ya es por sí mismo un puntero o dirección de memoria */
int main () {

    char nombre[0x20]; //0x20 es el carácter espacio
    printf ("Nombre: ");
    scanf (" %s", nombre);

    printf ("El nombre que has escrito es: %s\n", nombre);

    return EXIT_SUCCESS;
}
