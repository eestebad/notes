#include <stdio.h>
#include <stdlib.h>
//Versión de Txema.

char const * lista[] = {
    "Guten Morgern",
    "God Morning",
    "Buenos días",
    "Bon Giorno",
    "Bon Jour",
    "Bom Dia",
    "Καλημέρα",
    NULL
};

int main (int argc, char *argv[]) {
    char const **p = lista;

    while (*p != NULL){
        printf ("%s\n", *p);
        p++;
    }

    return EXIT_SUCCESS;
}

