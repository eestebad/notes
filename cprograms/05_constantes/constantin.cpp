#include <stdio.h>

#define EURO 166.386

#define TECNICOS 1
#define SOCIALES 2
#define CONTACTOS 4

ENUM TipoPalo {
    oros, copas, espadas, bastos, PALOS
}/*ENUM asigna un valor a cada palo (0, 1, 2, 3 y en PALOS el total*/

int main () {
    double input;
enum TipoPalo Micarta = espadas; /*del tipo entero quiero que me hagas una variable llamada mi carta*/

    int mariano = SOCIALES | CONTACTO /*mariano valdría en binario 110 porque hace un or con el valor en binario de SOCIALES y el valor en binario de CONTACTOS*/
    printf ("How much do you wanna change? ");
    scanf (" %lf", &input); /* el espacio antes del % significa saca todos los caracters que haya en el tubo antes. &input, significa la dirección de la variable input. Esta línea dice: después de haber sacado todos los espacios en blanco, me lees un nº real y lo metes en la dirección de input*/

    printf ("%.2lf₧  => %.2lf€", input, input / EURO);/* el => es una flecha*/


    return 0;
}
