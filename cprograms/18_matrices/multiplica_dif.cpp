#include <stdio.h>
#include <stdlib.h>

//PROGRAMA QUE CALCULE MULTIPLICACIÓN DE UNA MATRIZ DE 2X4 Y OTRA DE 4X3, EN LA RESULTANTE DE 2X3. Para poder multiplicar dos matrices, es necesario que el nº de columnas de la 1ª, sea igual que el nº de filas de la 2ª.
//Con dos for
#define F 2
#define M 4
#define D 3

void pedir_matrices(double m1[F][M], double m2[M][D]){ 
    printf("Introduzca la matriz A de 2x4:\n");
    for(int f = 0; f < F; f++){
        for(int c = 0; c < M; c++)
            scanf("%lf", &m1[f][c]);
    }

    printf("Introduzca la matriz B de 4x3:\n");
    for(int f = 0; f < M; f++){
        for(int c = 0; c < D; c++)
            scanf("%lf", &m2[f][c]);
    }
}

void imprime(double m3[F][D], const char *label){
    printf("La matriz %s resultante:\n", label);
    for(int f = 0; f < F; f++){
        printf("\n");
        for(int c = 0; c < D; c++)
            printf("%7.1lf", m3[f][c]);
    }
    printf("\n");
}

int main(){

    double A[F][M], B[M][D], C[F][D];

    pedir_matrices(A, B);

    for(int f = 0; f < F; f++)
        for(int c = 0; c < D; c++)
            for(int j = 0; j < M; j++)
                C[f][c] += A[f][j] * B[j][c];

    imprime(C, "C");

    return EXIT_SUCCESS;
}
