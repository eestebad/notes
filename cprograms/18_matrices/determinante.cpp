#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Hacer el determinante de una matriz.

#define D 3

double determinante(double op1[D], double op2[D]){
  double resultado1 = 0;

    for(int d = 0; d < D; d++) //d es la diagonal
        resultado1 += op1[d] * op2[d];

        return resultado1;
}

int main() {
  double v[D][D]= {
      {1, 2, 3},
      {4, 5, 6},
      {7, 8, 9}
  };

  printf("%.2lf", determinante);

    return EXIT_SUCCESS;
}
