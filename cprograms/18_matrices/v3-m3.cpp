#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

//#include "ansi.h" //Lo comento porque yo no la tengo. Es una cabecera creada para este programa con 3 constantes numéricas.


void title () {
    system ("clear");
    system ("toilet -fpagga --gay MATRICES");
    printf ("\n");
    printf ("Multiplicación de Matrices.\n");
    printf ("\n\n");
}

void pedir_datos (double *m, char const * const label, int filas, int cols) { //aquí label es puntero que no puede cambiarse, así como el char, ambos son constantes.
    title ();
    printf ("Introduce los datos de la Matriz %s\n", label);
    printf ("\n");

    for (int f=0; f<filas; f++) {
        for (int c=0; c<cols; c++) {
            printf ("\t\t\t");
            for (int tab=0; tab<c; tab++)
                printf ("\t");
            scanf ("%lf", (m + f * cols + c));  /* Aquí es imposible usar la notación de Arrays &m[f][c]. Al no saber el ancho le es imposible calcular el stride. */
//            GO_UP(1); //lo comento porque da error al quitar la cabecera "ansi.h", lo que hace es subir una línea el cursor.
        }
        printf ("\n");
    }
}

void mostrar (double *m, char const *label, int filas, int cols) { // Aquí el char es constante, el puntero label no.

    printf ("Matriz %s:\n", label);
    printf ("\n");
    for (int f=0; f<filas; f++) {
        printf ("\t\t");
        for (int c=0; c<cols; c++)
            printf ("%9.2lf", *(m + f * cols + c)); /* Idem con la notación de Arrays*/
        printf ("\n");
    }
}

int main (int argc, char *argv[]) {

    double *A, *B, *C;
    int M, K, N;   // Mantengo las mayúsculas por analogía con v2-m3.cpp

    /* Parte nueva */
    title ();
    printf ("Filas de A: ");
    scanf ("%u", &M);
    printf ("Columnas de A y Filas de B: ");
    scanf ("%u", &K);
    printf ("Filas de B: ");
    scanf ("%u", &N);


    /* Creamos las celdas de las matrices en el HEAP */
    A = (double *) malloc ( M * K * sizeof (double) );
    B = (double *) malloc ( K * N * sizeof (double) );
    C = (double *) malloc ( M * N * sizeof (double) );
    /* Fin de lo nuevo */

    bzero (C, M * N * sizeof(C));  /* Poner todo a 0. Se incluyen M y N porque ahora ya conocemos su valor */
    pedir_datos ( A, "A", M, K);   
    pedir_datos ( B, "B", K, N);

    /* Para calcular la fila i y j de c, sabemos que cij = aik·bkj contrayendo k en una acumulación */
    for (int i=0; i<M; i++)
        for (int j=0; j<N; j++)
            for (int k=0; k<K; k++)
                // C[i][j] += A[i][k] * B[k][j];  /* No puede calcular el stride */
                *(C + i * N + j) += *(A + i * K + k) * *(B + k * N + j); //modifico = a += porque si no, guarda el valor de la última multiplicación.

    title ();
    mostrar ( A, "A", M, K);
    mostrar ( B, "B", K, N);
    mostrar ( C, "C", M, N);
    printf ("\n\n");

    /* Nos acordamos de lberar el HEAP */
    free (A);
    free (B);
    free (C);

    return EXIT_SUCCESS;
}
