#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define CUANTOS 4

int main (int argc, char *argv[]) {

//    unsigned long perfecto[CUANTOS];
  //  unsigned long s = 0;  /* summit: cima de la pila. Primera posición vacía. */
    unsigned long candidato = 2;
    unsigned long suma = 0;
    unsigned long cuantos = 0;

    /* Cálculos */
    for (unsigned long i=0; i < CUANTOS; suma=0, candidato ++) {
        for (unsigned long div=candidato/2; div>0; div--) {
            if (candidato % div == 0)
                suma += div;
        }
        if (suma == candidato) {
            cuantos ++;
            printf("%lu\n", candidato);
        }
    }


    /* Salida */
  //  printf ("Números perfectos:\n\n");
   // for (int i=0; i<s; i++)
     //   printf(" %lu", perfecto[i]);

   // printf ("\n\n");


    return EXIT_SUCCESS;
}
