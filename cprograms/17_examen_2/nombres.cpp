#include <stdio.h>
#include <stdlib.h>

//Imprimir los nombres de un array de chars
int main () {
  const char *lista[] = {"Juan", "María", NULL};
  const char **puntero = lista;

  while(*puntero != NULL) //Mientras el valor de *puntero no sea NULL
     printf("\t%s\n", *puntero++); //Imprime lo que hay en la dirección de *puntero e incrementa *puntero

//    for (const char **puntero = lista;
//           *puntero != NULL; puntero++ )
//        printf ("\t%s\n", *puntero);

//    printf ("\n\n");

//    for (int i=0; lista[i]; i++)
//        printf ("\t%s\n", lista[i]);
//    printf ("\n\n");



    return EXIT_SUCCESS;
}
