#include <stdio.h>
#include <stdlib.h>


int main() {

    unsigned long num = 0;
    unsigned long suma = 0;

    printf("Dígame hasta qué nº quiere que compruebe los números perfectos: ");
    scanf(" %lu", &num);
    printf("\n");

    printf("los números perfectos del 1 al %lu son: ", num);
    for( unsigned long j = num; j <= num; j--){
        suma = 0;
        for(unsigned long i = 1; i <= j; i++){
            if(j % i == 0)
                suma += i;
        }
        if(j == suma)
            printf("%lu ", suma);
    }

    printf("\n");

    return EXIT_SUCCESS;
}
