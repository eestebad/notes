#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Como ya sabes pedir vectores, calcular el módulo de vectores y hacer su producto 
 escalar, esto te puede servir para calcular el ángulo que forman, ya que existe
 una segunda fórmula del producto escalar de la cual puedes despejar $\alpha$: el ángulito
 en cuestión:
 
 ```math
 \vec{v_1} \cdot \vec{v_2} = \left| \vec{v_1} \right| \cdot \left| \vec{v_2} \right| \cdot cos \alpha
 ```*/
// Calcular el ángulo es cos alfa = producto escalar/producto de los módulos

#define DIM 3

    int r[] = {2, 3, 4};
    int s[] = {1, 2, 3};

int calcula_producto (int r[], int s[]){ //Función para calcular el producto escalar.
    int operacion[DIM];
    int resultado = 0;
    int producto = 0;

    for(int i = 0; i < DIM; i++) {
      operacion[i] = r[i] * s[i];
      resultado = operacion[i];
      producto += resultado;
    }
  return producto;
}

int p_modulo ()
