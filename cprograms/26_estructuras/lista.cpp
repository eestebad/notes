#include <stdio.h>
#include <stdlib.h>

/*LISTAS ENLAZADAS. PROGRAMA QUE RECOGE LOS DATOS DE VARIOS EMPLEADOS UTILIZANDO ESTRUCTURAS Y MALLOC. DE MOMENTO SÓLO LO HACE UNA VEZ*/

#define MAX 0x30

struct TEmpleado {
    char nombre[MAX];
    char apellidos[MAX];
    double salario;
    struct TEmpleado *siguiente;
};

const char *opciones[] = {
    "Altas",

    "Salir",
    NULL
};

void title () {
    system ("clear");
    system ("toilet -fpagga --gay 'GESTION DE USUARIOS'");
    printf ("\n\n");
}

unsigned menu () {
    unsigned opcion;
    static unsigned n_opciones = sizeof(opciones) / sizeof (char *); //Al ser static, conserva su valor después cuando se usa
    // en el do while. La parte del sizeof es para darle el tamaño que ocupa opciones y saber cuándo parar en el do while.
    printf ("----------> %i\n", n_opciones);

    do { //Imprime el menú y guarda la opción que da el usuario en opcion. Se repite mientras no dé un nº de opción válido: 1 ó 2.
        // Altas o Salir.
        title ();
        printf ("Menu\n");
        printf ("====\n");
        printf ("\n");

        for (int op=0; opciones[op] != NULL; op++)
            printf ("\t%i.- %s\n", op + 1, opciones[op]);
        printf ("\n");
        printf ("\tOpcion: ");
        scanf (" %u", &opcion);
        opcion--;
    } while (opcion >= n_opciones - 1); //n_opciones es lo que ocupa opcion. Ponemos -1 para que tenga en cuenta el NULL, porque
    // sino, daría como opción válida el 3 que es NULL.

    return opcion;
}

void datos_empleado (struct TEmpleado *d) { // Recibe un puntero a una estructura.
    title ();
    printf ("Nombre: ");
    scanf (" %s", d->nombre); // Accedo al campo nombre de aquello a lo que apunta d (una de las estructuras de empleado).
    printf ("Apellidos: ");
    scanf (" %s", d->apellidos);
    printf ("Salario: ");
    scanf (" %lf", &d->salario); // Ponemos &d porque salario no es un array, no es puntero.
}

void altas (struct TEmpleado **inicio) {
    struct TEmpleado *ultimo = *inicio; // Puntero para avanzar los empleados.
    struct TEmpleado *nuevo = (struct TEmpleado *) malloc (sizeof (struct TEmpleado)); // nuevo apunta al malloc.
    datos_empleado (nuevo);
    nuevo->siguiente = NULL; // Decimos que no apunte a nada porque es el último empleado dado de alta y en las listas enlazadas
    // la última estructura apunta a NULL.
    //
    // ---   ---   ---
    // | |-->| |-->| |-->NULL
    // ---   ---   ---
    //


    if (!ultimo){ // es lo mismo que poner if(ultimo == NULL)
        *inicio = nuevo;
        return;
    }

    while (ultimo->siguiente != NULL) //Este while funciona a partir del 2º elemento, no con el 1º.
        ultimo = ultimo->siguiente;
    ultimo->siguiente = nuevo;

    /* l es nuevo
    l => Dir 1º empleado
    l->siguiente => Dir 2º empleado
    l->siguiente->siguiente => Dir 3º empleado
    */
}

void (*fn[]) (struct TEmpleado **inicio) = { &altas }; //Los dos ** de inicio, sólo se ponen en la declaración. Recibe la dirección
// de la dirección de un empleado. Aquello a lo que apunta inicio, es l.

void liberar(struct TEmpleado *l){ // Función recursiva para liberar la memoria reservada con malloc. Libera desde el último al 1º,
    // porque si liberase el 1º antes, ya no sabe dónde está lo demás porque pierde el primer puntero que está en l.

    if (l->siguiente)
        liberar (l->siguiente);

    free (l);
}

int main (int argc, char *argv[]) {
    struct TEmpleado *l = NULL;
    unsigned opcion;

    opcion = menu ();
    (*fn[opcion]) (&l); // Llamada a función altas, usando un puntero a funciones. Le pasamos la dirección de lista, l es la dirección
    // del primer empleado.
    liberar(l);

    return EXIT_SUCCESS;
}

