#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*Define la estructura TCordenada con los campos x e y. Define las variables posición y velocidad. Pregúntale al usuario por sus valores. Define un bucle infinito que incremente los campos de posición con los valores de los campos de velocidad a cada vuelta. Imprime también a cada vuelta los valores de posición y velocidad y usa usleep para que le dé tiempo al usuario de visualizar lo imprimido.*/

struct TCoordenada {
    double x;
    double y;
};

struct TCoordenada preguntar (const char *item) {
    struct TCoordenada info_recogida;

    printf ("%s: \n", item);
    printf ("\n");
    printf ("%s.x = ", item);
    scanf ("%lf", &info_recogida.x);
    printf ("%s.y = ", item);
    scanf ("%lf", &info_recogida.y);

    printf ("\n");
    printf ("\n");

    return info_recogida;
}

void mostrar (const char *etiqueta, struct TCoordenada data) {
    printf ("%s: (%.2lf, %.2lf)\n", etiqueta, data.x, data.y);
    printf ("\n");
}

int main (int argc, char *argv[]) {

    struct TCoordenada posicion, velocidad;

    posicion = preguntar ("Posción");
    velocidad = preguntar ("Velocidad");

    while (1) {
        posicion.x += velocidad.x;
        posicion.y += velocidad.y;
        system ("clear");
        mostrar ("Posición", posicion);
        mostrar ("Velocidad", velocidad);
        usleep (100000);
    }

    return EXIT_SUCCESS;
}

