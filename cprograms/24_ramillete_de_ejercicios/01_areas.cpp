#include <stdio.h>
#include <stdlib.h>

/*Programa que calcule área de triángulo o cuadrado según indique el usuario. Utilizar puntero a funciones*/

enum TOpcion {triangulo, cuadrado};
const char *nombre[] = {"triángulo", "cuadrado"};

double tri (double base, double alt) { return base * alt / 2; }
double cua (double base, double alt) { return base * alt    ; }
double error (double, double) {
    fprintf (stderr, "No sé qué figura usar.\n");
    exit (1);
}

double (*area_disp[]) (double, double) = {&tri, &cua, &error};


void titulo () {
    system ("clear");
    system ("toilet -fpagga --gay AREA");
    printf ("\n\n");
}

enum TOpcion menu (){ //En C++ no es necesario indicar que es enum, pero en C sí.
    int opcion;

    titulo ();
    printf (
            "\t1.- Triángulo\n"
            "\t2.- Cuadrado\n"
            "\n"
            "\tOpcion: ");
    scanf (" %i", &opcion);
    opcion--;

    return (enum TOpcion) opcion; // Las variable enum no se pueden poner en el scanf porque no reconoce los límites, no funcionaría.
                                  // Así que 1º declaramos opcion y una vez hecho el scanf, le hacemos un molde a enum.
}

void preguntar_dim (double *base, double *alt) { //El paso de base y alt a la función, se hace por referencia (su dirección, no su valor)
    //porque los va a devolver modificados. Por eso se pasan con un * delante aunque no se hayan declarado con él. No es necesario en 
    //este caso, poner el & en el scanf, porque lo que estoy pasando ya es un puntero.
    printf ("Base: ");
    scanf (" %lf", base);
    printf ("Altura: ");
    scanf (" %lf", alt);
}

int main (int argc, char *argv[]) {

    double base, alt;
    double (*area)(double, double) = &error;

    enum TOpcion opcion = menu ();
    area = area_disp[opcion];

    preguntar_dim (&base, &alt);
    //Funciona igual con esta línea que con la siguiente. ¿Para qué utilizar (*area) entonces?
    printf ("Area del %s = %.2lf\n", nombre[opcion], (*area_disp[opcion])(base, alt));
    //printf ("Area del %s = %.2lf\n", nombre[opcion], (*area)(base, alt));
    return EXIT_SUCCESS;
}

