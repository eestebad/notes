#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*11. Haz una función recursiva que calcule:

s(n) = 1 - x + \frac{x^2}{2} - \frac{x^3}{6} + \frac{x^4}{24} + \dots + (-1)^n \frac{x^n}{n!}*/

#define N 10

int factorial (int n) {
    if ( n == 0 )
        return 1;

    return n * factorial (n-1);
}

void s (int n, double coef[N]) {
    double nuevo = pow (-1, n) / factorial (n);

    coef[n] = nuevo;

    if ( n > 0 )
        s (n-1, coef);
}

int main (int argc, char *argv[]) {

    double coef[N];

    s (N-1, coef /*Array = paso por referencia */);

    printf ("[  ");
    for (int i=0; i<N; i++)
        printf ("%.4lf  ", coef[i]);
    printf ("  ]\n");

    return EXIT_SUCCESS;
}

