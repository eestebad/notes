#include <stdio.h>
#include <stdlib.h>

#define N 6

/* Imprimir ordenados los elementos de un array de enteros, sin cambiarlos de celda, utilizando un array de punteros a enteros. */

void muestra(int *a[N]){
    for(int i = 0; i < N; i++)
        printf("%i ", *a[i]);
    printf("\n");
}

int main(){
    int A[N] = {2, 5, 3, 7, 4, 1}; //Array normal
    int *o[N]; // Array de punteros a enteros.

    // Rellenamos el array de punteros, con las direcciones de las celdas del array normal.
    for(int i = 0; i < N; i++)
        o[i] = &A[i]; //Guardo en la celda del array de punteros, la dir de memoria de cada celda del array de enteros.

    muestra(o);
    // Ordenamos el array de punteros con el método de la burbuja.
    for(int p = 0; p < N-1; p++)
        for(int i = 0; i < N-1; i++)
            if(*o[i] < *o[i+1]){ //Ponemos el * pq quiero comparar el contenido de aquello a lo que apunta o.
                int *aux = o[i+1]; //No ponemos el * pq quiero intercambiar las direcciones de memoria.
                o[i+1] = o[i];
                o[i] = aux;
            }

    muestra(o);

    return EXIT_SUCCESS;
}
