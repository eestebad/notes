#include <stdio.h>
#include <stdlib.h>

/*Declaramos una variable int, la iniciamos con un valor entre 1 y 10. Por teclado solicitamos un nº y vemos si hemos acertado */
int main () {

    int i = 8;
    int n;

    printf ("Introduzca un nº del 1 al 10: ");
    scanf (" %i", &n);

    if (n == i) {
        printf ("Acertaste, mi nº es %i\n", i);
    }
    else if (n > i) {
        printf ("Tu nº %i es mayor que mi nº\n", n);
    }
    else {
        printf ("Tu nº %i es menor que el mío\n", n);
    }

    return EXIT_SUCCESS;
}
