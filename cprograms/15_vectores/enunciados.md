# Enunciados

## Problema 1
Hacer un programa que pueda leer un vector de dimensión desconocida.

Es decir, las siguientes serán entradas válidas:
```
[1, 3]
(1, 3)
[1, 3, 5]
(2.5, 9, 3.7, 11)
```
 Guarda la dimensión en la variable d, y el vector en v.
 
 ```c
 
 #define MAX 0x10
 
 unsigned d;
 double v[MAX];
 ```
 
 ## Problema 2
 Haz un programa que calcule el modulo de un vector. Sea:
 
 ```math
 \vec{v_1}=(a_1,b_1,c_1)
 ```
 
 entonces:
 
 ```math
 v = \left| \vec{v_1} \right| = \sqrt{a_1^2 + b_1^2 + c_1^2}
 ```
 
 ## Problema 3
 Haz un programa que calcule el producto escalar de dos vectores.
 Sean dos vectores:
 
 ```math
 \vec{v_1}=(a_1,b_1,c_1)
 ```
 
 ```math
 \vec{v_2}=(a_2,b_2,c_2)
 ```
 
 entonces, su producto escalar es:
 
 ```math
 \vec{v_1} \cdot \vec{v_2} = a_1 \cdot a_2 + b_1 \cdot b_2 +c_1 \cdot c_2
 ```
 
 que es un número real.
 
 ## Problema 4
 Como ya sabes pedir vectores, calcular el módulo de vectores y hacer su producto 
 escalar, esto te puede servir para calcular el ángulo que forman, ya que existe
 una segunda fórmula del producto escalar de la cual puedes despejar $\alpha$: el ángulito
 en cuestión:
 
 ```math
 \vec{v_1} \cdot \vec{v_2} = \left| \vec{v_1} \right| \cdot \left| \vec{v_2} \right| \cdot cos \alpha
 ```
 
 Confecciona dicho programa.
