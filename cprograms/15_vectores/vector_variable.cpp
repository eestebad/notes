#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Problema 1
  leer un vector de dimensión desconocida, con esta forma (3.4 5.1 4.1)*/

int main () {

    double *v1 = NULL;
    static int dim = 0;
    double buffer;
    char end;

    printf ("Introduzca las coordenadas del vector (2.1 3.4 6.2): ");
    scanf ("%*[(]");

    do {
        v1 = (double*) realloc (v1, (dim+1) * sizeof(double));
        scanf (" %lf", &buffer);
        v1[dim++] = buffer;
    } while (!scanf(" %1[)]", &end));

    printf("(");

    for (int componente = 0; componente < dim; componente++)
        printf (" %6.2lf", v1[componente]);

    printf (")\n");



    free (v1);

    return EXIT_SUCCESS;
}
