#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Haz un programa que calcule el producto escalar de dos vectores.
   Sean dos vectores:
   v1⃗=(a1,b1,c1)
   v2⃗=(a2,b2,c2)
   entonces, su producto escalar es:
   v1⃗⋅v2⃗=a1⋅a2+b1⋅b2+c1⋅c2
   que es un número real. */

int main () {
    //Declaro los vectores y su contenido:
    double v1[] = {2.3, 3.4, 5.5};
    double v2[] = {1.2, 6.7, 8.9};
    //Declaro producto para guardar la operación:
    double producto;
    //Bloque de cálculo:
    producto = (v1[0] * v2[0]) + (v1[1] * v2[1]) + (v1[2] * v2[2]);
    //Bloque salida de datos:
    printf ("El producto escalar de v1*v2 es: %lf\n", producto);

    return EXIT_SUCCESS;
}
