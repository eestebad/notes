#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*Dado un vector de componentes en coordenadas cilíndricas, pasarlas a coordenadas cartesianas*/

#define R 0
#define T 1
#define Z 2
#define X 0
#define Y 1
#define PI 3.1415926535
#define MAX 3

int main () {

    double cilin[MAX];
    double cart[MAX];

    //Solicito las coordenadas al usuario:
    for (int i = 0; i < MAX; i++) {
        printf ("Introduzca las coordenadas cilíndricas del vector: ");
        scanf (" %lf", &cilin[i]);
    }
    //Las imprimo:
    printf ("Las coordenadas introducidas son: \n");
    for (int i = 0; i < MAX; i++)
        printf (" %lf,", cilin[i]);

    //El ángulo me lo dan en grados, lo paso a radianes para operar con cos y sin y de nuevo lo paso a grados:
    //Calculo X e Y y guardo Z en cilin[3]:
    cart[X] = cart[X] / 180 * PI;
    cart[X] = cilin[R] * cos(cart[X]);
    cart[X] = cart[X] * 180 / PI;
    cart[Y] = cart[Y] / 180 * PI;
    cart[Y] = cilin[R] * sin(cart[Y]);
    cart[Y] = cart[Y] / 180 * PI;
    // cart[X] = cilin[R] * cos(cilin[T] / PI * 180);
    // cart[Y] = cilin[R] * sin(cilin[T] / PI * 180);
    cart[Z] = cilin[Z];

    printf ("\n");

    printf ("cartesianas = (%.2lf, %.2lf, %.2lf)\n", cart[X], cart[Y], cart[Z]);


    return EXIT_SUCCESS;
}

