#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Haz un programa que calcule el modulo de un vector. Sea:
  v1⃗=(a1,b1,c1)
entonces:
v=∣v1⃗∣= sqrt(a12+b12+c12)*/

int main () {

    double v1[] = {2., 5., 3.};
    double v;

    v = sqrt(pow(v1[0],2) + pow(v1[1],2) + pow(v1[2],2));

    printf ("El módulo del vector es: %.2lf\n", v);

    return EXIT_SUCCESS;
}
