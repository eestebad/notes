#include <stdio.h>
#include <stdlib.h>

/* Hacer un programa que pueda leer un vector de dimensión desconocida.

Es decir, las siguientes serán entradas válidas:
```
[1, 3]
(1, 3)
[1, 3, 5]
(2.5, 9, 3.7, 11)
```
 Guarda la dimensión en la variable d, y el vector en v.
 
 ```c
 
 #define MAX 0x10
 
 unsigned d;
 double v[MAX];
 ```*/

int main(){
    double buffer;
    double *vec = NULL;
    static int dim = 0;
    char end;

    printf("ej: (1.5 2 3.7).\tVector: ");
    scanf(" %*[(]");
    do{
        vec = (double *) realloc(vec, (dim+1) * sizeof(double));
        scanf(" %lf", &buffer);
        vec[dim++] = buffer;
    }while (!scanf(" %1[)]", &end));

    printf("\n\t( ");
    for (int componente=0; componente<dim; componente++)
        printf("%6.2lf", vec[componente]);
    printf(" )\n");

    free(vec);
    return EXIT_SUCCESS;
}

