#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Hacer un programa que pueda leer un vector de dimensión desconocida.
  Es decir, las siguientes serán entradas válidas:
  [1, 3]
  (1, 3)
  [1, 3, 5]
  (2.5, 9, 3.7, 11)
  Guarda la dimensión en la variable d, y el vector en v.*/

#define MAX 0x10

int main () {

    static int dim = 0;
    double *vector = NULL;
    double i;
    double buffer;
    char end;

    printf("Introduzca las coordenadas (1.2 3.2 5.6) para el vector: ");
    scanf ("%*[(]");

    do {
        vector = (double*) realloc (vector, (dim+1) * sizeof(double));
        scanf ("%lf", &buffer);
        vector[dim++] = buffer;
    }
    while (!scanf(" %1[)]", &end));

    printf ("\n\t(");

    for (int componente = 0; componente < dim; componente++)
        printf (" %6.2lf", vector[componente]);

    printf (" )\n");

    free (vector);

    return EXIT_SUCCESS;
}

