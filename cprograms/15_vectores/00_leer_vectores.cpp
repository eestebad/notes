#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Programa que recoge por pantalla un máximo de valores para un vector y luego los imprime.*/

int main () {

    int n;
    int i;

    printf ("Introduzca el nº de elementos del vector: ");
    scanf (" %i", &n);

    int v[n];

    for (i = 0; i < n; i++) {
        printf ("Introduce valor para el vector:\n");
        scanf (" %i", &v[i]);
    }

    printf ("\n");

    for (i = 0; i < n; i++) 
        printf (" %i\n", v[i]);


    return EXIT_SUCCESS;
}
