#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Problema 4
  Como ya sabes pedir vectores, calcular el módulo de vectores y hacer su producto 
  escalar, esto te puede servir para calcular el ángulo que forman, ya que existe
  una segunda fórmula del producto escalar de la cual puedes despejar $\alpha$: el ángulito
  en cuestión:

  ```math
  \vec{v_1} \cdot \vec{v_2} = \left| \vec{v_1} \right| \cdot \left| \vec{v_2} \right| \cdot cos \alpha
  ```
  La fórmula del ángulo es: cos alfa = producto escalar de los vectores / producto de sus módulos.
  Confecciona dicho programa.*/

#define DIM 3

int main () {

    double r[DIM];// = {1, 2, 3};
    double s[DIM];// = {4, 5, 6};
    double operacion[DIM];
    double p_escalar = 0;
    double buffer_r;
    double buffer_s;
    static int dim_r = 0;
    static int dim_s = 0;

    printf("Introduzca las componentes del vector r (2.1 3.5 4.2): \n");

    for(int c = 0; c < DIM; c++){
        scanf( " %lf", &buffer_r);
        r[dim_r++] = buffer_r;
    }

    printf("Las componentes de r son: %5.2lf, %5.2lf, %5.2lf\n", r[0], r[1], r[2]);

    
    printf("Introduzca las componentes del vector s (2.1 3.5 4.2): \n");
    for( int k = 0; k < DIM; k++ ){
        scanf(" %lf", &buffer_s);
        s[dim_s++] = buffer_s;
    }

    //Imprimo las componentes de s:

    printf("Las componentes de s son: ");
    for(int j = 0; j < DIM; j++)
        printf("%5.2lf", s[j]);

    printf("\n");

    for(int i = 0; i < DIM; i++){
        operacion[i] = r[i] * s[i];
        p_escalar += operacion[i];
    }

    printf("%.2lf\n", p_escalar);

    double modulo_r = 0;
    double modulo_s = 0;
    modulo_r = sqrt( pow(r[0],2) + pow(r[1], 2) + pow(r[2], 2) );
    modulo_s = sqrt( pow(s[0], 2) + pow(s[1], 2) + pow(s[2], 2) );

    printf("modulo_r= %5.2lf y modulo_s= %5.2lf\n", modulo_r, modulo_s);

    //Cálculo de coseno alfa:
    double p_modulos = 0;
    double arcocoseno = 0;
    double coseno_a = 0;

    p_modulos = modulo_r * modulo_s;

    printf("Porducto de los módulos: %5.2lf\n", p_modulos);

    arcocoseno = p_escalar / p_modulos;
    printf("Arcocoseno: %5.2lf\n", arcocoseno);

    coseno_a = acos(arcocoseno);

    printf("Ángulo = %10.8lf radianes\n", coseno_a);

    return EXIT_SUCCESS;
}
