#include <stdio.h>
#include <stdlib.h>

#define DIM 3
//menú para elegir a qué base quiero cambiar, con un valor centinela:
const char *const bases[] = {
    "cartesianas",
    "cilíndricas",
    "esféricas",
    NULL
};
//componentes es array bi de 3 filas y 3 columnas. 
const char * const componentes [][DIM] = { //sólo puedes dejar el primer corchete vacío.
    {"X", "Y", "Z"}, //fila 0
    {"Ro", "Theta", "Z"}, //fila 1
    {"R", "Theta", "Phi"} //fila 2
};

enum TBase {cart, cil, esf, BASES}; //enum; declara las constantes, valen 0, 1, 2 y 3

void title () {
    system ("clear");
    system ("toilet -fpagga --gay VEC3D");
    printf ("\n\n");
}

void print_options (const char * const estadio) {  //no puedes cambiar puntero ni contenido
    const char **base = (const char **) bases; 

    title ();

    printf ("Elige la base %s:\n", estadio);
    printf ("\n");
    printf ("\n");

    for (int i=0; *base!=NULL; base++, i++)
        printf ("\t%i.- %s\n", i+1, *base);

    printf ("\n");
    printf ("\n");
}

enum TBase ask_option () { //devuelve un enum
    unsigned eleccion;

    do {
        printf ("                                                       \r");
        printf ("    Opción: ");
        scanf (" %u", &eleccion); //PASO POR REFERNCIA
        printf ("\x1B[1A"); //sube cursor una línea
    } while (eleccion <= 0 || eleccion > BASES); //lo repito mientras la opción no sea 0, 1, 2

    printf ("\n");
    eleccion--;//decremento para que las opciones sean 1, 2, 3 

    return (enum TBase) eleccion; //moldeo eleccion para que sea enum
}

void menu (enum TBase *src, enum TBase *dst) { //src va a recibir la dirección de un enum TBase
    print_options ("inicial");
    *src = ask_option ();
    print_options ("destino");
    *dst = ask_option ();
}

void ask_vector (enum TBase base, double vector[DIM]) {
    printf ("Componente: ");
    printf ("\n");
    printf ("\n");

    for (int c=0; c<DIM; c++) {
        printf (" %s: ", componentes[base][c]);
        scanf (" %lf", &vector[c]);
    }
}

int main (int argc, char *argv[]) {

    enum TBase srcbas, dstbas; //del tipo enum TBase genera 2 variables que albergan la base de origen (source) y la de destino (destiny)- Pueden valer cartesianas, cilindricas... lo definido . Dice pasa de cilin a cart o lo que sea
    double src_vec[DIM], dst_vec[DIM]; //vector que me da el user y el que calculo yo. Tienen 3 celdas de double.

    menu ( &srcbas, &dstbas ); //imprimo menú. Tengo que pasarlo por referencia pq una función no puede devolver más de un valor de retorno.
    title ();
    ask_vector (srcbas, src_vec);

    if (srcbas == dstbas)

    return EXIT_SUCCESS;
}

