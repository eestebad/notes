#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*Dado un vector en coordenadas cartesianas de tres componentes, convertir a un vector en coordenadas cilíndricas */

#define X 0
#define Y 1
#define Z 2
#define R 0
#define T 1 //es Theta.
#define MAX 3
#define PI 3.1415926535

int main () {

    double cart[MAX];
    double cilin[MAX];

    //Solicito las tres coordenadas:
    for (int i = 0; i < MAX; i++) {
        printf ("Introduzca las coordenadas cartesianas del vector: ");
        scanf("%lf", &cart[i]);
    }

    printf ("\n");

    //Las imprimo para ver que se han guardado ok:
    for (int i = 0; i < MAX; i++)
        printf ("Las coordenadas son: %lf\n", cart[i]);

    printf ("\n");

    //Calculo el módulo de la sombra del vector cart:
    // cilin[R] = sqrt (cart[X]*cart[X] + cart[Y]*cart[Y]);
    //Calculando el cuadrado con la función pow:
    cilin[R] = sqrt (pow(cart[X],2) + pow(cart[Y],2));
    //Imprimo el resultado:
    printf ("El módulo de la sombra del vector cart es: %lf\n", cilin[R]);

    printf ("\n");

    //Calculo Theta=T:
    cilin[T] = atan2 (cart[Y], cart[X]) * 180 / PI;
    printf ("El arcotangente de Theta es: %lf\n", cilin[T]);

    printf ("\n");

    cilin[Z] = cart[Z];

    //Imprimos las coordenadas utilizando las variables:
    printf ("Las coordenadas cilíndricas del vector r = (%.2lf, %.2lf, %.2lf)\n", cilin[R], cilin[T], cilin[Z]);


    //Imprimo las coordenadas cilíndricas, una por una:
    /* printf ("En las coordenadas cilíndricas del vector, R = %lf \n", cilin[R] );

       printf ("En las coordenadas cilíndricas del vector, Theta = %lf \n", cilin[T] );

       printf ("En las coordenadas cilíndricas del vector, Z = %lf \n", cilin[Z] );
       */

    return EXIT_SUCCESS;
}
