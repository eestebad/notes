#include <stdio.h>
#include <stdlib.h>

int main () {

    char operador;
    int op1, op2;
    int resultado;
    int op_buena = 0;

    /*Bloque de entrada de datos*/
    printf ("Si quieres sumar, escribe s. Si quieres restar, escribe r: ");
    scanf (" %c", &operador);

    printf ("Operando: ");
    scanf (" %i", &op1);

    printf ("Operando: ");
    scanf (" %i", &op2);

    /*Bloque de cálculo, utilizando una variable de bandera*/
    if (operador == 's') {
        resultado = op1 + op2;
        op_buena = 1;
    }

    if (operador == 'r') {
        resultado = op1 - op2;
        op_buena = 1;
    }

    if (!op_buena) {
        fprintf (stderr, "Opción incorrecta.\n");
        return EXIT_FAILURE;
    }


    /*Salida de datos*/
    printf (" %i %c %i = %i\n",
            op1,
            operador,
            op2,
            resultado);

    return    EXIT_SUCCESS;
}
