#include <stdio.h>
#include <stdlib.h>


/*Se pide al usuario que introduzca un nº cero o uno por cada color y se le informa de qué color ha resultado*/
int main () {
    /*Declaro r, y, b como enteros. Declaro el array color, que contiene los nombres de los colores que se pueden dar*/
    int r, y, b;
    char color [8] [7] = {"red", "yellow", "blue", "black", "orange", "purple", "green", "white"};

    /*Se solicitan al usuario los valores para cada color (red, yellow, blue) y poder determinar qué color resulta*/
    printf ("Introduce parámetro 1 para mezcla de color, sólo 0 ó 1:");
    scanf (" %i", &r);

    printf ("Introduce parámetro 2 para mezcla de color, sólo 0 ó 1:");
    scanf (" %i", &y);

    printf ("Introduce parámetro 3 para mezcla de color, sólo 0 ó 1:");
    scanf (" %i", &b);

    /*Dependiendo de los valores introducidos por el usuario, se asigna uno de los colores del array*/
    if (r == 1 && y == 0 && b == 0)
        printf ("The colour is %s\n", color [0]);

    if (r == 0 && y == 1 && b == 0)
        printf ("The colour is %s\n", color [1]);

    if (r == 0 && y == 0 && b == 1)
        printf ("The colour is %s\n", color [2]);

    if (r == 0 && y == 0 && b == 0)
        printf ("The colour is %s\n", color [3]);

    if (r == 1 && y == 1 && b == 0)
        printf ("The colour is %s\n", color [4]);

    if (r == 1 && y == 0 && b == 1)
        printf ("The colour is %s\n", color [5]);

    if (r == 0 && y == 1 && b == 1)
        printf ("The colour is %s\n", color [6]);

    if (r == 1 && y == 1 && b == 1)
        printf ("The colour is %s\n", color [7]);

    return 0;
}


