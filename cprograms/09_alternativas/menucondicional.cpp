#include <stdio.h>
#include <stdlib.h>


int main () {
    char operador;
    int op1, op2;
    int resultado;

    printf ("Si quieres sumar, escribe s. Si quieres restar escribe r:");
    scanf (" %c", &operador);

    if (operador != 'r' && operador != 's') {
        fprintf (stderr, "Carácter no válido.\n");
        return EXIT_FAILURE;
    }

    printf ("Operando: ");
    scanf (" %i", &op1);

    printf ("Operando: ");
    scanf (" %i", &op2);

    /*Bloque de cálculo*/
    if (operador == 's')
        resultado = op1 + op2;

    if (operador == 'r')
        resultado = op1 - op2;

    /*salida de datos*/
    printf ("%i %c %i = %i\n",
            op1,
            operador == 's'? '+' : '-',
            op2,
            resultado);

    return EXIT_SUCCESS;

}
