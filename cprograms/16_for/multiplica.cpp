#include <stdio.h>
#include <stdlib.h>

/*Programa que calcula la potencia de un nº*/

#define P 3

int main () {

    int num = 7;
    int potencia = 1;

    for(int i = 0; i < P; i++)
        potencia *= num;

    printf("La potencia de %i del número %i es = %i\n", P, num, potencia);

    return EXIT_SUCCESS;
}
