#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    int base;
    unsigned exponente;
    int potencia = 1;
    int multiplicacion;

    printf ("Base: ");
    scanf (" %i", &base);
    printf ("Exponente: ");
    scanf (" %u", &exponente);

    for (unsigned mul=0; mul<exponente; mul++) {
        multiplicacion = 0;
        for (int sum=0; sum<base; sum++)
            multiplicacion += potencia;
        potencia = multiplicacion;
    }


    printf ("%i^%u = %i\n", base, exponente, potencia);


    return EXIT_SUCCESS;
}

