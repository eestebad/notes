#include <stdio.h>
#include <stdlib.h>

/*hallar todos los nº primos que hay entre el 1 y el nº dado por el usuario */


int main() {

    unsigned numero = 0;
    unsigned divisor = 0;
    unsigned j = 1;

    printf ("Introduce un nº: ");
    scanf(" %u", &numero);
    printf("\n");

    for(; j <= numero; j++){
          for (int i = 1; i <= j; i++){
            if(j % i == 0)
                divisor++;
        }
        if(divisor == 2)
            printf("El nº %i es primo\n", j);
        else
            printf("El nº %i no es primo\n", j);
    }

    return EXIT_SUCCESS;
}
