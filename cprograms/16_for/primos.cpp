#include <stdio.h>
#include <stdlib.h>

/*Decir si un nº dado por el usuario es primo o no */


int main() {

    unsigned numero = 0;
    unsigned divisor = 0;

    printf ("Introduce un nº: ");
    scanf(" %u", &numero);
    printf("\n");

    for (int i = 1; i <= numero; i++){
        if(numero % i == 0)
            divisor++;
    }
    if(divisor == 2)
        printf("El nº %i es primo\n", numero);
    else
        printf("El nº %i no es primo\n", numero);

    return EXIT_SUCCESS;
}
