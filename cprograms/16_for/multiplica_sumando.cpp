#include <stdio.h>
#include <stdlib.h>

/*Programa que calcula la potencia de un nº sumando*/


int main () {

    int exponente = 2;
    int num = 3;
    int potencia = 1;
    int multi;
    /*El primer for pone multi a cero y entra al 2º siempre que i sea menor que el exponente. En ese caso se ejecuta de nuevo el 2º for, que cuando acaba, ejecuta meter en potencia lo que vale multi*/
    for(int i = 0; i< exponente; i++){
        multi = 0;
        for(int u = 0; u < num; u++)
            multi += potencia;

        potencia = multi; //esta línea se ejecuta cuando ha terminado el segundo for. Metemos en potencia lo que hay en multi y volvemos al primer for.
    }

    printf("La potencia de %i del número %i es = %i\n", exponente, num, potencia);

    return EXIT_SUCCESS;
}
