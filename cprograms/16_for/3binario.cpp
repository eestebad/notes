#include <stdio.h>
#include <stdlib.h>

int main (){

    char *nombre;
    unsigned numero = 13;
    unsigned i;
    unsigned u;
    unsigned operacion[4];
    unsigned n_binario = 0;

//    printf ("Introduce un nº para pasarlo a binario: \n");
  //  scanf (" %u", &numero);

    for ( i = 1, u = 0; numero > 0, u < 4; numero <<=1, i*10, u++ )
        operacion[u] = (numero % 2) * i;

    n_binario = operacion[0] + operacion[1] + operacion[2] + operacion[3];

    printf ("El nº %u en binario es: %u \n", numero, n_binario);

    return EXIT_SUCCESS;
}
