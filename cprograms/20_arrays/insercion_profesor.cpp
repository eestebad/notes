#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
1. Preguntar números positivos al usuario. Calcular la mediana. La mediana es aquél número que divide a la población (el conjunto de números) por la mitad. Básicamente el elemento del medio de un array cuando éste está ordenado. El programa dejará de pedir números cuando el usuario introduzca un número negativo. En la versión uno usa el método de inserción y en la dos, el de la burbuja. */

#define N 4
#define M 20

void muestra (int a[N]) {
    for(int i=0; i < N; i++)
        printf("%i ", a[i]);
    printf("\n");
}

int main (int argc, char *argv[]){
    int A[N] = {7, 12, 17, 9};
    int menor;
    /*Inicialización*/
/*    srand(time (NULL)); //Función para rellenar con números aleatorios. En este caso, con time, desde la fecha de inicio de nuestro tiempo(algo de 1970), en segundos.
    for(int i=0; i < N; i++)
        A[i] = rand () % M + 1; // Números entre 1 y 20 para rellenar el array.
*/
    muestra (A);
    /* Ordenación de inserción */
    for(int buscando = 0; buscando < N - 1; buscando++){
        menor = buscando;
        for(int i = buscando + 1; i < N; i++)
            if(A[i] < A[menor])
                menor = i;
        if(menor > buscando){
            int aux = A[buscando];
            A[buscando] = A[menor];
            A[menor] = aux;
        }
    }
    /* Fin de la ordenación */
    muestra (A);

    return EXIT_SUCCESS;
}
