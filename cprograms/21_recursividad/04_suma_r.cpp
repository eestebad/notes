#include <stdio.h>
#include <stdlib.h>

/* Uso de funciones recursivas. Este programa imprime la suma de los 10 primeros números naturales. Se puede hacer de forma iterativa o recursiva*/

#define N 10

// Versión iterativa
unsigned int suma (unsigned int n) {
    unsigned int total = 0;
    for (unsigned int i=1; i<=n; i++)
        total += i;

    return total;
}

//Versión recursiva y la utilizada en este programa:
unsigned int suma_r (unsigned int n) {

    if (n<1)
        return 0;

    return n + suma_r (n-1);
}

int main (int argc, char *argv[]) {

    printf ("La suma de los %u primeros números: %u\n", N, suma_r (N)); /*En la llamada suma_r  ( N )  N vale 10 porque esta en el define. Este 10 que se pasa en la llamada se llama parámetro actual
en la definición de la función pone unsigned n. Este es el parámetro formal (un espacio en el que se guarda el parámetro actual)*/

    return EXIT_SUCCESS;
}
