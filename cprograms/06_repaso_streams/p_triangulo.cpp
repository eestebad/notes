#include <stdio.h>
#include <stdlib.h>

int main () {

    unsigned altura;
    char simbolo;
    printf ("Altura: ");
    scanf (" %u", &altura);
    printf ("Introduzca el símbolo: ");
    scanf ("%c", &simbolo);
  
    for (int f = 0; f < altura; f++) {
        for (int c = 0; c < f + 1; c++)
            printf ("%c", simbolo);
        printf ("\n");

    }
    return EXIT_SUCCESS;
}
