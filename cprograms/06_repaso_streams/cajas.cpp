#include <stdio.h>
#include <stdlib.h>

const char *simbolo = "♠";

int main() {
    unsigned altura;

    //Saber cuál es la altura del cuadrado
    printf ("Altura: ");
    scanf (" %u", &altura);

    //Pintar cada línea
    for (int f = 0; f < altura; f++) {
        //Pintar cada símbolo
        for (int c = 0; c < altura; c++)
            if (f == 0 || c == 0 ||
                    f == altura -1 || c == altura -1)
                printf (" %s", simbolo); //al tener espacios delante de %s no hace el cuadrado
            else 
                printf (" ");
        //Imprimir un salto de línea
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
