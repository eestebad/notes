
#include <stdio.h>
#include <stdlib.h> /*incluye el uso de la variable EXIT_SUCCESS que es lo mismo que poner 0*/

int base;

int main() {

    printf ("Introduzca la base: ");

    scanf ("%i", &base);

    for (int i = 0; i < base; i++)
        printf ("*");
    printf ("\n");


    return EXIT_SUCCESS;
}
