#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <strings.h>
#include <time.h>
#include <unistd.h>

// Comenta la siguiente línea para ver los vecinos de cada celda.
//#define NDEBUG

#define TIEMPO      45     /* Número de segundos que va a funcionar el programa */
#define POB_INI     0.20   /* Tanto por 1 el tablero que ha de ser poblado */


#define TITLE_SPACE 8      /* Lo que ocupa el título */
#define MARGIN      15     /* Margen a los lados del cuadro */

int filas, cols;            // Filas y columnas de la pantalla.
                            // Lo hacemos un recurso global, puesto que la pantalla
                            // es un recurso global.

int valor (int *m, int f, int c) { /* Método accedente que tiene en cuenta que si f y c son valores válidos. */
    if ( f<0 || f>= filas || c<0 || c>= cols )
        return 0;

    return *(m + f * cols + c);
}

int vecinos (int *m, int f, int c) { /* Cuenta la cantidad de vecinos de una célula */
    int suma = 0;

    for (int s=-1; s<2; s++)      /* Filas superior, propia e inferior */
        for (int t=-1; t<2; t++)  /* Columnas anterior, propia y posterior*/
            suma += valor (m, f + s, c + t);

    suma -= valor(m, f, c);       /* El bucle suma el valor de esta celda y ahora lo quitamos. */

    return suma;
}

void computa (int *fuente, int *albaran) {   /* Calcula el número de vecinos que tiene una célula. */
    for (int f=0; f<filas; f++)
        for (int c=0; c<cols; c++)
            *(albaran + f * cols + c) = vecinos (fuente, f, c);
}

void actualiza (int *celula, int *computo) { /* Pinta el tablero de la generación siguiente. */
    for (int f=0; f<filas; f++)
        for (int c=0; c<cols; c++) {
            int vecinos = *(computo + f * cols + c);
            if ( vecinos != 2 && vecinos !=3 )        // Entender este if else if es la única gracia de todo el
                *(celula + f * cols + c) = 0;         // programa. Asume que vecinos toma distintos valores y
            else if (vecinos == 3)                    // haz un seguimiento.
                *(celula + f * cols + c) = 1;
        }
}

void title () {
    system ("clear");
    system ("toilet -f pagga --gay '    Juego de la Vida   '");
    printf ("\n\n");
}

void pinta (int *m, int *p) { /* Pintamos el array en pantalla */
    title();

    for (int f=0; f<MARGIN-TITLE_SPACE; f++)
             printf("\n");

     for (int f=0; f<filas; f++) {
                  for (int c=0; c<MARGIN; c++)
             printf(" ");
         for (int c=0; c<cols; c++)
             if ( *(m + f * cols + c) ) {
                 printf ("\x1B[7m");              // Vídeo inverso
#ifndef NDEBUG
                 printf ("%i", *(p + f * cols + c));
#else
                 printf (" ");
#endif
                 printf ("\x1B[0m");             // Volver a invertir video
             } else {
#ifndef NDEBUG
                 printf ("%i", *(p + f * cols + c));
#else
                 printf (" ");
#endif
             }
         printf ("\n");
     }
}

void rellena_aleatorio (int poblacion_ini, int *celula) {
    srand (time (NULL));                                       // Cambiar la posición del siguente número a sacar de una sucesión.

    /* Rellenamos con valores aleatorios */

    for (int i=0; i<poblacion_ini; /* Sólo incrementamos si somos capaces de activar la célula */ ) {
        int f = rand () % filas; // rand () obtiene el siguiente valor de una sucesión de números ya definida en el sistema.
        int c = rand () % cols;  // Son número pseudo aleatorios. (parece que lo son y no lo son).
        if (*(celula + f * cols + c) == 0) {
            *(celula + f * cols + c) = 1;
            i++;                 // El incremento del bucle.
        }
    }
}

void rellena_cruz (int *celula) {
    *(celula + 1) = 1;
    *(celula + 1 * cols) = 1;
    *(celula + 1 * cols + 1) = 1;
    *(celula + 1 * cols + 2) = 1;
    *(celula + 2 * cols + 1) = 1;
}

int main (int argc, char *argv[]) {
    struct winsize w;           // Estructura que rellanamos con lo que el kernel sabe de la terminal. Las estructuras son los padres de los objetos.
    int *celula, *computo;      // Matrices bidimensionales.
    int poblacion_ini;
    time_t inicio, actual;

    /*********** INICIALIZACION ***********/
    /* Le preguntamos al kernel por la terminal */

    ioctl (0, TIOCGWINSZ, &w);                   // ioctl viene de input output control. Es la función genérica para comunicarse con el kernel de Sist Op.
    filas = w.ws_row - 2 * MARGIN;               // Restamos el tamaño del título.
    cols  = w.ws_col - 2 * MARGIN;               // Columnas de pantalla.
    poblacion_ini = POB_INI * filas * cols;
    inicio = time (NULL);

    /* Reservas dinámicas */

    celula  = (int *) malloc (filas * cols * sizeof (int));    // Reserva de memoria
    computo = (int *) malloc (filas * cols * sizeof (int));
    bzero (celula, filas * cols * sizeof (int));               // Ponemos todo a 0. Acordaos que sizeof (celula) == 8. No hay información sobre las celdas.
    bzero (computo, filas * cols * sizeof (int));
    rellena_aleatorio (poblacion_ini, celula);
    //rellena_cruz (celula);
    computa (celula, computo);     // Analiza detalladamente las llamadas

    /**********      BUCLE      ***********/
    do {                        // Esto es el ALGORITMO.
        actual = time (NULL);
        pinta (celula, computo);
        printf ("\n\n\tTiempo Restante: %lis\n", TIEMPO + inicio - actual);
        computa (celula, computo);     // Analiza detalladamente las llamadas
        actualiza (celula, computo);   // que aquí se hacen.
        usleep (100000);               // Parar una décima de segundo para que se vea la pantalla.
    } while (actual - inicio < TIEMPO);


    /********** PROTOCOLO DE SALIDA *******/
    system ("clear");
    free (computo);             // Liberamos las reservas de memoria.
    free (celula);              // No se nos puede olvidar.

    return EXIT_SUCCESS;
}
