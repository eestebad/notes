#include <stdlib.h>

#include "common.h"
#include "interfaz.h"

int main (int argc, char *argv[]) {

    enum TBase srcbas, dstbas;
    double src_vec[DIM], dst_vec[DIM];

    menu ( &srcbas, &dstbas );
    title ();
    ask_vector (srcbas, src_vec);

    if (srcbas == dstbas) {
        print_result (src_vec, src_vec, srcbas, dstbas);
        return EXIT_SUCCESS;
    }

    return EXIT_SUCCESS;
}


