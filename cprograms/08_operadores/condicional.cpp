#include <stdio.h>
#include <stdlib.h>

#ifndef NUM /*si no está definida NUM, defínela como 3*/
#define NUM 3
#endif

int main () {
    char resultado = NUM % 2 == 0 ? 'p' : 'i';

    printf ("%i => %c\n", NUM, resultado);

    return EXIT_SUCCESS;

}
