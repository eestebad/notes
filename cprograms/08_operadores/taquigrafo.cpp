#include <stdio.h>
#include <stdlib.h>

#define P(...) printf ("MACRO:" __VA_ARGS__ ) /*cuando quiera poner printf pongo P, y al ponerle paréntesis al lado, estoy definiendo una macro*/

int main () {

    int b = 37;

    b %= 5;

    P ("%%: %i\n", b);

    b <<= 2;
    P("<<: %i\n", b);

    return  EXIT_SUCCESS;

}
