#include <stdio.h>
#include <stdlib.h>

#define MAX_ERROR .0000001

int main () {
    double user_number;

    printf ("Number: ");
    scanf ( " %lf", &user_number);

    if (user_number >= 3. - MAX_ERROR && 
        user_number <= 3. + MAX_ERROR)
        printf ("Para mí es un 3 a todos los efectos. \n");
    else
        printf ("Para mí no es un 3. \n");

    return EXIT_SUCCESS;

}
