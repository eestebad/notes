#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main () {
    //Declaro e inicializo dos enteros:
    int x, y;

    x = 10;
    y = 2;
    // Sumo x + y y lo guardo en x.
    x += y;
    printf ("El valor de x+y es %i\n", x); //Imprimo el resultado.
    // Resto x - y, guardándolo en x.
    x -= y;
    printf ("El valor de x-y es %i\n", x);
    // Multiplico x * y, guardándolo en x.
    x *= y;
    printf ("El valor de x*y es %i\n", x);
    // Divido x/y, guardándolo en x:
    x /= y;
    printf ("El valor de x/y es %i\n", x);
    // Calculo el resto de la división x/y:
    //x = x % y;
    //printf ("El resto de dividir x/y es %i\n", x);
    // Calculo y elevado a x y lo guardo en x:
    x = pow (y, x);
    printf ("El valor de y elevado a x es %i\n", x);
    // Calculo la raíz cuadrada de x:
    x = sqrt (x);
    printf ("La raíz cuadrada de x es %i\n", x);
    //Incremento x:
    x++;
    printf ("EL valor de x++ es %i\n", x);
    //Decremento x:
    x--;
    printf ("El valor de x-- es %i\n",x);

    ++x;
    printf ("El valor de ++x es %i\n", x);

    --x;
    printf ("El valor de --x es %i\n", x);



    return EXIT_SUCCESS;
}
