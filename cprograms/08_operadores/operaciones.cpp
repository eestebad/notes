#include <stdio.h>
#include <stdlib.h>

//Programa que solicita un nº al usuario y lo multiplica por el nº pi, imprimiendo el resultado por pantalla.
//Defino la constante de carácter PI.
#define PI 3.1416

int main () {
//definición de variables. x se inicializa a la vez.
    float y;
    float x = PI;
//Se solicita un nº al usuario.
    printf ("Escriba un nº: ");
// el nº dado se guarda en y.
    scanf (" %f", &y);
//Imprimo el nuevo valor de y.
    printf ("El valor de y es %f\n", y);
//multiplico x por y, guardando el resultado en x.
    x *= y;
//Imprimo el nuevo valor de x.
    printf ("EL nuevo valor de x es %f\n", x);

    return EXIT_SUCCESS;
}
