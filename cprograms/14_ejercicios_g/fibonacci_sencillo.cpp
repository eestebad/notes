#include <stdio.h>
#include <stdlib.h>

/*Meter la sucesión de Fibonacci en un array e imprimirla */

#define MAX 0x100 //defino un máximo

int main () {

    int i;  //Definida fuera del for porque utilizaremos su valor después.

    unsigned long f[MAX]; //Definición del array.

    f[0] = f[1] = 1;  //Inicializo a 1.

    for (i = 2; i < MAX; i++) //Inicializo i a 2 porque es la primera celda del array que quiero rellenar. Este for rellena el array.
        f[i] = f[i-1] + f[i-2];

    for (int u = 1; u < i; u++)
        printf ("%i: %lu\n", u, f[u]);


    return EXIT_SUCCESS;
}
