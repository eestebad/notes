#include <stdio.h>
#include <stdlib.h>

// Programa con una función que, dado el valor x, compute cuánto vale el polinomio. Solución del profesor.

void title () {
    system ("clear");
    system ("toilet -fpagga --gay POLINOMIOS");
    printf ("\n");
    printf ("Multiplicación de Matrices.\n");
    printf ("\n\n");
}

double f (double *polinomio, double grado, double x) {

    double altura = 0,
           potencia = 1;

    for (int p=0; p<grado; p++, potencia*=x)
        altura += polinomio[p] * potencia;

    return altura;
}

int main (int argc, char *argv[]) {
    double buffer, x, *pol = NULL;
    int dim = 0;
    char end[2]; /* Un paréntesis y un \0*/

    title ();

    printf ("Introduce el Polinomio\n");
    printf ("ej: 3x² + 2x + 1.5 => (1.5 2 3)\tPolinomio: ");
    scanf (" %*[(]");
    do {
        pol = (double *) realloc(pol, (dim+1) * sizeof(double));
        scanf(" %lf", &buffer);
        pol[dim++] = buffer;
    } while (!scanf(" %1[)]", end));

    printf ("\t\t\t\t\tx: ");
    scanf (" %lf", &x);

    printf ("f(%.2lf) = %.2lf\n", x, f(pol, dim, x));

    free (pol);

    return EXIT_SUCCESS;
}
