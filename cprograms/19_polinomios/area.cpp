#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
 #include <math.h>

// CALCULAR LA INTEGRAL

#define MAX_ERROR .001

void title () {
    system ("clear");
    system ("toilet -fpagga --gay POLINOMIOS");
    printf ("\n");
    printf ("Polinomios. Integral.\n");
    printf ("\n\n");
}


double f (double *polinomio, double grado, double x) {

    double altura = 0,
           potencia = 1;

    for (int p=0; p<grado; p++, potencia*=x)
        altura += polinomio[p] * potencia;

    return altura;
}

double *pedir (int *dim) {
    double buffer, *pol = NULL;
    char end[2]; /* Un paréntesis y un \0*/

    *dim = 0;
    printf ("Introduce el Polinomio\n");
    printf ("ej: 3x² + 2x + 1.5 => (1.5 2 3)\tPolinomio: ");
    scanf (" %*[(]");

    scanf (" %*[(]");

    do {
        pol = (double *) realloc(pol, (*dim+1) * sizeof(double));
        scanf(" %lf", &buffer);
        pol[(*dim)++] = buffer;
    } while (!scanf(" %1[)]", end));

    return pol;
}

void mostrar (double *p, int d) {
    for (int i=d-1; i>0; i--)
        printf ("%.2lfx^(%i) + ",p[i], i);
    printf ("%.2lf\n", p[0]);
}

double integral (double *p, int d, int li, int ls, double inc) {
    double suma = 0;
    for (double x=li; x<ls; x+=inc)
        suma += f(p, d, x);
    suma *= inc;

    return suma;
}

int main (int argc, char *argv[]) {
    double *pol, li, ls;   // polinomio, limite inferior y superior.
    int dim;               // Grado de los polinomios + 1.
    char opt;              // Opciones de los menús.
    int otro_pol;          // El usuario quiere trabajar con otro polinomio.
    int otra_area;         // Otra area del mismo polinomio.
    double area, ultima_area;


    do {
        title ();
        pol = pedir (&dim); // dim se pasa por referencia
                            // Sólo puede haber un valor de retorno.
                            // Lo ideal sería que pol y dim fueran un único dato, ¿verdad?


        do {
            printf ("\n");
            printf ("Límite inferior: ");
            scanf ("%lf", &li);
            printf ("Límite superior: ");
            scanf ("%lf", &ls);

            if (li > ls) {  // invertir li y ls
                double aux = li;   /* Ojo. Declarar aquí no es C. */
                li = ls;
                ls = aux;
            }

            /* Cálculos */
            double inc = fabs (ls - li);  // No es necesario porque li < ls.
                                          // Lo dejo para que veáis cómo se hace el
                                          // valor absoluto de los double.
            area = integral (pol, dim, li, ls, inc);   // inc es el ancho de los rectángulos.
            ultima_area = area - 5 * MAX_ERROR;        // Inicialmente deben diferir las dos
                                                       // variables para que el algoritmo
                                                       // intente aproximarlas.

            /*
             * La estrategia es calcular el área muchas veces,
             * dividiendo el incremento por la mitad
             * cada vez. Hasta que obtengamos dos resultados
             * casi idénticos.
             **/
            for (inc/=2; fabs (area - ultima_area) > MAX_ERROR; inc/=2) {
                ultima_area = area;
                area = integral (pol, dim, li, ls, inc);
            }

            title ();
            printf ("\n");
            printf ("El área de:\n");
            mostrar (pol, dim);
            printf ("entre [%.2lf, %.2lf] es: \n", li, ls);
            printf ("\nArea = %.3lf\n", area);
            printf ("\n");
            printf ("\n");
            /* Fin de los cálculos */

            printf ("Quiere calcular otra area (s/n): ");
            scanf (" %c", &opt);
            otra_area = tolower(opt) != 'n';
        } while (otra_area);

            printf ("Quiere trabajar on otro polinomio (s/n): ");
            scanf (" %c", &opt);
            otro_pol = tolower(opt) != 'n';
    } while (otro_pol);


    printf ("\n");
    printf ("\n");

    return EXIT_SUCCESS;
}
