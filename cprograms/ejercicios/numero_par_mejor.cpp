#include <stdio.h>
#include <stdlib.h>

/* Recoger un nº positivo por teclado e indicar si es impar o par. Utilizar una variable para el bloque de cálculo */

int main () {

    int numero, opera;

    printf ("Introduzca un nº positivo: \n");
    scanf (" %i", &numero);

    opera = numero % 2;

    if (numero < 0) {
        printf ("El nº %i no es positivo \n", numero);
    }
    else if (opera == 0) {
        printf ("El nº %i es par \n", numero);
    }
    else {
        printf ("El nº %i es impar \n", numero);
    }

    return EXIT_SUCCESS;
}
