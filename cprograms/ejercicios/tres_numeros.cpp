#include <stdio.h>
#include <stdlib.h>

/* Programa que recoja tres números e imprima cuál es el menor y cuál es el mayor */

int main () {
//Declaro variables
    int n1, n2, n3;

    int mayor, menor;
//Bloque de entrada de datos
    printf ("Introduzca el número 1: \n");
    scanf (" %i", &n1);

    printf ("Introduzca el número 2: \n");
    scanf (" %i", &n2);

    printf ("Introduzca el número 3: \n");
    scanf (" %i", &n3);
//BLoque de cálculo
    if (n1 > n2 && n1 > n3) {
        mayor = n1;
    }
    else if (n1 < n2 && n1 < n3) {
        menor = n1;
    }
    else {
        printf ("El nº %i no es el mayor ni el menor \n", n1);
    }

    if (n2 > n1 && n2 > n3) {
        mayor = n2;
    }
    else if (n2 < n1 && n2 < n3) {
        menor = n2;
    }
    else {
        printf ("El nº %i no es el mayor ni el menor \n", n2);
    }

    if (n3 > n1 && n3 > n2) {
        mayor = n3;
    }
    else if (n3 < n1 && n3 < n2) {
        menor = n3;
    }
    else {
        printf ("El nº %i no es el mayor ni el menor \n", n3);
    }
//Bloque de salida de datos
    printf ("El nº mayor es %i \n", mayor);
    printf ("El nº menor es %i \n", menor);

    return EXIT_SUCCESS;
}
