#include <stdio.h>
#include <stdlib.h>

#define OP 2;

int main () {

    double n, mitad, doble;

    printf ("Introduzca un nº real: ");
    scanf (" %lf", &n);

    mitad = (n > 100) ? n / 2 : n * 2;

    printf ("El resultado es %lf\n", mitad);

    /*    if (n > 100) {
          mitad = n / 2;
          printf ("La mitad de su nº es %f\n", mitad);
          }
          else {
          doble = n * 2;
          printf ("El doble de su nº es %f\n", doble);
          }
          */


    return EXIT_SUCCESS;
}
