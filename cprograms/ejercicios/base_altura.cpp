#include <stdio.h>
#include <stdlib.h>

/* Programa que pida base y altura y presente un menú con las opciones: calcular el área de un rectángulo y calcular el área de un triángulo. Según la opción el programa hará el cálculo del área del triángulo o del rectángulo. Si mete otra opción, dará error y no calculará nada. Realizarlo con if-else if-else y con switch. */

int main () {

    int base, altura, op, calculo;

    printf ("Introduzca la base: \n");
    scanf (" %i", &base);

    printf ("Introduzca la altura: \n");
    scanf (" %i", &altura);

    printf ("Elija una opción del menú: \n\
            1- Calcular área de un rectángulo.\n\
            2- Calcular área de un triángulo.\n");
    scanf (" %i", &op);

    //Con switch:
    switch (op) {
        case 1:
            calculo = base * altura;
            printf ("El área del rectángulo es: %i\n", calculo);
            break;
        case 2:
            calculo = (base * altura) / 2;
            printf ("El área del triángulo es: %i\n", calculo);
            break;
        default:
            printf ("La opción elegida no existe.\n");
            break;
    }
    /*Con if:
      if (op == 1) {
      calculo = base * altura;
      printf ("El área del rectángulo es: %i\n", calculo);
      }
      else if (op == 2) {
      calculo = (base * altura) / 2;
      printf ("El área del triángulo es: %i\n", calculo);
      }
      else {
      printf ("La opción elegida no existe.\n");
      }*/



    return EXIT_SUCCESS;
}
