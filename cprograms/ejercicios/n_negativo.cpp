#include <stdio.h>
#include <stdlib.h>

int main () {

    float n;

    printf ("Introduzca un nº real: ");
    scanf (" %f", &n);

    if (n < 0) {
        printf ("Su nº %f, es negativo\n",n);
    }
    else {
        printf ("Su nº %f, es positivo\n", n);
    }



    return EXIT_SUCCESS;
}
