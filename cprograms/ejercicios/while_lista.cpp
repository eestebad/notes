#include <stdio.h>
#include <stdlib.h>

//Programa que imprime el contenido de la constante de carácter lista, usando un puntero y el valor NULL.

int main(){
    char const *lista[] = {
        "Hola",
        "Adiós",
        "Buenos días",
        NULL
    };
    char const **p = lista;

    while(*p != NULL) {
        printf("%s\n", *p);
        p++;
    }

    return EXIT_SUCCESS;
}
