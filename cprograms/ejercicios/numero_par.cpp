#include <stdio.h>
#include <stdlib.h>

/*Realizar un programa que lea de la entrada estándar un nº entero positivo y escriba si es par o impar */

int main () {

    int numero;

    printf ("Escribe un nº positivo: \n");
    scanf (" %i", &numero);

    if (numero < 0) {
        printf ("El nº no es positivo \n");
    }
    else if (numero % 2 == 0) {
        printf ("El nº %i es par \n", numero);
    }
    else {
        printf ("EL nº %i es impar \n", numero);
    }

    return EXIT_SUCCESS;
}
