#include <stdio.h>
#include <stdlib.h>

/* Programa que pida la nota de un examen. Presentará la calificación correspondiente a esa nota: SUSPENSO, APROBADO, NOTABLE O SOBRESALIENTE, según los criterios indicados.*/

int main () {

    double nota;

    printf ("Introduzca la nota del examen: \n");
    scanf (" %lf", &nota);

    if (nota >= 0 && nota < 5) {
        printf ("La calificación es SUSPENSO.\n");
    }
    else if (nota >= 5 && nota < 7) {
        printf ("La calificación es APROBADO.\n");
    }
    else if (nota >= 7 && nota < 9) {
        printf ("La calificación es NOTABLE.\n");
    }
    else if (nota >= 9 && nota <= 10){
        printf ("La calificación es SOBRESALIENTE.\n");
    }
    else {
        printf ("La nota introducida no es válida.\n");
    }

    return EXIT_SUCCESS;
}
