#include <stdio.h>
#include <stdlib.h>

/*Programa que calcula la potencia de un nº sumando*/

#define P 3

int main () {

    int num = 7;
    int multi = 1;
    int potencia = 0;

    for(int i = 0; i< P; i++){
        multi *= num;
        for(int u = 1; u < num; u++)
            potencia += multi;
    }

    printf("La potencia de %i del número %i es = %i\n", P, num, potencia);

    return EXIT_SUCCESS;
}
