#include <stdio.h>
#include <stdlib.h>

/* Programa que permita introducir por teclado 10 números enteros en un array y haga las operaciones:
 * 1- Presentar por pantalla los 10 números introducidos.
 * 2- Presentar por pantalla los 10 números en orden inverso a como se introdujeron.
 * 3- Calcular y presentar la suma de los primeros 4 números introducidos en el array.
 * 4- Calcular y presentar la suma de los 7 últimos números introducidos.
 * 5- Calcular la suma y la media de todos los números del array.
 * 6- Mostrar cuál fue el mayor nº introducido.
 * 7- Mostrar cuál fue el menor nº introducido.
 * 8- Contar cuántos números pares y cuántos impares fueron introducidos (el cero no debe considerarse nº par).
 * 9- Contar cuántos ceros se introdujeron en el array.
 * 10- Contar cuántos números positivos se introdujeron en el array (no considerar el cero como nº positivo).
 * 11- Contar cuántos números negativos se introdujeron en el array.*/

#define MAX 10

int main () {

    int numeros[MAX];

    int i, mayor;
    int suma = 0;
    int suma2 = 0;
    int suma3 = 0;
    double media = 0;

    //Solicito los 10 números y los meto en el array:
    printf ("Introduzca 10 números enteros: \n");

    for (i = 0; i < MAX; i++) {
        printf ("Introduzca un nº : \n");
        scanf (" %i", &numeros[i]);
    }
    //1- Los muestro por pantalla:
    printf ("Elementos del array: \n");
    for (int u = 0; u < MAX; u++)
        printf ("En la posición %i del array: %i\n", u, numeros[u]);

    printf ("\n");

    //2- Los muestro por pantalla en orden inverso a cómo se introdujeron:
    printf ("Elementos del array en orden inverso: \n");
    for (int u = 9; u >= 0; u--)
        printf ("En la posición %i del array: %i\n", u, numeros[u]);

    printf ("\n");

    //3- Calcular y presentar la suma de los primeros 4 primeros números introducidos en el array:
    for (int u = 0; u < 4; u++)
        suma += numeros[u];

    //Imprimo el resultado de la suma:
    printf ("Suma de los 4 primeros números introducidos en el array: %i\n", suma);

    //4- Calcular y presentar la suma de los 7 últimos:
    for (int u = 3; u < MAX; u++)
        suma2 += numeros[u];

    printf ("Suma de los 7 últimos números introducidos en el array: %i\n", suma2);

    printf ("\n");

    //5- Calcular la suma y la media de todos los números:
    for (int u = 0; u < MAX; u++)
        suma3 += numeros[u];

    printf ("La suma de todos los elementos del array es: %i\n", suma3);

    printf ("\n");

    //5- Calcular la media de todos los números:
    media = (double) suma3 / MAX;

    printf ("La media de los números del array es: %.2lf\n", media);

    //6- Mostrar cuál fue el mayor nº introducido:
    mayor = numeros[0]; //considero al 1º el mayor
    for (i=0; i<10; i++) { //el for se repite 10 veces para recorrer todo el array
        if (numeros[i] > mayor){ //Si el número es mayor que lo ya tenemos guardado en mayor, entonces lo guarda en mayor y vuelve al for. Si no, no lo guarda y vuelve al for. Así hasta que i sea < 10 que ya ha recorrido todo el array.
            mayor = numeros[i];
        }
    }
    printf("El nº mayor es %i\n", mayor);


    return EXIT_SUCCESS;
}
