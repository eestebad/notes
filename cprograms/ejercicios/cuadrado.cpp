#include <stdio.h>
#include <stdlib.h>

/* Programa que pida al usuario un nº entero. Si es negativo, que calcule el cuadrado del nº y si es positivo, el cubo. Hacerlo con if y con ?: */

int main () {

    int n, cuadrado, cubo;

    printf ("Introduzca un nº entero: ");
    scanf (" %i", &n);

    /* con if:
       if (n < 0) {
       cuadrado = n * n;
       printf ("El cuadrado del nº %i es: %i\n", n, cuadrado);
       }
       else {
       cubo = n * n * n;
       printf ("El cubo del nº %i es: %i\n", n, cubo);
       }*/

    cuadrado = (n < 0) ? n * n : n * n * n;

    printf ("El resultado es: %i\n", cuadrado);

    return EXIT_SUCCESS;
}

