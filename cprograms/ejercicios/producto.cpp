#include <stdio.h>
#include <stdlib.h>

/* Programa que pida nº unidades vendidas de un producto. Si es positivo calcula la mitad. Si la mitad calculada es mayor que 100, debe aparecer en pantalla "NO COMPRAR" y si es menor o igual que 100 debe aparecer "REALIZAR PEDIDO". Si el nº es negativo, debe aparecer mensaje "VENTAS NO PUEDEN SER NEGATIVAS".*/

int main () {

    int unidades, mitad;

    printf ("Introduzca el nº de unidades vendidas: \n");
    scanf (" %i", &unidades);

    if (unidades >= 0) {
        mitad = unidades / 2;
        printf ("La mitad de las unidades vendidas %i es: %i.\n", unidades, mitad);
        if (mitad > 100) {
            printf ("La mitad es > 100, NO COMPRAR\n");
        }
            else {
                printf ("mitad es <=100, REALIZAR PEDIDO.\n");
            }
        }
        else if (unidades < 0) {
            printf ("Las ventas no pueden ser negativas.\n");
        }
        else {
            printf ("NO SE PROCESARÁN DATOS.\n");
        }

        return EXIT_SUCCESS;
    }
