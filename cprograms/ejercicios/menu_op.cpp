#include <stdio.h>
#include <stdlib.h>

/* Programa que pida un nº y luego presente menú para calcular el doble, el triple o el cuádruple. Según la opción elegida 1, 2 ó 3, el programa realizará un cálculoy presentará el resultado por pantalla. En caso de que se introduzca otra opción, aparecerá error y no se calculará nada. Hacerlo con if, else if, else y con switch. */

int main () {

    int n, op, doble, triple, cuadruple;

    printf ("Introduzca un nº: ");
    scanf (" %i", &n);

    printf ("Elija una opción: \n\
            1- Calcular el doble.\n\
            2- Calcular el triple.\n\
            3- Calcular el cuádruple.\n ");
    scanf (" %i", &op);

    //Con switch:
    switch (op) {
        case 1:
            doble = n * 2;
            printf ("El doble del nº %i es: %i\n", n, doble);
            break;

        case 2:
            triple = n * 3;
            printf ("El triple del nº %i es: %i\n", n, triple);
            break;

        case 3:
            cuadruple = n * 4;
            printf ("El cuádruple del nº %i es: %i\n", n, cuadruple);
            break;

        default:
            printf ("La opción indicada no existe.\n");
            break;
    }

    /* Con if:
       if (op == 1){
       doble = n * 2;
       printf ("El doble del nº %i es: %i\n", n, doble);
       }
       else if (op == 2) {
       triple = n * 3;
       printf ("EL triple del nº %i es: %i\n", n, triple);
       }
       else if (op == 3) {
       cuadruple = n * 4;
       printf ("El cuádruple del nº %i es: %i\n", n, cuadruple);
       }
       else {
       printf ("La opción elegida no existe.\n");
       }*/

    return EXIT_SUCCESS;
}
