#include <stdio.h>
#include <stdlib.h>

/*Programa que pida dos números y presente menú para sumar, multiplicar, restar o dividir, según la opción elegida. Realizar con if-else if-else y switch.*/

int main () {

    int n1, n2, suma, multi, resta, division;
    char op;

    printf ("Introduzca el primer número a operar: \n");
    scanf (" %i", &n1);

    printf ("Introduzca el segundo número a operar: \n");
    scanf (" %i", &n2);

    printf ("¿Qué operación desea realizar?: \n\
        A) Sumar los números.\n\
        B) Multiplicar los números.\n\
        C) Restar el primer nº menos el segundo.\n\
        D) Dividir el primer nº por el segundo.\n");
    scanf (" %c", &op);

    //COn switch:
    switch (op) {
        case 'A':
            suma = n1 + n2;
            printf ("La suma de los números %i + %i es: %i\n", n1, n2, suma);
            break;
        case 'B':
            multi = n1 * n2;
            printf ("La multiplicación de los números %i * %i es: %i\n", n1, n2, multi);
            break;
        case 'C':
            resta = n1 - n2;
            printf ("La resta de los números %i - %i es: %i\n", n1, n2, resta);
            break;
        case 'D':
            division = n1 / n2;
            printf ("La división de los números %i / %i es: %i\n", n1, n2, division);
            break;
        default:
            printf ("La opción elegida no existe.\n");
            break;
    }

    /* Con if:
       if (op == 'A') {
       suma = n1 + n2;
       printf ("La suma de los números %i + %i es: %i\n", n1, n2, suma);
       }
       else if (op == 'B') {
       multi = n1 * n2;
       printf ("La multiplicación de los números %i * %i es: %i\n", n1, n2, multi);
       }
       else if (op == 'C') {
       resta = n1 - n2;
       printf ("La resta de los números %i - %i es: %i\n", n1, n2, resta);
       }
       else if (op == 'D') {
       division = n1 / n2;
       printf ("La división de los números %i / %i es; %i\n", n1, n2, division);
       }
       else {
       printf ("La opción elegida no existe.\n");
       }*/


    return EXIT_SUCCESS;
}
