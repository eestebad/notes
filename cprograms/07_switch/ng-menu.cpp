#include <stdio.h>
#include <stdlib.h>

enum TOpcion { suma, resta };

const char *opciones[] = {
    "Suma",
    "Resta",
    NULL
};

int menu () {
    int opcion;
    system ("clear");
    system ("toilet -fpagga --gay TheShow");

    printf (
"\n\
    MENU\n\
    ====\n\
\n\
        1.- Suma. \n\
        2.- Resta.\n\
\n\
    Tu opcion: \
"
            );
    scanf (" %i", &opcion);

    return opcion - 1;
}

int main () {
    int op1 = 7, op2 = 5;
    int opcion = menu ();

    printf ("La opción elegida es: %i\n", opcion);

    switch (opcion) {
        case suma:
            printf ( "%i + %i = %2i\n", op1, op2, op1 + op2 );
            break;
        case resta:
            printf ( "%i - %i = %2i\n", op1, op2, op1 - op2 );
            break;

        default:
            fprintf (stderr, "Opción incorrecta. \n");
            return EXIT_FAILURE;
    }

    printf ("%p - %s - %c\n", opciones[0], opciones[0], *opciones[0]);

    return EXIT_SUCCESS;
}

