#include <stdio.h>
#include <stdlib.h>

/* Funcionamiento del valor de retorno de scanf. Pido fecha al usuario y guardo sólo día y año. El valor de retorno de scanf será 2 */

int main (int argc, char *argv[]) {

    int d, y; //Declaro d e y para guardar día y año.
    int rv; //Declaro rv para guardar el valor de retorno del scanf.

    printf ("dd/mm/yyyy:  "); //Pido la fecha en un formato en concreto.
    rv = scanf (" %i/%*i%*1[/-]%i", &d, &y); /*Digo que guarde el valor de retorno de scanf en rv. Con %*i digo que lea pero no guarde el mes. Con *1[/-] digo que lea pero no guarde, siempre que se cumpla el criterio de ser / o -. Si lo que viene del tubo no cumple ese criterio, deja de sacar caracteres del tubo */

    printf (" día: %i\n año %i\n", d, y); //Imprime d e y
    printf ("rv : %i\n", rv); //Imprime el valor de retorno de scanf.

    return EXIT_SUCCESS;
}

