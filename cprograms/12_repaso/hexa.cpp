#include <stdio.h>
#include <stdlib.h>
/* Programa para ver funcionamiento del atoi y de especificador de scanf*/
#define MAX 0x100 //defino el tamaño que usará el array

int main (int argc, char *argv[]) {

    char leido[MAX]; //declaro array con máximo 256
    int numero;

    printf ("Hexa: "); //Pido un nº
    scanf (" %[0-9]", leido); /*si es un nº lo guardo en leído. Si no cumple ese criterio, el carácter se quedará en el tubo y para sacarl    o __fpurge*/
     numero = atoi (leido); /*Como con %[0-9] los nº se guardan como char, en código ASCII hexa, los transformo a enteros con atoi*/

    int resultado = 0;
    for (int i=0; leido[i]!='\0'; i++) // Recorro el array
        resultado = resultado * 10 + leido[i] - '0'; /*En cada vuelta multiplico resultado que está inicializado a 0, por 10, le sumo lo q    ue hay en leido[i] que (para el nº 754) la 1ª vez es 37, la 2ª 35 y la 3ª 34 y le resto 0, que es 30 en hexa. Así transformo cada carácter     hexa a entero: 37=7, 35=5 y 34=4*/

    printf ("Numero:\t%i\nResultado:\t%i\n", numero, resultado); /*Imprimo el nº ya entero transformado con atoi (numero) y transformado c    on el for (resultado*/
    printf ("\n");

    return EXIT_SUCCESS;
}

