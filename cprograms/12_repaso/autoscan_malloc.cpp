#include <stdio.h>
#include <stdlib.h>

//Ejemplo del funcionamiento de malloc

int main (int argc, char *argv[]) {

    char *fruta; //Defino array que no sé cuánto va a ocupar

    printf( "Fruta: " );  //SOlicito al usuario que introduzca un nombre de fruta
    scanf (" %ms", &fruta); //lo guardo en array fruta con el especificador %ms que hace un malloc, ya que no sé cuánto ocupará fruta.

    printf (" %s\n", fruta);

    free (fruta); //Libero la memoria que el malloc había reservado.

    return EXIT_SUCCESS;
}

