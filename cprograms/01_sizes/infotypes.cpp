
#include <stdio.h>
/* infotypes list*/

int main (){

    char var1;
    long int var2;
    signed char var3;
    long long int var4;
    double var5;
    unsigned char var6;
    signed int var7;
    long double var8;
    int var9;

    printf ("char: %lu bytes\n", sizeof (var1));
    printf ("long int: %lu bytes\n", sizeof (var2));
    printf ("signed char: %lu bytes\n", sizeof (var3));
    printf ("long long int: %lu bytes\n", sizeof (var4));
    printf ("double: %lu bytes\n", sizeof (var5));
    printf ("unsigned char: %lu bytes\n", sizeof (var6));
    printf ("signed int: %lu bytes\n", sizeof (var7));
    printf ("long double: %lu bytes\n", sizeof (var8));
    printf ("int: %lu bytes\n", sizeof (var9));

    return 0;

}
