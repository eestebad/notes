/*
 * =====================================================================================
 *
 *       Filename:  argv_use.cpp
 *
 *    Description: argv use. Exercise of Advanced Linux Programming by Mark Mitchell, Jeffrey Oldham, Alex Samuel.
 *
 *        Version:  1.0
 *        Created:  15/07/20 18:17:24
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    printf ("The name of this program es %s.\n", argv[0]);
    printf ("This program was invoked with %d arguments.\n", argc - 1);

    if (argc > 1)
    {
        int i;
        printf ("The arguments are:\n");
        for (i = 1; i < argc; ++i)
            printf (" %s\n", argv[i]);
    }

    return EXIT_SUCCESS;
}


