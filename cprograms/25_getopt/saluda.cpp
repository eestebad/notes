#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define VERDE "\x1B[32m"
#define RESET "\x1B[0m"

enum TLang {es, en} lang = es;

const char *command_name;
const char *greet_word[] = {"Hola", "Hi"};

void print_usage (FILE *output) {
    int rv = 0;

    if (output == stderr)
        rv = 1;

    fprintf ( output, " Usage:    %s <name> \n Greets <name>\n \n  Parameters:\n -h Shows help.\n -l <lang> Greets in another language.\n", command_name);

    exit (rv);
}

int main (int argc, char *argv[]) {

    char c;
    int verbose = 0;

    command_name = argv[0];

    if (argc < 2)
        print_usage (stderr);

    while ( (c = getopt (argc, argv, "hvl:")) != -1 ) {
        switch (c) {
            case 'h': print_usage (stdout);
                      break;
            case 'v': verbose = 1;
                      break;
            case 'l':
                      if (strcmp (optarg, "en") == 0 )
                          lang = en;
                      break;
            case '?':
                      if (optopt == 'l')
                          fprintf (stderr, "la opción l necesita un parámetro.\n");
                      print_usage (stderr);

                      break;
        }
    }

    for (int i=optind; i<argc; i++) {
        if (verbose)
            printf (VERDE "Estoy a punto de imprimir el nombre %i.\n" RESET, i - optind + 1);
        printf ("%s, %s\n", greet_word[lang], argv[i]);
        if (verbose)
            printf (VERDE "Acabo de imprimir el nombre %i.\n" RESET, i - optind + 1);
    }
    return EXIT_SUCCESS;
}

