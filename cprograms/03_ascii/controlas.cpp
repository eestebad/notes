#include <stdio.h>
#include <unistd.h>

#define MAXCOL 100

int main() {

/*las tres líneas hacen lo mismo*/
    printf ("hola \x0A \x09 Beep \x0A");/*en hexadecimal*/
    printf ("hola \n \t Beep \a \n");/*nombres ascii*/
    printf ("hola \012 \011 Beep \07 \012");/*en octal*/

    printf ("hola \0x0A esto es secreto");/*sólo sale hola, porque \0 indica fin de la cadena*/
/* el primer for modifica al segundo y el segundo al print f*/

    print ("\n");
    for (int vez = 0; vez<MAXCOL; vez++) {
        printf ("\r"); 
        for (int igual=0; igual<vez; igual++)
            printf("=");
        printf ("> %2i%%", vez);
        fpurge (stdout)   printf ("\n");
    for (int vez=0; vez<=MAXCOLS; vez++) {
        printf ("\r");
        for (int igual=0; igual<vez; igual++) /*imprime igual*/
            printf("=");
        printf ("> %2i%%", vez);
        fflush (stdout); /* vacía el tubo para que imprima la terminal*/
        usleep (100000);
    }
    printf ("\n.FIN.\n");

    return 0;
}

    }
    printf ("\n.FIN.");
    return 0;
}
