#include <stdio.h>
#define SALTO ('a' - 'A')

int main () {
char letra = 'c';

/* si la letra es mayúsculas*/   
if (letra >= 'a' && letra <= 'z')
letra -= SALTO; 
else
/* Si no*/
letra += SALTO;
    /* Change the letter "c" lower case to capital letter */
    printf ("letra en mayúsculas: %c\n", letra);

    return 0;
}
