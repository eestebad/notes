#include <stdio.h>

int main () {
    /* declare letra */
    char letra;
    /* Change the letter "c" lower case to capital letter */
    for (letra = 0x63; letra >= 0x43; letra--)
	printf ("letra en mayúsculas: %c\n", letra);

    return 0;
}
