/*Crear base APLICACIÓN*/
CREATE DATABASE APLICACION;
USE APLICACION;
CREATE TABLE TABLA (nombre VARCHAR (20), base_datos VARCHAR (20), PRIMARY KEY (nombre, base_datos));
CREATE TABLE CAMPO (nombre VARCHAR (20), nombre_TABLA VARCHAR (20), base_datos_TABLA VARCHAR (20), tipo VARCHAR (20), PRIMARY KEY (nombre), FOREIGN KEY (nombre_TABLA, base_datos_TABLA) REFERENCES TABLA (nombre, base_datos) ON UPDATE CASCADE);
CREATE TABLE compone (nombre_TABLA VARCHAR (20), base_datos_TABLA VARCHAR (20), nombre_CAMPO VARCHAR (20), FOREIGN KEY (nombre_TABLA, base_datos_TABLA) REFERENCES TABLA (nombre, base_datos) ON UPDATE CASCADE, FOREIGN KEY (nombre_CAMPO) REFERENCES CAMPO (nombre) ON UPDATE CASCADE);

/*Crear base APARATOS ELECTRÓNICOS*/
CREATE DATABASE APARATOS_ELECTRONICOS;
USE APARATOS_ELECTRONICOS;
/*Creamos la tabla TIPO*/
CREATE TABLE TIPO (nombre VARCHAR (30), caracteristica VARCHAR (30), contiene_general VARCHAR (30), PRIMARY KEY (nombre, caracteristica, contiene_general));
/*Creamos la tabla APARATO*/
CREATE TABLE APARATO (codigo INT (20), descripcion VARCHAR (30), nombre_TIPO VARCHAR (30), caracteristica_TIPO VARCHAR (30), contiene_general_TIPO VARCHAR (30), PRIMARY KEY (codigo), FOREIGN KEY (nombre_TIPO, caracteristica_TIPO, contiene_general_TIPO) REFERENCES TIPO (nombre, caracteristica, contiene_general) ON UPDATE CASCADE);
/*Creamos la tabla FABRICANTE*/
CREATE TABLE FABRICANTE (cif CHAR (9), domicilio_social VARCHAR (40), PRIMARY KEY (cif));
/*Creamos la tabla COMPONENTE*/
CREATE TABLE COMPONENTE (nombre VARCHAR (40), especificaciones VARCHAR (50), codigo_APARATO INT (20), cif_FABRICANTE CHAR (9), precio INT (5), PRIMARY KEY (nombre), FOREIGN KEY (codigo_APARATO) REFERENCES APARATO (codigo) ON UPDATE CASCADE, FOREIGN KEY (cif_FABRICANTE) REFERENCES FABRICANTE (cif) ON UPDATE CASCADE);
/*Creamos la tabla compone*/
CREATE TABLE compone (codigo_APARATO INT (20), nombre_COMPONENTE VARCHAR (40), n_unidades INT (5), cif_FABRICANTE CHAR (9), FOREIGN KEY (codigo_APARATO) REFERENCES APARATO (codigo) ON UPDATE CASCADE, FOREIGN KEY (nombre_COMPONENTE) REFERENCES COMPONENTE (nombre) ON UPDATE CASCADE, FOREIGN KEY (cif_FABRICANTE) REFERENCES FABRICANTE (cif) ON UPDATE CASCADE);
/*Fin tablas base APARATOS*/

/*Creamos la base MUEBLE*/
CREATE DATABASE MUEBLE;
USE MUEBLE;
/*Creamos la tabla MUEBLE, con el campo precio en DECIMAL para poder introducir números con decimales*/
CREATE TABLE MUEBLE (nombre VARCHAR (20), precio DECIMAL (4,2), PRIMARY KEY (nombre));
/*Creamos la tabla PIEZA, con el campo identificador como char, porque puede ser alfanumérico, ya que no se indica nada y de longitud fija*/
CREATE TABLE PIEZA (identificador CHAR (15), PRIMARY KEY (identificador));
/*Creamos la tabla ESTANTE, con los campos altura y pasillo como enteros, ya que se entiende que vendrán identificados por un nº entero*/
CREATE TABLE ESTANTE (altura INT (10), pasillo INT (10), PRIMARY KEY (altura, pasillo));
/*Creamos las tablas de las relaciones tiene y está*/
CREATE TABLE tiene (nombre_MUEBLE VARCHAR (20), identificador_PIEZA CHAR (15), FOREIGN KEY (nombre_MUEBLE) REFERENCES MUEBLE (nombre) ON UPDATE CASCADE, FOREIGN KEY (identificador_PIEZA) REFERENCES PIEZA (identificador) ON UPDATE CASCADE);
CREATE TABLE esta (identificador_PIEZA CHAR (15), altura_ESTANTE INT (10), pasillo_ESTANTE INT (10), n_unidades INT (10), FOREIGN KEY (identificador_PIEZA) REFERENCES PIEZA (identificador) ON UPDATE CASCADE, FOREIGN KEY (altura_ESTANTE, pasillo_ESTANTE) REFERENCES ESTANTE (altura, pasillo) ON UPDATE CASCADE);
/*FIN DE LA BASE MUEBLES*/

/*Crear base EJERCICIO REFUERZO 4: JUEGO*/
CREATE DATABASE JUEGO;
USE JUEGO;
/*Crear tabla ESCENARIO, con los campos tiempo y riesgo que como no sé lo que contienen, los creo como CHAR (alfanuméricos)*/
CREATE TABLE ESCENARIO (numero INT (3), tiempo CHAR (4), riesgo CHAR (15), PRIMARY KEY (numero));
/*Crear tabla PERSONAJE*/
CREATE TABLE PERSONAJE (nombre VARCHAR (20), fuerza INT (3), inteligencia INT (3), habilidad INT (3), nombre_dominante_PERSONAJE VARCHAR (20), numero_ESCENARIO INT (3), PRIMARY KEY (nombre, nombre_dominante_PERSONAJE), FOREIGN KEY (numero_ESCENARIO) REFERENCES ESCENARIO (numero) ON UPDATE CASCADE);
/*Crear tabla OBJETO. Se crea código como CHAR porque no se dice si es numérico o no. Se crea un campo para hora, minuto, segundo de tipo TIME*/
CREATE TABLE OBJETO (codigo CHAR (5), hora_minuto_segundo TIME, nombre_PERSONAJE VARCHAR (20), numero_ESCENARIO INT (3), PRIMARY KEY (codigo), FOREIGN KEY (nombre_PERSONAJE) REFERENCES PERSONAJE (nombre) ON UPDATE CASCADE, FOREIGN KEY (numero_ESCENARIO) REFERENCES ESCENARIO (numero) ON UPDATE CASCADE);
/*FIN BASE JUEGO*/

/*Crear base EJERCICIO REPASO 1: AGENDA WALTER*/
CREATE DATABASE AGENDA_WALTER;
USE AGENDA_WALTER;
/*Crear tabla PERSONA*/
CREATE TABLE PERSONA (nombre VARCHAR (30), apellidos VARCHAR (30), trabajo VARCHAR (40), PRIMARY KEY (nombre, apellidos));
/*Crear tabla SITUACION, que tiene nombre y apellidos PERSONA, pq incluye la relación cita, que tenía cardinalidad 1,1*/
CREATE TABLE SITUACION (hora TIME, lugar VARCHAR (40), vestuario VARCHAR (20), mercancia VARCHAR (20), nombre_PERSONA VARCHAR (30), apellidos_PERSONA VARCHAR (30), PRIMARY KEY (hora), FOREIGN KEY (nombre_PERSONA, apellidos_PERSONA) REFERENCES PERSONA (nombre, apellidos) ON UPDATE CASCADE);
/*Crear tabla OBJETO*/
CREATE TABLE OBJETO (nombre VARCHAR (20), tamano VARCHAR (20), nombre_OBJETO_contenedor VARCHAR (20), PRIMARY KEY (nombre));
/*Crear tabla lleva*/
CREATE TABLE lleva (hora_SITUACION TIME, nombre_OBJETO VARCHAR (20), FOREIGN KEY (hora_SITUACION) REFERENCES SITUACION (hora) ON UPDATE CASCADE, FOREIGN KEY (nombre_OBJETO) REFERENCES OBJETO (nombre) ON UPDATE CASCADE);
/*FIN BASE AGENDA_WALTER*/

/*Crear base EJERCICIO DE REPASO 3: PEQUEÑO NICOLÁS*/
CREATE DATABASE PEQUENO_NICOLAS;
USE PEQUENO_NICOLAS;

/*Crear tabla CONTACTO, cuya clave es un ID único artificial*/
CREATE TABLE CONTACTO (ID MEDIUMINT NOT NULL AUTO_INCREMENT, nombre VARCHAR (40), apellidos VARCHAR (40), PRIMARY KEY (ID));

/*Crear tabla POLÍTICO, que cuelga del ISA CONTACTO, cuya PK es el ID de la entidad CONTACTO*/
CREATE TABLE POLITICO (ID_CONTACTO MEDIUMINT NOT NULL AUTO_INCREMENT, relevancia TINYINT (3), partido VARCHAR (40), FOREIGN KEY (ID_CONTACTO) REFERENCES CONTACTO (ID) ON UPDATE CASCADE);

/*Crear tabla MIEMBRO_REALEZA, que cuelga del ISA CONTACTO, cuya PK es el ID de la entidad CONTACTO*/
CREATE TABLE MIEMBRO_REALEZA (ID_CONTACTO MEDIUMINT NOT NULL AUTO_INCREMENT, ocupacion VARCHAR (20), FOREIGN KEY (ID_CONTACTO) REFERENCES CONTACTO (ID) ON UPDATE CASCADE);

/*Crear tabla FAMOSO, que cuelga del ISA CONTACTO, cuya PK es el ID de la entidad CONTACTO*/
CREATE TABLE FAMOSO (ID_CONTACTO MEDIUMINT NOT NULL AUTO_INCREMENT, n_famosos TINYINT (4), apodo VARCHAR (30), FOREIGN KEY (ID_CONTACTO) REFERENCES CONTACTO (ID) ON UPDATE CASCADE);

/*Crear tabla EVENTO, que se relaciona con la entidad FAMOSO*/
CREATE TABLE EVENTO (hora TIME, fecha DATE, lugar VARCHAR (30), cargo VARCHAR (20), ID_CONTACTO_FAMOSO MEDIUMINT NOT NULL AUTO_INCREMENT, PRIMARY KEY (hora, fecha, lugar), FOREIGN KEY (ID_CONTACTO_FAMOSO) REFERENCES FAMOSO (ID_CONTACTO) ON UPDATE CASCADE);

/*Crear tabla presenta, que es una relación reflexiva de la entidad CONTACTO. No se puede traer dos veces el ID de la tabla CONTACTO, así que se trae una vez y el otro ID se rellenará manualmente*/
CREATE TABLE presenta (ID_CONTACTO_presentador MEDIUMINT (4), ID_CONTACTO_presentado MEDIUMINT (4), PRIMARY KEY (ID_CONTACTO_presentador), FOREIGN KEY (ID_CONTACTO_presentado) REFERENCES CONTACTO (ID) ON UPDATE CASCADE);
/*FIN BASE PEQUENO_NICOLAS*/


/*Crear base EJERCICIO DE REPASO 4: DEBATE*/
CREATE DATABASE DEBATE;
USE DEBATE;

/*Crear tabla PARTICIPANTE*/
CREATE TABLE PARTICIPANTE (nombre VARCHAR (40), apellidos VARCHAR (50), partido VARCHAR (40), minutos SMALLINT (3), segundos SMALLINT (5), PRIMARY KEY (partido));

/*Crear tabla TEMA*/
CREATE TABLE TEMA (nombre VARCHAR (40), PRIMARY KEY (nombre));

/*Crear tabla TITULAR, que es atributo multivalorado de TEMA*/
CREATE TABLE TITULAR (nombre_TEMA VARCHAR (40), titular VARCHAR (100), PRIMARY KEY (titular), FOREIGN KEY (nombre_TEMA) REFERENCES TEMA (nombre) ON UPDATE CASCADE);

/*Crear tabla IDEA*/
CREATE TABLE IDEA (ID MEDIUMINT (4), contenido VARCHAR (100), ID_IDEA_encadenada MEDIUMINT (4), PRIMARY KEY (ID, ID_IDEA_encadenada));

/*Crear tabla TURNO*/
CREATE TABLE TURNO (numero TINYINT (3), partido_PARTICIPANTE VARCHAR (40), ID_IDEA MEDIUMINT (4), PRIMARY KEY (numero), FOREIGN KEY (partido_PARTICIPANTE) REFERENCES PARTICIPANTE (partido) ON UPDATE CASCADE, FOREIGN KEY (ID_IDEA) REFERENCES IDEA (ID) ON UPDATE CASCADE);

/*Crear tabla REFERENCIA, que es la entidad principal de un ISA*/
CREATE TABLE REFERENCIA (ID MEDIUMINT (4), nombre VARCHAR (100), PRIMARY KEY (ID));

/*Crear tabla PERSONA, que es entidad hija de un ISA*/
CREATE TABLE PERSONA (ID_REFERENCIA MEDIUMINT (4), apellidos VARCHAR (40), FOREIGN KEY (ID_REFERENCIA) REFERENCES REFERENCIA (ID) ON UPDATE CASCADE);

/*Crear tabla PROGRAMA, que es entidad hija de un ISA*/
CREATE TABLE PROGRAMA (ID_REFERENCIA MEDIUMINT (4), cadena VARCHAR (40), FOREIGN KEY (ID_REFERENCIA) REFERENCES REFERENCIA (ID) ON UPDATE CASCADE);

/*Crear tabla NOTICIA, que es entidad hija de un ISA*/
CREATE TABLE NOTICIA (ID_REFERENCIA MEDIUMINT (4), fecha DATE, FOREIGN KEY (ID_REFERENCIA) REFERENCES REFERENCIA (ID) ON UPDATE CASCADE);

/*Crear tabla opina, que es relación entre PARTICIPANTE y TEMA*/
CREATE TABLE opina (partido_PARTICIPANTE VARCHAR (40), nombre_TEMA VARCHAR (40), FOREIGN KEY (partido_PARTICIPANTE) REFERENCES PARTICIPANTE (partido) ON UPDATE CASCADE, FOREIGN KEY (nombre_TEMA) REFERENCES TEMA (nombre) ON UPDATE CASCADE);

/*Crear tabla habla, que es relación entre TURNO y REFERENCIA*/
CREATE TABLE habla (numero_TURNO TINYINT (3), ID_REFERENCIA MEDIUMINT (4), FOREIGN KEY (numero_TURNO) REFERENCES TURNO (numero) ON UPDATE CASCADE, FOREIGN KEY (ID_REFERENCIA) REFERENCES REFERENCIA (ID) ON UPDATE CASCADE);

/*Crear tabla relaciona, que es relación entre REFERENCIA y NOTICIA*/
CREATE TABLE relaciona (ID_REFERENCIA_NOTICIA MEDIUMINT (4), ID_REFERENCIA MEDIUMINT (4), FOREIGN KEY (ID_REFERENCIA_NOTICIA) REFERENCES NOTICIA (ID_REFERENCIA) ON UPDATE CASCADE, FOREIGN KEY (ID_REFERENCIA) REFERENCES REFERENCIA (ID) ON UPDATE CASCADE);

/*FIN BASE DEBATE*/

/*Crear base EJERCICIO DE REPASO 5: PROGRAMACIÓN TV*/
CREATE DATABASE PROGRAMACION_TV;
USE PROGRAMACION_TV;

/*Crear tabla CADENA. Logo se crea como VARCHAR para almacenar la imagen como URL*/
CREATE TABLE CADENA (nombre VARCHAR (50), logo VARCHAR (100), PRIMARY KEY (nombre));

/*Crear tabla FRANJA. Como es entidad débil, trae la PK de CADENA. Es entidad principal de un ISA*/
CREATE TABLE FRANJA (nombre VARCHAR (50), nombre_CADENA VARCHAR (50), descripcion VARCHAR (100), hora TIME, dia DATE, PRIMARY KEY (nombre), FOREIGN KEY (nombre_CADENA) REFERENCES CADENA (nombre) ON UPDATE CASCADE);

/*Crear tabla PROGRAMA, que es entidad hija del ISA FRANJA. Trae la PK de FRANJA y la de CADENA, porque FRANJA es débil*/
CREATE TABLE PROGRAMA (nombre_FRANJA VARCHAR (50), nombre_CADENA_FRANJA VARCHAR (50), tipo VARCHAR (50), productora VARCHAR (50), FOREIGN KEY (nombre_FRANJA) REFERENCES FRANJA (nombre) ON UPDATE CASCADE, FOREIGN KEY (nombre_CADENA_FRANJA) REFERENCES FRANJA (nombre_CADENA) ON UPDATE CASCADE);

/*Crear tabla SERIE, que es entidad hija del ISA FRANJA. Trae la PK de FRANJA y la de CADENA, porque FRANJA es débil*/
CREATE TABLE SERIE (nombre_FRANJA VARCHAR (50), nombre_CADENA_FRANJA VARCHAR (50), n_episodio SMALLINT (5), temporada SMALLINT (4), FOREIGN KEY (nombre_FRANJA) REFERENCES FRANJA (nombre) ON UPDATE CASCADE, FOREIGN KEY (nombre_CADENA_FRANJA) REFERENCES FRANJA (nombre_CADENA) ON UPDATE CASCADE);

/*Crear tabla PERSONA, que se relaciona con FRANJA mediante sale*/
CREATE TABLE PERSONA (nombre VARCHAR (50), tipo VARCHAR (30), PRIMARY KEY (nombre));

/*Crear tabla sale, que es relación entre FRANJA y PERSONA y se trae la PK de CADENA, porque FRANJA es débil */
CREATE TABLE sale (nombre_FRANJA VARCHAR (50), nombre_CADENA_FRANJA VARCHAR (50), nombre_PERSONA VARCHAR (50), FOREIGN KEY (nombre_FRANJA) REFERENCES FRANJA (nombre) ON UPDATE CASCADE, FOREIGN KEY (nombre_CADENA_FRANJA) REFERENCES FRANJA (nombre_CADENA) ON UPDATE CASCADE, FOREIGN KEY (nombre_PERSONA) REFERENCES PERSONA (nombre) ON UPDATE CASCADE);

/* FIN BASE PROGRAMACIÓN TV*/


/*Crear base EJERCICIO DE REPASO 6: BIBLIOTECA*/
CREATE DATABASE BIBLIOTECA;
USE BIBLIOTECA;

/*Crear la tabla ARTÍCULO, que es entidad principal de ISA*/
CREATE TABLE ARTICULO (codigo INT, nombre VARCHAR (50), resumen MEDIUMTEXT, anyo YEAR, deterioro CHAR (2), comentario VARCHAR (100), PRIMARY KEY (codigo));

/*Crear la tabla LIBRO, que es entidad hija del ISA ARTÍCULO*/
CREATE TABLE LIBRO (codigo_ARTICULO_LIBRO INT, n_paginas INT, FOREIGN KEY (codigo_ARTICULO_LIBRO) REFERENCES ARTICULO (codigo) ON UPDATE CASCADE);

/*Crear la tabla CD, que es entidad hija del ISA ARTÍCULO*/
CREATE TABLE CD (codigo_ARTICULO_CD INT, n_canciones TINYINT, FOREIGN KEY (codigo_ARTICULO_CD) REFERENCES ARTICULO (codigo) ON UPDATE CASCADE);

/*Crear la tabla PELÍCULA, que es entidad hija del ISA ARTÍCULO*/
CREATE TABLE PELICULA (codigo_ARTICULO_PELICULA INT, duracion SMALLINT, FOREIGN KEY (codigo_ARTICULO_PELICULA) REFERENCES ARTICULO (codigo) ON UPDATE CASCADE);

/*Crear la tabla AUTOR, que se relaciona con ARTÍCULO mediante tiene*/
CREATE TABLE AUTOR (ID INT, nombre VARCHAR (50), pais VARCHAR (40), PRIMARY KEY (ID));

/*Crear la tabla SOCIO, que se relaciona con ARTÍCULO mediante presta. Se asigna VARCHAR a teléfono, porque pueden ser varios*/
CREATE TABLE SOCIO (DNI CHAR (9), nombre VARCHAR (50), apellidos VARCHAR (50), codigo INT, direccion VARCHAR (60), PRIMARY KEY (DNI));

/*Crear la tabla TELEFONO, que es atributo multivalorado de SOCIO*/
CREATE TABLE TELEFONO (DNI_SOCIO CHAR (9), telefono VARCHAR (100), PRIMARY KEY (telefono), FOREIGN KEY (DNI_SOCIO) REFERENCES SOCIO (DNI) ON UPDATE CASCADE);

/*Crear la tabla presta, que relaciona las entidades ARTÍCULO y SOCIO*/
CREATE TABLE presta (codigo_ARTICULO INT, DNI_SOCIO CHAR (9), fecha_prestamo DATE, fecha_maxima DATE, fecha_devolucion DATE, FOREIGN KEY (codigo_ARTICULO) REFERENCES ARTICULO (codigo) ON UPDATE CASCADE, FOREIGN KEY (DNI_SOCIO) REFERENCES SOCIO (DNI) ON UPDATE CASCADE);

/*Crear la tabla tiene, que relaciona las entidades AUTOR y ARTÍCULO*/
CREATE TABLE tiene (codigo_ARTICULO INT, ID_AUTOR INT, FOREIGN KEY (codigo_ARTICULO) REFERENCES ARTICULO (codigo) ON UPDATE CASCADE, FOREIGN KEY (ID_AUTOR) REFERENCES AUTOR (ID) ON UPDATE CASCADE);

/*FIN BASE BIBLIOTECA*/


/*EJERCICIO EXAMEN1: HAMBURGUESERÍA*/
CREATE DATABASE HAMBURGUESERIA;
USE HAMBURGUESERIA;

/*Crear tabla TIENDA*/
CREATE TABLE TIENDA (ID SMALLINT, PRIMARY KEY (ID));

/*Crear tabla CUPÓN, que es entidad débil y además absorbe la relación ofrece, por lo que se trae la clave de TIENDA y los atributos de ofrece*/
CREATE TABLE CUPON (codigo SMALLINT, ID_TIENDA SMALLINT, precio INT (3), fecha_inicio DATE, fecha_final DATE, PRIMARY KEY (codigo), FOREIGN KEY (ID_TIENDA) REFERENCES TIENDA (ID) ON UPDATE CASCADE);

/*Crear tabla PRODUCTO, que es la entidad principal de un ISA*/
CREATE TABLE PRODUCTO (nombre VARCHAR (30), PRIMARY KEY (nombre));

/*Crear tabla HAMBURGUESA, que es hija en el ISA y tiene un atributo multivalorado. Se trae la PK de TIENDA porque tiene cardinalidad 1,1*/
CREATE TABLE HAMBURGUESA (nombre_PRODUCTO VARCHAR (30), tipo VARCHAR (20), n_carne TINYINT (2), ID_TIENDA SMALLINT, FOREIGN KEY (nombre_PRODUCTO) REFERENCES PRODUCTO (nombre) ON UPDATE CASCADE, FOREIGN KEY (ID_TIENDA) REFERENCES TIENDA (ID) ON UPDATE CASCADE);

/*Crear tabla EXTRA, que es atributo multivalorado de HAMBURGUESA*/
CREATE TABLE EXTRA (nombre_PRODUCTO_HAMBURGUESA VARCHAR (30), extra VARCHAR (20), FOREIGN KEY (nombre_PRODUCTO_HAMBURGUESA) REFERENCES PRODUCTO (nombre) ON UPDATE CASCADE, PRIMARY KEY (extra));

/*Crear tabla SANDI, que es hija en el ISA */
CREATE TABLE SANDI (nombre_PRODUCTO VARCHAR (30), sabor VARCHAR (20), FOREIGN KEY (nombre_PRODUCTO) REFERENCES PRODUCTO (nombre) ON UPDATE CASCADE);

/*Crear tabla COMPLEMENTO, que es hija en el ISA */
CREATE TABLE COMPLEMENTO (nombre_PRODUCTO VARCHAR (30), tipo VARCHAR (20), FOREIGN KEY (nombre_PRODUCTO) REFERENCES PRODUCTO (nombre) ON UPDATE CASCADE);

/*Crear tabla contiene, que es relación entre CUPÓN Y PRODUCTO y se trae la PK de TIENDA, porque CUPÓN es débil*/
CREATE TABLE contiene (ID_TIENDA_CUPON SMALLINT, codigo_CUPON SMALLINT, nombre_PRODUCTO VARCHAR (30), cantidad SMALLINT, FOREIGN KEY (ID_TIENDA_CUPON, codigo_CUPON) REFERENCES CUPON (ID_TIENDA, codigo) ON UPDATE CASCADE, FOREIGN KEY (nombre_PRODUCTO) REFERENCES PRODUCTO (nombre) ON UPDATE CASCADE);

/*FIN BASE HAMBURGUESERÍA*/

/* FIN EJERCICIOS BASES */

