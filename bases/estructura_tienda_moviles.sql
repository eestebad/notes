-- EJERCICIOS DE REFUERZO 1: se desea diseñar una base de datos para una conocida tienda de móviles. --

CREATE DATABASE TIENDA_MOVILES;

USE TIENDA_MOVILES;

-- Se crea la tabla CLIENTE, CON DNI COMO PK --
CREATE TABLE CLIENTE (
DNI CHAR (9), nombre VARCHAR (20), apellidos VARCHAR (40), telefono CHAR (9), email VARCHAR (40), 

PRIMARY KEY (DNI), UNIQUE KEY email (email)
);

-- Se crea la tabla TIENDA, con nombre como PK --
CREATE TABLE TIENDA (
nombre VARCHAR (20), provincia VARCHAR (15), localidad VARCHAR (30), direccion VARCHAR (100), telefono CHAR (9), dia_apertura ENUM('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'), dia_cierre ENUM('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'), hora_apertura TIME, hora_cierre TIME, 

PRIMARY KEY (nombre)
);

-- Se crea la tabla OPERADORA, con nombre como PK. El porcentaje es DECIMAL para que sea posible poner un valor exacto. Frecuencia es smallint porque la frecuencia GSM va de 800 a 1900 --
CREATE TABLE OPERADORA (
nombre VARCHAR (20), color_logo VARCHAR (10), porcentaje_cobertura DECIMAL (3,2), frecuencia_GSM SMALLINT (4), pagina_web VARCHAR (300), 

PRIMARY KEY (nombre)
);

-- Se crea la tabla TARIFA, con multiclave de nombre y FK nombre_operadoras. Se interpreta que tamano_datos es la cantidad de Gb o Mb que incluye la tarifa, por lo que se define decimal, por si se cruza con alguna base de facturación y se necesita saber el total de datos. Tipo_datos es char de dos porque interpreto que son mb o gb. Minutos_gratis smallint porque las tarifas ilimitadas no lo son, tienen un máximo para controlar uso indebido. SMS_gratis mediumint porque hay tarifas especiales para los envíos masivos de sms --
CREATE TABLE TARIFA (
nombre VARCHAR (20), nombre_OPERADORA VARCHAR (20), tamano_datos DECIMAL (3,2), tipo_datos CHAR (2), minutos_gratis SMALLINT (5), SMS_gratis MEDIUMINT, 

PRIMARY KEY (nombre), 

FOREIGN KEY (nombre_OPERADORA) REFERENCES OPERADORA (nombre) ON UPDATE CASCADE
);

-- Se crea la tabla MOVIL, con multiclave propia de marca y modelo. Pulgadas_pantalla es decimal porque las pulgadas se dan con decimales, son números exactos. Camara_mpx es decimal de 3 enteros, porque se da en un nº exacto y ya hay noticias de que samsung está desarrollando una cámara de 108 Mp de resolución --
CREATE TABLE MOVIL (
marca VARCHAR (20), modelo VARCHAR (20), descripcion VARCHAR (300), SO VARCHAR (20), RAM SMALLINT, pulgadas_pantalla DECIMAL (1,1), camara_mpx DECIMAL (3,1), 

PRIMARY KEY (marca, modelo)
);

-- Se crea la tabla MOVIL_LIBRE con multiclave de dos FK --
CREATE TABLE MOVIL_LIBRE (
marca_MOVIL VARCHAR (20), modelo_MOVIL VARCHAR (20), precio DECIMAL (3,2), 

FOREIGN KEY (marca_MOVIL, modelo_MOVIL) REFERENCES MOVIL (marca, modelo) ON UPDATE CASCADE
);

-- Se crea la tabla MOVIL_CONTRATO con multiclave de 3 FKs --
CREATE TABLE MOVIL_CONTRATO (
marca_MOVIL VARCHAR (20), modelo_MOVIL VARCHAR (20), nombre_OPERADORA VARCHAR (20), precio DECIMAL (3,2), 

FOREIGN KEY (marca_MOVIL, modelo_MOVIL) REFERENCES MOVIL (marca, modelo) ON UPDATE CASCADE, 

FOREIGN KEY (nombre_OPERADORA) REFERENCES OPERADORA (nombre) ON UPDATE CASCADE
);

-- Se crea la tabla OFERTA, con multiclave de 4 FKs de 2 tablas distintas --
CREATE TABLE OFERTA (
nombre_OPERADORA_TARIFA VARCHAR (20), nombre_TARIFA VARCHAR (20), marca_MOVIL_CONTRATO VARCHAR (20), modelo_MOVIL_CONTRATO VARCHAR (20), 

FOREIGN KEY (nombre_OPERADORA_TARIFA, nombre_TARIFA) REFERENCES TARIFA (nombre_OPERADORA, nombre) ON UPDATE CASCADE, 

FOREIGN KEY (marca_MOVIL_CONTRATO, modelo_MOVIL_CONTRATO) REFERENCES MOVIL_CONTRATO (marca_MOVIL, modelo_MOVIL) ON UPDATE CASCADE
);

-- Se crea la tabla COMPRA con multiclave de 4 FKs de 3 tablas --
CREATE TABLE COMPRA (
DNI_CLIENTE CHAR (9), nombre_TIENDA VARCHAR (20), marca_MOVIL_LIBRE VARCHAR (20), modelo_MOVIL_LIBRE VARCHAR (20), dia DATE, 

FOREIGN KEY (DNI_CLIENTE) REFERENCES CLIENTE (DNI) ON UPDATE CASCADE, 

FOREIGN KEY (nombre_TIENDA) REFERENCES TIENDA (nombre) ON UPDATE CASCADE, 

FOREIGN KEY (marca_MOVIL_LIBRE, modelo_MOVIL_LIBRE) REFERENCES MOVIL_LIBRE (marca_MOVIL, modelo_MOVIL) ON UPDATE CASCADE
);

-- Se crea la tabla CONTRATO con multiclave de 6 FKs de 3 tablas. Las claves que vienen de la tabla oferta, les añado contrato porque vienen en origen de esa tabla --
CREATE TABLE CONTRATO (
DNI_CLIENTE CHAR (9), nombre_TIENDA VARCHAR (20), nombre_OPERADORA_TARIFA_OFERTA VARCHAR (20), nombre_TARIFA_OFERTA VARCHAR (20), marca_MOVIL_CONTRATO_OFERTA VARCHAR (20), modelo_MOVIL_CONTRATO_OFERTA VARCHAR (20), dia DATE,


FOREIGN KEY (DNI_CLIENTE) REFERENCES CLIENTE (DNI) ON UPDATE CASCADE, 

FOREIGN KEY (nombre_TIENDA) REFERENCES TIENDA (nombre) ON UPDATE CASCADE, 

FOREIGN KEY (nombre_OPERADORA_TARIFA_OFERTA, nombre_TARIFA_OFERTA, marca_MOVIL_CONTRATO_OFERTA, modelo_MOVIL_CONTRATO_OFERTA) REFERENCES OFERTA (nombre_OPERADORA_TARIFA, nombre_TARIFA, marca_MOVIL_CONTRATO, modelo_MOVIL_CONTRATO) ON UPDATE CASCADE
);

