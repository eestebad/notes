
-- Consultas Jardineria --

-- 01. Sacar el código de oficina y la ciudad donde hay oficinas. --
SELECT CodigoOficina, Ciudad FROM Oficinas;

-- 02. Sacar cuántos empleados hay en la compañía. --
SELECT COUNT(*) FROM Empleados;

-- 03. Sacar cuántos clientes tiene cada país. --
SELECT Pais, count(*) FROM Clientes GROUP BY Pais;

-- 04. Sacar cuál fue el pago medio en 2008. --
SELECT AVG(Cantidad) FROM Pagos WHERE FechaPago BETWEEN '2008-01-01' AND '2008-12-31'; 
-- Otra forma mejor --
SELECT AVG(Cantidad) FROM Pagos WHERE YEAR(FechaPago) = '2008';

-- 05. Sacar cuántos pedidos están en cada estado ordenado descendente por el número de pedidos. Problema, hay un Pediente con un 1 y lo ordena así: 24, 29, 1, 60 --
SELECT Estado, count(*) AS 'Nº Pedidos' FROM Pedidos GROUP BY Estado ORDER BY COUNT(*) DESC;
+-----------+-------------+
| Estado    | Nº Pedidos  |
+-----------+-------------+
| Entregado |          60 |
| Pendiente |          29 |
| Rechazado |          24 |
| Pediente  |           1 |
+-----------+-------------+

-- 06. Sacar el precio del producto más caro y del más barato. No podemos sacar el nombre del producto, porque saca el primero que pilla, o si no da error. --
SELECT MAX(PrecioVenta) FROM Productos;
+------------------+
| MAX(PrecioVenta) |
+------------------+
|           462.00 |
+------------------+

SELECT MIN(PrecioVenta) FROM Productos;
+------------------+
| MIN(PrecioVenta) |
+------------------+
|             1.00 |
+------------------+
-- para que saque los dos a la vez --
SELECT MAX(PrecioVenta) AS 'Caro', MIN(PrecioVenta) AS 'Barato' FROM Productos;

-- 07. Sacar la ciudad y el teléfono de las oficinas de Estados Unidos. --
-- Primero compruebo cómo está escrito Estados Unidos --
SELECT DISTINCT Pais FROM Oficinas;
+------------+
| Pais       |
+------------+
| España     |
| EEUU       |
| Inglaterra |
| Francia    |
| Australia  |
| Japón      |
+------------+

-- Después hago la consulta: --
SELECT Ciudad, Telefono FROM Oficinas WHERE Pais = "EEUU";
+---------------+-----------------+
| Ciudad        | Telefono        |
+---------------+-----------------+
| Boston        | +1 215 837 0825 |
| San Francisco | +1 650 219 4782 |
+---------------+-----------------+


-- 08. Sacar el nombre, los apellidos y el email de los empleados a cargo de Alberto Soria. --
-- Primero hago consulta del CodigoEmpleado de Alberto Soria --
SELECT CodigoEmpleado FROM Empleados WHERE Nombre = "Alberto" AND Apellido1 = "Soria";
+----------------+
| CodigoEmpleado |
+----------------+
|              3 |
+----------------+

-- Después consulto los empleados con CodigoJefe de Alberto Soria, el 3 --
-- No se puede hacer así, con el codigo jefe, porque si cambian el código, de Alberto Soria, la consulta mostraría otro jefe: SELECT Nombre, Apellido1, Apellido2, Email FROM Empleados WHERE CodigoJefe = 3; --
+-------------+------------+-----------+---------------------------+
| Nombre      | Apellido1  | Apellido2 | Email                     |
+-------------+------------+-----------+---------------------------+
| Felipe      | Rosas      | Marquez   | frosas@jardineria.es      |
| Juan Carlos | Ortiz      | Serrano   | cortiz@jardineria.es      |
| Carlos      | Soria      | Jimenez   | csoria@jardineria.es      |
| Emmanuel    | Magaña     | Perez     | manu@jardineria.es        |
| Francois    | Fignon     |           | ffignon@gardening.com     |
| Michael     | Bolton     |           | mbolton@gardening.com     |
| Hilary      | Washington |           | hwashington@gardening.com |
| Nei         | Nishikori  |           | nnishikori@gardening.com  |
| Amy         | Johnson    |           | ajohnson@gardening.com    |
| Kevin       | Fallmer    |           | kfalmer@gardening.com     |
+-------------+------------+-----------+---------------------------+

-- La correcta sería --
SELECT Nombre, CONCAT(Apellido1, " ", Apellido2) AS 'Apellidos', Email FROM Empleados WHERE CodigoJefe = (SELECT CodigoEmpleado FROM Empleados WHERE Nombre = 'Alberto' AND Apellido1 = 'Soria');

-- 9. Sacar el cargo, nombre, apellidos y email del jefe de la empresa --
-- Primero consulto qué puestos hay --
SELECT DISTINCT Puesto FROM Empleados;
+-----------------------+
| Puesto                |
+-----------------------+
| Director General      |
| Subdirector Marketing |
| Subdirector Ventas    |
| Secretaria            |
| Representante Ventas  |
| Director Oficina      |
+-----------------------+

-- Después consulto los datos del empleado con puesto Director General --
SELECT Puesto, Nombre, Apellido1, Apellido2, Email FROM Empleados WHERE Puesto = "Director General";
+------------------+--------+-----------+-----------+----------------------+
| Puesto           | Nombre | Apellido1 | Apellido2 | Email                |
+------------------+--------+-----------+-----------+----------------------+
| Director General | Marcos | Magaña    | Perez     | marcos@jardineria.es |
+------------------+--------+-----------+-----------+----------------------+
-- Otra opción más sencilla:--
SELECT Puesto, CONCAT(Nombre, ' ', Apellido1, ' ', Apellido2),  Email FROM Empleados WHERE CodigoJefe IS NULL;


-- 10. Sacar el nombre, apellidos y cargo de aquellos que no sean representantes de ventas. --
-- Primero hago consulta de los puestos --
SELECT DISTINCT Puesto FROM Empleados;
+-----------------------+
| Puesto                |
+-----------------------+
| Director General      |
| Subdirector Marketing |
| Subdirector Ventas    |
| Secretaria            |
| Representante Ventas  |
| Director Oficina      |
+-----------------------+

-- Después consulto los datos de los que no son "Representante Ventas" --
SELECT Nombre, Apellido1, Apellido2, Puesto FROM Empleados WHERE Puesto != "Representante Ventas";
+----------+------------+-----------+-----------------------+
| Nombre   | Apellido1  | Apellido2 | Puesto                |
+----------+------------+-----------+-----------------------+
| Marcos   | Magaña     | Perez     | Director General      |
| Ruben    | López      | Martinez  | Subdirector Marketing |
| Alberto  | Soria      | Carrasco  | Subdirector Ventas    |
| Maria    | Solís      | Jerez     | Secretaria            |
| Carlos   | Soria      | Jimenez   | Director Oficina      |
| Emmanuel | Magaña     | Perez     | Director Oficina      |
| Francois | Fignon     |           | Director Oficina      |
| Michael  | Bolton     |           | Director Oficina      |
| Hilary   | Washington |           | Director Oficina      |
| Nei      | Nishikori  |           | Director Oficina      |
| Amy      | Johnson    |           | Director Oficina      |
| Kevin    | Fallmer    |           | Director Oficina      |
+----------+------------+-----------+----------------------

-- 11. Sacar el número de clientes que tiene la empresa. --
SELECT COUNT(*) FROM Clientes;

-- 12. Sacar el nombre de los clientes españoles. --
-- Primero consulto qué países hay --
SELECT DISTINCT Pais FROM Clientes;
-- Después consulto los de España y Spain, salen 27 --
SELECT NombreCliente FROM Clientes WHERE Pais = 'España' OR Pais = 'Spain';

-- 13. Sacar cuántos clientes tiene cada país. --
SELECT Pais, COUNT(*) AS 'Nº Clientes' FROM Clientes GROUP BY Pais;

-- 14. Sacar cuántos clientes tiene la ciudad de Madrid. --
-- Antes compruebo qué ciudades hay en la tabla --
SELECT DISTINCT Ciudad FROM Clientes;
SELECT Ciudad, COUNT(*) AS 'Nº Clientes Madrid' FROM Clientes WHERE Ciudad = 'Madrid';

-- 15. Sacar cuántos clientes tienen las ciudades que empiezan por M. --
SELECT Ciudad, COUNT(*) FROM Clientes WHERE Ciudad LIKE 'M%' GROUP BY Ciudad;

-- 16  Sacar el código de empleado y el número de clientes al que atiende cada representante de ventas.--
SELECT CodigoEmpleadoRepVentas, COUNT(*) FROM Clientes GROUP BY CodigoEmpleadoRepVentas;

-- 17. Sacar el número de clientes que no tiene asignado representante de ventas. --
-- Todos tienen asignado representante de ventas, por lo que la consulta devuelve Empty --
SELECT CodigoCliente, CodigoEmpleadoRepVentas FROM Clientes WHERE CodigoEmpleadoRepVentas IS NULL;

-- 18. Sacar cuál fue el primer y último pago que hizo algún cliente. --
SELECT MAX(FechaPago), MIN(FechaPago) FROM Pagos;

-- No usar ésta porque el dato que da es errońeo 
SELECT CodigoCliente, MIN(FechaPago), MAX(FechaPago) FROM Pagos GROUP BY CodigoCliente; --

-- 19. Sacar el código de cliente de aquellos clientes que hicieron pagos en 2008. --
SELECT CodigoCliente FROM Pagos WHERE YEAR(FechaPago) = 2008;

-- 20. Sacar los distintos estados por los que puede pasar un pedido. --
SELECT DISTINCT Estado FROM Pedidos;

-- 21. Sacar el número de pedido, código de cliente, fecha requerida y fecha de entrega de los pedidos que no han sido entregados a tiempo. Mejor comparar con funciones de fecha, como datadiff--
SELECT CodigoPedido AS 'Pedidos retrasados', CodigoCliente, FechaEsperada, FechaEntrega, Estado FROM Pedidos WHERE FechaEntrega > FechaEsperada;
-- versión de fernando --
SELECT CodigoPedido AS 'Pedidos retrasados', CodigoCliente, Estado, FechaEsperada, FechaEntrega, Estado FROM Pedidos WHERE FechaEntrega > FechaEsperada AND (Estado='Entregado' OR Estado='Rechazado');

-- 22. Sacar cuántos productos existen en cada línea de pedido. --
SELECT NumeroLinea, COUNT(*) FROM DetallePedidos GROUP BY NumeroLinea;
-- Por nº de pedido --
SELECT CodigoPedido AS 'Nº Pedido', SUM(Cantidad) AS 'Nº de productos' FROM DetallePedidos GROUP BY CodigoPedido;

-- 23. Sacar un listado de los 20 códigos de productos más pedidos ordenado por cantidad pedida. ¿Cómo ordena cuando tienen la misma cantidad?--
SELECT CodigoProducto, Cantidad FROM DetallePedidos ORDER BY Cantidad DESC LIMIT 20;

-- 24. Sacar el número de pedido, código de cliente, fecha requerida y fecha de entrega de los pedidos cuya fecha de entrega ha sido al menos dos días antes de la fecha requerida. --
SELECT CodigoPedido, CodigoCliente, FechaEsperada, FechaEntrega FROM Pedidos WHERE DATEDIFF(FechaEsperada, FechaEntrega) >= 2;

-- 25. Sacar la facturación que ha tenido la empresa en toda la historia, indicando la base imponible, el IVA y el total facturado. NOTA: La base imponible se calcula sumando el coste del producto por el número de unidades vendidas. El IVA, es el 21% de la base imponible, y el total, la suma de los dos campos anteriores. --
-- Se puede hacer así, que es más manual --
SELECT SUM(PrecioUnidad*Cantidad) AS 'Base Imponible', SUM(PrecioUnidad*Cantidad)*0.21 AS IVA, SUM(PrecioUnidad*Cantidad)+SUM(PrecioUnidad*Cantidad)*0.21 AS TOTAL FROM DetallePedidos;

-- o sacando el factor común:--
--BaseImponible+BaseImponible*0.21=TOTAL
-- Si sacamos el factor común, sería:
-- BaseImponible(1+1*0.21)=BaseImponible(1+0.21)=BaseImponible*1.21--

SELECT SUM(PrecioUnidad*Cantidad) AS 'Base Imponible', SUM(PrecioUnidad*Cantidad)*0.21 AS IVA, SUM(PrecioUnidad*Cantidad)*1.21 AS TOTAL FROM DetallePedidos;

-- 26. Sacar la misma información que en la pregunta anterior, pero agrupada por código de producto filtrada por los códigos que empiecen por FR. --

SELECT CodigoProducto, SUM(PrecioUnidad*Cantidad) AS 'Base Imponible', SUM(PrecioUnidad*Cantidad)*0.21 AS IVA, SUM(PrecioUnidad*Cantidad)*1.21 AS TOTAL FROM DetallePedidos WHERE CodigoProducto LIKE 'FR-%' GROUP BY CodigoProducto;

-- 27. Obtener el nombre del producto más caro. --
-- se saca así para que si hay dos con el mismo precio, saque los dos --
SELECT CodigoProducto, Nombre, PrecioVenta FROM Productos WHERE PrecioVenta=(SELECT MAX(PrecioVenta) FROM Productos);
-- si pongo sólo el max, saca sólo el precio más caro, pero no dice cuántos productos tienen ese precio y pueden ser varios. Por ejemplo, si lo hago con el min en la bbdd jardinería, hay varios.--
SELECT MAX(PrecioVenta) FROM Productos; --saca el precio máximo--

-- 28. Obtener el nombre del producto del que más unidades se hayan vendido en un mismo pedido. --
-- sin el nombre --
SELECT CodigoPedido, CodigoProducto, Cantidad FROM DetallePedidos WHERE Cantidad = (SELECT MAX(Cantidad) FROM DetallePedidos);

-- con el nombre--
SELECT Productos.Nombre, DetallePedidos.CodigoProducto, DetallePedidos.Cantidad FROM Productos, DetallePedidos WHERE Productos.CodigoProducto = DetallePedidos.CodigoProducto AND DetallePedidos.Cantidad = ( SELECT MAX(Cantidad) FROM DetallePedidos );

-- 29. Obtener los clientes cuya línea de crédito (cuánto te dejan financiar) sea mayor que los pagos que haya realizado. --
SELECT Clientes.CodigoCliente, Clientes.Limitecredito, Pagos.CodigoCliente, SUM(Pagos.Cantidad) FROM Pagos NATURAL JOIN Clientes GROUP BY Pagos.CodigoCliente HAVING Clientes.LimiteCredito > SUM(Pagos.Cantidad);
-- ASÍ NO FUNCIONA, CON WHERE, PORQUE CUANDO LLEGA AL WHERE AÚN NO HA RESUELTO EL GROUP BY --
SELECT Clientes.CodigoCliente, Clientes.Limitecredito, Pagos.CodigoCliente, SUM(Pagos.Cantidad) FROM Pagos NATURAL JOIN Clientes WHERE Clientes.LimiteCredito > SUM(Pagos.Cantidad) GROUP BY Pagos.CodigoCliente;
-- ASÍ TAMPOCO ¿POR QUÉ? ¿ES PORQUE EL WHERE COMPARA CON UNA FUNCIÓN DE AGREGACIÓN? --
SELECT Clientes.CodigoCliente, Clientes.Limitecredito, Pagos.CodigoCliente, SUM(Pagos.Cantidad) FROM Pagos NATURAL JOIN Clientes WHERE Clientes.LimiteCredito > (SELECT SUM(Pagos.Cantidad), Pagos.CodigoCliente FROM Pagos GROUP BY Pagos.CodigoCliente);

-- 30. Sacar el producto que más unidades tiene en stock y el que menos unidades tiene en stock. --
SELECT Nombre, CantidadEnStock FROM Productos WHERE CantidadEnStock = ( SELECT MAX(CantidadEnStock) FROM Productos ) OR CantidadEnStock = ( SELECT MIN(CantidadEnStock) FROM Productos );

-- SACAR EL Nº DE OFICINAS EN LAS CIUDADES QUE COMIENZAN POR S --
SELECT Ciudad, COUNT(*) FROM Clientes WHERE Ciudad LIKE 'S%' GROUP BY Ciudad HAVING COUNT(*)>1;
-- LAS CIUDADES QUE TIENEN MÁS DE 4 clientes --
SELECT Ciudad, COUNT(*) FROM Clientes GROUP BY Ciudad HAVING COUNT(*)>4;
+-------------+----------+
| Ciudad      | COUNT(*) |
+-------------+----------+
| Fuenlabrada |        5 |
| Madrid      |       11 |
+-------------+----------+


-- SACAR LOS PRODUCTOS CON CANTIDAD, STOCK MENOR DE LA MEDIA --
SELECT CodigoProducto, CantidadEnStock FROM Productos WHERE CantidadEnStock < ( SELECT AVG(CantidadEnStock) FROM Productos);

-- SACAR LAS CIUDADES CON MÁS DE 4 OFICINAS --
SELECT Ciudad, COUNT(*) FROM Clientes GROUP BY Ciudad HAVING COUNT(*)>4;

-- CIUDADES QUE EMPIECEN POR S Y TENGAN MÁS DE 1 OFICINA --
SELECT Ciudad, COUNT(*) FROM Clientes WHERE Ciudad LIKE 'S%' GROUP BY Ciudad HAVING COUNT(*)>1;

-- 31. Sacar el nombre de los clientes y el nombre de sus representantes junto con la ciudad de la oficina a la que pertenece el representante. --
SELECT Clientes.NombreCliente, Empleados.Nombre, Oficinas.Ciudad FROM Clientes, Empleados, Oficinas WHERE Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado AND Empleados.CodigoOficina = Oficinas.CodigoOficina;

-- 32. Sacar la misma información que en la pregunta anterior pero sólo de los clientes que no hayan hecho pagos. --
-- compruebo cuántos clientes han hecho pagos --
SELECT DISTINCT CodigoCliente FROM Pagos;
-- miro cuántos clientes hay en total --
SELECT COUNT(*) FROM Clientes;
-- ESTA NO ES LA SOLUCIÓN, QUEDA PENDIENTE POR FERNANDO --
SELECT Clientes.NombreCliente, Empleados.Nombre, Oficinas.Ciudad FROM Clientes, Empleados, Oficinas, Pagos WHERE Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado AND Empleados.CodigoOficina = Oficinas.CodigoOficina AND Clientes.CodigoCliente NOT IN (SELECT Pagos.CodigoCliente FROM Pagos);

-- CLIENTES QUE ESTÁN EN PAGOS --
SELECT CodigoCliente FROM Clientes WHERE CodigoCliente IN (SELECT DISTINCT CodigoCliente FROM Pagos);

-- 33. Obtener un listado con el nombre de los empleados junto con el nombre de sus jefes. Es una consulta reflexiva porque apunto a la misma tabla--
-- con alias --
SELECT Curritos.CodigoEmpleado, Curritos.Nombre, Curritos.CodigoJefe, Jefes.Nombre FROM Empleados AS Curritos JOIN Empleados AS Jefes ON Curritos.CodigoJefe=Jefes.CodigoEmpleado;

-- 34. Obtener el nombre de los clientes a los que no se les ha entregado a tiempo un pedido. --
-- Pedidos que se han entregado con retraso --
SELECT Pedidos.CodigoCliente, Pedidos.CodigoPedido, Pedidos.FechaEsperada, Pedidos.FechaEntrega FROM Pedidos WHERE Pedidos.FechaEsperada<Pedidos.FechaEntrega;
-- Con el nombre del cliente --
SELECT Clientes.NombreCliente, Pedidos.CodigoCliente, Pedidos.CodigoPedido, Pedidos.FechaEsperada, Pedidos.FechaEntrega FROM Clientes, Pedidos WHERE Pedidos.FechaEsperada<Pedidos.FechaEntrega AND Clientes.CodigoCliente = Pedidos.CodigoCliente;
-- NO SALE ORDENADO: Sólo el nombre de los clientes a los que no se les ha entregado a tiempo, es decir, que no salga repetido el cliente --
SELECT DISTINCT Clientes.NombreCliente, Pedidos.CodigoCliente FROM Clientes, Pedidos WHERE Pedidos.FechaEsperada<Pedidos.FechaEntrega AND Clientes.CodigoCliente = Pedidos.CodigoCliente;
-- SOLUCIÓN MÁS CORRECTA --
SELECT Clientes.CodigoCliente, Clientes.NombreCliente FROM Clientes WHERE Clientes.CodigoCliente IN (SELECT DISTINCT Pedidos.CodigoCliente FROM Pedidos WHERE Pedidos.FechaEsperada<Pedidos.FechaEntrega);
-- CLIENTES QUE SÍ LES HA LLEGADO EL PEDIDO A TIEMPO --
SELECT Clientes.CodigoCliente, Clientes.NombreCliente FROM Clientes WHERE Clientes.CodigoCliente IN (SELECT DISTINCT Pedidos.CodigoCliente FROM Pedidos WHERE Pedidos.FechaEsperada>=Pedidos.FechaEntrega);

-- PEDIDOS QUE HAN LLEGADO ANTES DE LA FECHA ESPERADA O EL MISMO DÍA --
SELECT Pedidos.CodigoPedido, Pedidos.FechaEsperada, Pedidos.FechaEntrega FROM Pedidos WHERE Pedidos.FechaEsperada>=Pedidos.FechaEntrega;

-- 35. Sacar un listado de clientes indicando el nombre del cliente y cuántos pedidos ha realizado. --
-- SACA CUÁNTOS PEDIDOS HA HECHO CADA CLIENTE, POR CODIGOCLIENTE --
SELECT Pedidos.CodigoCliente, COUNT(*) AS NumPedidos FROM Pedidos GROUP BY Pedidos.CodigoCliente;

-- SOLUCIÓN FERNANDO --
SELECT Clientes.NombreCliente, Clientes.CodigoCliente, COUNT(*) FROM Clientes, Pedidos WHERE Clientes.CodigoCliente=Pedidos.CodigoCliente GROUP BY Pedidos.CodigoCliente;

-- 36. Sacar un listado con los nombres de los clientes y el total pagado por cada uno de ellos. --
SELECT Clientes.NombreCliente, SUM(Pagos.Cantidad) FROM Clientes, Pagos WHERE Clientes.CodigoCliente=Pagos.CodigoCliente GROUP BY Pagos.CodigoCliente;
-- ésta saca cuando el pago haya sido mayor de 10.000. El HAVING se pone después del GROUP BY porque primero tiene que resolver el GROUP BY para resolver el HAVING --
SELECT Clientes.NombreCliente, Clientes.CodigoCliente, SUM(Pagos.Cantidad) FROM Clientes, Pagos WHERE Clientes.CodigoCliente=Pagos.CodigoCliente GROUP BY Pagos.CodigoCliente HAVING SUM(Pagos.Cantidad)>10000;


-- 37. Sacar el nombre de los ctes que hayan hecho pedidos en 2008. --
-- SACA LOS PEDIDOS HECHOS EN 2008 --
SELECT Pedidos.FechaPedido FROM Pedidos HAVING YEAR(FechaPedido)=2008;
-- solución carlos--
SELECT DISTINCT NombreCliente AS 'Cliente' 
FROM Clientes C, Pedidos P 
WHERE C.CodigoCliente = P.CodigoCliente 
AND YEAR(FechaPedido) = 2008;
--SOLUCIÓN FERNANDO --
SELECT DISTINCT Clientes.NombreCliente FROM Clientes, Pedidos WHERE Pedidos.CodigoCliente=Clientes.CodigoCliente AND YEAR(FechaPedido)=2008;

-- 38. Listar el nombre del cliente y el nombre y apellido de sus representantes de aquellos clientes que no hayan realizado pagos. --

-- fernando dice que no la hace --

-- 39. Sacar un listado de clientes donde aparezca el nombre de su comercial y la ciudad donde está su oficina. --
-- ver foto --

--40 Sacar el nombre, apellidos, oficina y cargo de aquellos que no sean representantes de ventas. --
SELECT Empleados.Nombre, Clientes.CodigoCliente, Clientes.CodigoEmpleadoRepVentas FROM Clientes RIGHT JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado WHERE Clientes.CodigoCliente IS NULL;

-- 41. Sacar cuántos empleados tiene cada oficina, mostrando el nombre de la ciudad donde está la oficina. --

SELECT COUNT(Empleados.CodigoEmpleado) AS 'Nº de Empleados', Oficinas.Ciudad FROM Empleados, Oficinas WHERE Empleados.CodigoOficina=Oficinas.CodigoOficina GROUP BY Oficinas.Ciudad; 

+------------------+----------------------+
| Nº de Empleados  | Ciudad               |
+------------------+----------------------+
|                4 | Barcelona            |
|                3 | Boston               |
|                3 | Londres              |
|                4 | Madrid               |
|                3 | Paris                |
|                2 | San Francisco        |
|                3 | Sydney               |
|                6 | Talavera de la Reina |
|                3 | Tokyo                |
+------------------+----------------------+

-- LO MISMO CON NATURAL JOIN --
SELECT COUNT(Empleados.CodigoEmpleado) AS 'Nº de Empleados', Oficinas.Ciudad FROM Empleados NATURAL JOIN Oficinas GROUP BY Oficinas.Ciudad;

-- LO MISMO LAS TABLAS CON ALIAS --
SELECT COUNT(E.CodigoEmpleado) AS 'Nº de Empleados', O.Ciudad FROM Empleados AS E NATURAL JOIN Oficinas AS O GROUP BY O.Ciudad;

-- 42 Sacar un listado con el nombre de los empleados, y el nombre de sus respectivos jefes. Usamos alias porque es una reflexiva SI se hiciera sólo con CodigoEmpledo y COdigoJefe no haría falta, pero al meter el nombre sí, porque es la misma columna 2 veces. Se pone ON E.CodigoJefe=J.CodigoEmpleado porque queremos el codgoJefe de los Empleados y el codigoEmpleado de los Jefes--
SELECT E.CodigoEmpleado, E.Nombre, E.CodigoJefe, J.Nombre FROM Empleados AS E JOIN Empleados AS J ON E.CodigoJefe=J.CodigoEmpleado;

-- si quiero sacar todos los empleados pongo un RIGHT: --
SELECT E.CodigoEmpleado, E.Nombre, E.CodigoJefe, J.Nombre FROM Empleados AS E RIGHT JOIN Empleados AS J ON E.CodigoJefe=J.CodigoEmpleado;

-- 43 Sacar el nombre, apellido, oficina (ciudad) y cargo del empleado que no represente a ningún cliente. --
-- SOLUCIÓN de Fernando --
SELECT Empleados.CodigoEmpleado, CONCAT_WS(' ', Empleados.Nombre, Empleados.Apellido1),  Oficinas.Ciudad, Empleados.Puesto FROM Clientes RIGHT JOIN Empleados NATURAL JOIN Oficinas ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado WHERE Clientes.CodigoEmpleadoRepVentas IS NULL ORDER BY Empleados.CodigoEmpleado;

-- la mía --
SELECT Empleados.Nombre, Empleados.Apellido1, Empleados.Puesto, Empleados.CodigoEmpleado, Oficinas.Ciudad FROM Empleados NATURAL JOIN Oficinas WHERE Empleados.CodigoEmpleado NOT IN (SELECT Clientes.CodigoEmpleadoRepVentas FROM Clientes); 


-- 44 Sacar la media de unidades en stock de los productos agrupados por gama. --
SELECT Productos.Gama, AVG(Productos.CantidadEnStock) AS 'Media stock' FROM Productos GROUP BY Productos.Gama;
+--------------+-------------+
| Gama         | Media stock |
+--------------+-------------+
| Aromáticas   |    140.0000 |
| Frutales     |    182.1296 |
| Herramientas |     15.0000 |
| Ornamentales |     81.9286 |
+--------------+-------------+
-- LO MISMO CON ALIAS PARA LA TABLA --
SELECT P.Gama, AVG(P.CantidadEnStock) AS 'Media stock' FROM Productos AS P GROUP BY P.Gama;

-- 45 Sacar los clientes que residan en la misma ciudad donde hay una oficina, indicando dónde está la oficina. NO SE PUEDE HACER CON NATURAL JOIN PORQUE NO CRUZAMOS POR PK --
SELECT Clientes.CodigoCliente, Oficinas.Ciudad FROM Clientes, Oficinas WHERE Clientes.Ciudad=Oficinas.Ciudad;

-- 46 Sacar los clientes que residan en ciudades donde no hay oficinas ordenado por la ciudad donde residen. --
SELECT Clientes.CodigoCliente, Clientes.Ciudad FROM Clientes WHERE Clientes.Ciudad NOT IN (SELECT Oficinas.Ciudad FROM Oficinas) ORDER BY Clientes.Ciudad;
-- SOLUCIÓN FERNANDO ES CON DISTINCT, AUNQUE SACA LO MISMO--
SELECT Clientes.CodigoCliente, Clientes.Ciudad FROM Clientes WHERE Clientes.Ciudad NOT IN (SELECT DISTINCT Oficinas.Ciudad FROM Oficinas) ORDER BY Clientes.Ciudad;
-- sacarlo con la dirección --
SELECT Clientes.CodigoCliente, Oficinas.Ciudad, Oficinas.LineaDireccion1 FROM Clientes JOIN Empleados NATURAL JOIN Oficinas ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado WHERE Clientes.Ciudad IN (SELECT DISTINCT Oficinas.Ciudad FROM Oficinas);

-- 47 Sacar el número de clientes que tiene asignado cada representante de ventas. --
SELECT Clientes.CodigoEmpleadoRepVentas AS 'Representante', COUNT(*) FROM Clientes GROUP BY Clientes.CodigoEmpleadoRepVentas;

-- 48 Sacar cuál fue el cliente que hizo el pago con mayor cuantía y el que hizo el pago con menor cuantía. --
SELECT Pagos.CodigoCliente, Pagos.Cantidad FROM Pagos WHERE Pagos.Cantidad = ( SELECT MAX(Pagos.Cantidad) FROM Pagos ) OR Cantidad = ( SELECT MIN(Pagos.Cantidad) FROM Pagos );

-- 49 Sacar un listado con el precio total de cada pedido. --
SELECT DetallePedidos.CodigoPedido, SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad) AS 'Precio Total' FROM DetallePedidos GROUP BY DetallePedidos.CodigoPedido;

-- 50 Sacar los clientes que hayan hecho pedido en el 2008 por una cuantía superior a 2000 euros. --
SELECT Pedidos.CodigoCliente, SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad) FROM Pedidos NATURAL JOIN DetallePedidos WHERE YEAR(Pedidos.FechaPedido)=2008 GROUP BY Pedidos.CodigoCliente HAVING SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad) > 2000;
+---------------+----------------------------------------------------------+
| CodigoCliente | SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad) |
+---------------+----------------------------------------------------------+
|             1 |                                                  3600.00 |
|             4 |                                                  4002.00 |


--solución victor usando el alias de la columna y agrupando por pedido--
SELECT Clientes.NombreCliente, Pedidos.CodigoPedido, SUM(DetallePedidos.Cantidad * DetallePedidos.PrecioUnidad) AS Cuantia FROM Pedidos NATURAL JOIN DetallePedidos NATURAL JOIN Clientes WHERE YEAR(Pedidos.FechaPedido) = 2008 GROUP BY DetallePedidos.CodigoPedido HAVING Cuantia > 2000;
+--------------------------------+--------------+----------+
| NombreCliente                  | CodigoPedido | Cuantia  |
+--------------------------------+--------------+----------+
| Tendo Garden                   |            3 | 10850.00 |
| DGPRODUCTIONS GARDEN           |            9 |  2535.00 |


-- SACAR EL IMPORTE TOTAL DE CADA PEDIDO --
SELECT Pedidos.CodigoCliente, SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad) FROM Pedidos NATURAL JOIN DetallePedidos GROUP BY Pedidos.CodigoCliente;
+---------------+----------------------------------------------------------+
| CodigoCliente | SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad) |
+---------------+----------------------------------------------------------+
|             1 |                                                  6165.00 |
|             3 |                                                 10926.00 |
|             4 |                                                 81849.00 |
|             5 |                                                 23794.00 |


-- 51 Sacar cuántos pedidos tiene cada cliente en cada estado. ES CON DOS GROUP BY--
-- SOLUCIÓN FERNANDO --
SELECT Pedidos.CodigoCliente, Pedidos.Estado, COUNT(*) FROM Pedidos GROUP BY Pedidos.CodigoCliente, Pedidos.Estado ORDER BY Pedidos.CodigoCliente;

-- 52 Sacar los clientes que han pedido más de 200 unidades de cualquier producto. --
-- SACA EL Nº DE PRODUCTOS PEDIDO POR CADA CLIENTE --
SELECT Pedidos.CodigoCliente, SUM(DetallePedidos.Cantidad) FROM Pedidos NATURAL JOIN DetallePedidos GROUP BY Pedidos.CodigoCliente;
+---------------+------------------------------+
| CodigoCliente | SUM(DetallePedidos.Cantidad) |
+---------------+------------------------------+
|             1 |                         1218 |
|             3 |                          527 |
|             4 |                         1480 |
|             5 |                          882 |

-- SACA EL Nº DE PRODUCTOS PEDIDO POR CADA CLIENTE, POR CADA PRODUCTO --
SELECT Pedidos.CodigoCliente, DetallePedidos.CodigoProducto, DetallePedidos.Cantidad FROM Pedidos NATURAL JOIN DetallePedidos;
+---------------+----------------+----------+
| CodigoCliente | CodigoProducto | Cantidad |
+---------------+----------------+----------+
|             1 | FR-106         |        3 |
|             1 | FR-108         |        1 |
|             1 | FR-11          |       10 |

-- SOLUCIÓN, SACA LOS CLIENTES QUE HAN PEDIDO MÁS DE 200 PRODUCTOS DE CUALQUIER PRODUCTO --
SELECT Pedidos.CodigoCliente, DetallePedidos.CodigoProducto, DetallePedidos.Cantidad FROM Pedidos NATURAL JOIN DetallePedidos WHERE DetallePedidos.Cantidad > 200;
+---------------+----------------+----------+
| CodigoCliente | CodigoProducto | Cantidad |
+---------------+----------------+----------+
|             1 | AR-008         |      450 |
|             1 | AR-009         |      290 |
|             4 | FR-17          |      423 |

--FERNANDO AÑADE DISTINCT PARA QUE NO SLGAN LOS CLIENTES REPETIDOS --
SELECT DISTINCT Pedidos.CodigoCliente FROM Pedidos NATURAL JOIN DetallePedidos WHERE DetallePedidos.Cantidad > 200;



-- 53 Obtener el nombre del cliente con mayor limite de crédito. Primero hacemos la consulta del MAX y luego la metemos como subconsulta. Tb se puede sacar con order by y limit 1, pero no es óptimo --
SELECT Clientes.CodigoCliente, Clientes.LimiteCredito FROM Clientes WHERE Clientes.LimiteCredito = (SELECT MAX(Clientes.LimiteCredito) FROM Clientes);
+---------------+---------------+
| CodigoCliente | LimiteCredito |
+---------------+---------------+
|             5 |     600000.00 |
+---------------+---------------+
-- Fernando con el nombre --
SELECT Clientes.NombreCliente, Clientes.LimiteCredito FROM Clientes WHERE Clientes.LimiteCredito = (SELECT MAX(Clientes.LimiteCredito) FROM Clientes);


-- 54 Obtener el nombre, apellido1 y cargo de los empleados que no representen a ningún cliente. --
SELECT CONCAT(Empleados.Nombre, " ", Empleados.Apellido1) AS 'Apellidos', Empleados.CodigoEmpleado, Empleados.Puesto FROM Empleados WHERE Empleados.CodigoEmpleado NOT IN (SELECT Clientes.CodigoEmpleadoRepVentas FROM Clientes);

-- Solución de Fernando. No haría falta que el join fuese left, pero sólo con join da error---
SELECT Empleados.Nombre, Empleados.Apellido1, Empleados.Puesto FROM Empleados LEFT JOIN Clientes ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas WHERE Clientes.CodigoCliente IS NULL;

-- 55 Sacar un listado con el nombre de cada cliente y el nombre y apellido de su representante de ventas. --
SELECT Clientes.NombreCliente, CONCAT(Empleados.Nombre, " ", Empleados.Apellido1) AS 'Nombre Empleado' FROM Clientes, Empleados WHERE Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas;

-- 56 Mostrar el nombre de los clientes que no hayan realizado pagos junto con el nombre de sus representantes de ventas. --
SELECT DISTINCT Clientes.NombreCliente, Empleados.Nombre, Pagos.Cantidad FROM Clientes LEFT JOIN Empleados ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas LEFT JOIN Pagos ON Clientes.CodigoCliente=Pagos.CodigoCliente WHERE Pagos.Cantidad IS NULL ORDER BY Clientes.NombreCliente;

--Solución de Fernando, le añado el distinct--
SELECT DISTINCT Clientes.NombreCliente, Empleados.Nombre, Empleados.Apellido1 FROM Empleados JOIN Clientes NATURAL LEFT JOIN Pagos ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas WHERE Pagos.CodigoCliente IS NULL;

-- nombre cliente con el nombre de su representante --
SELECT DISTINCT Clientes.NombreCliente, Empleados.Nombre FROM Clientes JOIN Empleados ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas;

-- 57 Listar las ventas totales de los productos que hayan facturado más de 3000 euros. Se mostrará el nombre, unidades vendidas, total facturado y total facturado con impuestos (21% IVA). --
SELECT DetallePedidos.CodigoProducto, Productos.Nombre, SUM(DetallePedidos.Cantidad) AS 'Unidades vendidas', SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad) AS 'Facturación', SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad)*1.21 AS 'Facturación con IVA' FROM DetallePedidos NATURAL JOIN Productos GROUP BY DetallePedidos.CodigoProducto HAVING SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad) > 3000;

+----------------+---------------------------+-------------------+--------------+----------------------+
| CodigoProducto | Nombre                    | Unidades vendidas | Facturación  | Facturación con IVA  |
+----------------+---------------------------+-------------------+--------------+----------------------+
| 30310          | Azadón                    |               220 |      2640.00 |            3194.4000 |
| FR-11          | Limonero 30/40            |               131 |     13092.00 |           15841.3200 |
| FR-4           | Naranjo calibre 8/10      |                88 |      2552.00 |            3087.9200 |

-- Solución Fernando --
SELECT DetallePedidos.CodigoProducto, Productos.Nombre, SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad) AS 'Facturado', SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad)*1.21 FROM DetallePedidos NATURAL JOIN Productos GROUP BY DetallePedidos.CodigoProducto HAVING Facturado>3000;

-- 58 Listar la dirección de las oficinas que tengan clientes en Fuenlabrada. --
SELECT Oficinas.CodigoOficina, Oficinas.LineaDireccion1, Clientes.Ciudad FROM Oficinas NATURAL JOIN Empleados JOIN Clientes ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas WHERE Clientes.Ciudad LIKE 'F%';
+---------------+------------------------------+-------------+
| CodigoOficina | LineaDireccion1              | Ciudad      |
+---------------+------------------------------+-------------+
| MAD-ES        | Bulevar Indalecio Prieto, 32 | Fuenlabrada |
| MAD-ES        | Bulevar Indalecio Prieto, 32 | Fuenlabrada |
| TAL-ES        | Francisco Aguirre, 32        | Fuenlabrada |

--PARA COMPROBAR SI ESTÁ BIEN, PORQUE SALEN OFICINAS HASTA DE SYDNEY--
-- MIRO CODIGO EMPLEADO DE LOS CLIENTES DE FUENLABRADA --
SELECT Empleados.CodigoEmpleado, Oficinas.CodigoOficina, Oficinas.LineaDireccion1, Clientes.Ciudad FROM Oficinas NATURAL JOIN Empleados JOIN Clientes ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas WHERE Clientes.Ciudad LIKE 'F%';
+----------------+---------------+------------------------------+-------------+
| CodigoEmpleado | CodigoOficina | LineaDireccion1              | Ciudad      |
+----------------+---------------+------------------------------+-------------+
|              8 | MAD-ES        | Bulevar Indalecio Prieto, 32 | Fuenlabrada |
|              8 | MAD-ES        | Bulevar Indalecio Prieto, 32 | Fuenlabrada |
|              5 | TAL-ES        | Francisco Aguirre, 32        | Fuenlabrada |
|              5 | TAL-ES        | Francisco Aguirre, 32        | Fuenlabrada |
|              5 | TAL-ES        | Francisco Aguirre, 32        | Fenlabrada  |
|             30 | SYD-AU        | 5-11 Wentworth Avenue        | Fuenlabrada |
+----------------+---------------+------------------------------+-------------+
-- MIRO A QUÉ CÓDIGO DE OFICINA PERTENECE CADA EMPLEADO --
select Empleados.CodigoEmpleado, Empleados.CodigoOficina FROM Empleados;
+----------------+---------------+
| CodigoEmpleado | CodigoOficina |
+----------------+---------------+
|             11 | BCN-ES        |
|             12 | BCN-ES        |
|             13 | BCN-ES        |
|             14 | BCN-ES        |
|             20 | BOS-USA       |
|             21 | BOS-USA       |
|             22 | BOS-USA       |
|             26 | LON-UK        |
|             27 | LON-UK        |
|             28 | LON-UK        |
|              7 | MAD-ES        |
|              8 | MAD-ES        |
|              9 | MAD-ES        |
|             10 | MAD-ES        |
|             15 | PAR-FR        |
|             16 | PAR-FR        |
|             17 | PAR-FR        |
|             18 | SFC-USA       |
|             19 | SFC-USA       |
|             29 | SYD-AU        |
|             30 | SYD-AU        |
|             31 | SYD-AU        |

-- MIRO EL EMPLEADO DE LA OFICINA DE SYDNEY POR EL CÓDIGO EMPLEADO EN LA TABLA CLIENTES Y EFECTIVAMENTE TIENE CLIENTES EN FUENLABRADA--
select Clientes.CodigoEmpleadoRepVentas, Clientes.Ciudad FROM Clientes WHERE Clientes.CodigoEmpleadoRepVentas=30;
+-------------------------+-------------+
| CodigoEmpleadoRepVentas | Ciudad      |
+-------------------------+-------------+
|                      30 | Madrid      |
|                      30 | Madrid      |
|                      30 | Humanes     |
|                      30 | Getafe      |
|                      30 | Fuenlabrada |
+-------------------------+-------------


-- 59 Sacar el cliente que hizo el pedido de mayor cuantía. --

-- SACAR TODOS LOS CLIENTES CON SUS PEDIDOS, HAYAN REALIZADO PEDIDOS O NO --
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Pedidos.Estado FROM Pedidos NATURAL RIGHT JOIN Clientes;

-- SACAR EL Nº DE PEDIDOS QUE HA REALIZADO CADA CLIENTE, siempre que haya hecho algún pedido --
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, COUNT(*) FROM Pedidos NATURAL JOIN Clientes GROUP BY Pedidos.CodigoCliente;
+---------------+--------------------------------+----------+
| CodigoCliente | NombreCliente                  | COUNT(*) |
+---------------+--------------------------------+----------+
|             1 | DGPRODUCTIONS GARDEN           |       11 |
|             3 | Gardening Associates           |        9 |
|             4 | Gerudo Valley                  |        5 |

-- SACAR EL Nº DE PRODUCTOS POR PEDIDO QUE HA PEDIDO CADA CLIENTE --
SELECT Clientes.CodigoCliente, Pedidos.CodigoPedido, SUM(DetallePedidos.Cantidad) FROM Clientes NATURAL JOIN Pedidos NATURAL JOIN DetallePedidos GROUP BY DetallePedidos.CodigoPedido;
+---------------+--------------+------------------------------+
| CodigoCliente | CodigoPedido | SUM(DetallePedidos.Cantidad) |
+---------------+--------------+------------------------------+
|             5 |            1 |                          113 |
|             5 |            2 |                          164 |
|             5 |            3 |                          232 |
|             5 |            4 |                          179 |
|             1 |            8 |                           14 |
|             1 |            9 |                          625 |

-- SACAR CUÁNTOS CLIENTES LLEVA CADA EMPLEADO --
SELECT Empleados.CodigoEmpleado, COUNT(*) FROM Clientes JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado GROUP BY Clientes.CodigoEmpleadoRepVentas;
+----------------+----------+
| CodigoEmpleado | COUNT(*) |
+----------------+----------+
|              5 |        5 |
|              8 |        4 |
|              9 |        2 |
|             11 |        5 |

-- SI QUIERO SACAR QUÉ CLIENTES LLEVAN TODOS LOS EMPLEADOS USO LEFT JOIN. Por ejemplo, Marcos que es el jefe y no lleva clientes --
SELECT Empleados.CodigoEmpleado, Empleados.Nombre, COUNT(Clientes.CodigoEmpleadoRepVentas) FROM Empleados LEFT JOIN Clientes ON Empleados.CodigoEmpleado = Clientes.CodigoEmpleadoRepVentas GROUP BY Empleados.CodigoEmpleado;

+----------------+-----------------+-----------------------------------------+
| CodigoEmpleado | Nombre          | COUNT(Clientes.CodigoEmpleadoRepVentas) |
+----------------+-----------------+-----------------------------------------+
|              1 | Marcos          |                                       0 |
|              2 | Ruben           |                                       0 |
|              3 | Alberto         |                                       0 |
|              4 | Maria           |                                       0 |
|              5 | Felipe          |                                       5 |
|              6 | Juan Carlos     |                                       0 |
|              7 | Carlos          |                                       0 |
|              8 | Mariano         |                                       4 |


-- SACAR LOS PEDIDOS DE LOS CLIENTES --
SELECT Clientes.NombreCliente, Pedidos.CodigoPedido FROM Clientes NATURAL JOIN Pedidos ORDER BY Pedidos.CodigoPedido;

-- SACAR TODOS LOS CLIENTES, HAYAN HECHO O NO PEDIDOS CON SUS PEDIDOS --
SELECT Clientes.NombreCliente, Pedidos.CodigoPedido FROM Clientes NATURAL LEFT JOIN Pedidos ORDER BY Pedidos.CodigoPedido;

-- SACAR TODOS LOS CLIENTES, HAYAN HECHO O NO PEDIDOS CON SUS PEDIDOS ORDENADO POR CODIGO CLIENTE --
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Pedidos.CodigoPedido FROM Clientes NATURAL LEFT JOIN Pedidos ORDER BY Clientes.CodigoCliente;

-- SACAR LOS CLIENTES QUE NO HAN HECHO PEDIDOS --
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Pedidos.CodigoPedido FROM Clientes NATURAL LEFT JOIN Pedidos WHERE Pedidos.CodigoPedido IS NULL ORDER BY Clientes.CodigoCliente;

-- Producto más caro --
SELECT Nombre, PrecioVenta FROM Productos WHERE PrecioVenta = (SELECT MAX(PrecioVenta) FROM Productos);

