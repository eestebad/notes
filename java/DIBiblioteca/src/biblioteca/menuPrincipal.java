package biblioteca;

import java.util.Scanner;

/*Escriba un programa para una biblioteca que contenga libros y
revistas.
a) Las caracter�sticas comunes que se almacenan tanto para las revistas
como para los libros son el c�digo, el t�tulo y el a�o de publicaci�n. Estas
tres caracter�sticas se pasan por par�metro en el momento de crear los
objetos.
b) Los libros tienen adem�s un atributo prestado. Los libros cuando se
crean no est�n prestados.
c) Las revistas tienen un n�mero. En el momento de crear las revistas se
pasa el n�mero por par�metro.
d) Tanto las revistas como los libros deben tener (aparte de los
constructores) un m�todo toString() que devuelve el valor de todos los
atributos en una cadena de caracteres. Tambi�n tienen un m�todo que
devuelve el a�o de publicaci�n y otro para el c�digo.
e) Para prevenir posibles cambios en el programa se tiene que
implementar una interfaz Prestable con los m�todos prestar(), devolver()
y prestado(). La clase Libro implementa esta interfaz.
*/

public class menuPrincipal {

	public static void main(String[] args) {
		
		Scanner leerTeclado = new Scanner (System.in);
		
		//boolean prestadoLibro = false;
		libro odisea = new libro (123456, "Odisea", 1980);
		System.out.println("Libros:\n"+ "* Odisea: " + " \n\t" + odisea.toString() + " \n\t");
		
		revista historia = new revista (234567, "Historia", 2020, 57);
		System.out.println("Revistas:\n"+ "* Historia: " + " \n\t" + historia.toString() + " \n\t");
		
		historia.getCodigo(historia.codigo);
		historia.getAnioPublicacion(historia.anioPublicacion);
		//Pruebo si funciona el m�todo para comprobar si el libro se puede prestar o no:
		odisea.prestar(odisea.prestadoLibro);


		leerTeclado.close();
	}

}
