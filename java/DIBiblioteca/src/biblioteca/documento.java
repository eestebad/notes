package biblioteca;

public class documento {
	public int codigo;
	public String titulo;
	public int anioPublicacion;
	
	//Constructor:
	public documento (int codigo, String titulo, int anioPublicacion) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.anioPublicacion = anioPublicacion;
	}
	
	//M�todos getter y setter:
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public String titulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAnioPublicacion() {
		return anioPublicacion;
	}
	
	public void setAnioPublicacion(int anioPublicacion) {
		this.anioPublicacion = anioPublicacion;
	}

	@Override
	public String toString() {
		return "documento [C�digo=" + codigo + ", T�tulo=" + titulo +", A�o de publicaci�n=" + anioPublicacion + "]";
	};
	
}
