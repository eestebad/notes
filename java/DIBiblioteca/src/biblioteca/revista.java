package biblioteca;

public class revista extends documento{
	protected int numero;
	
	//Constructor:
	public revista (int codigo, String titulo, int anioPublicacion, int numero) {
		super(codigo, titulo, anioPublicacion);
		this.numero = numero;
	}
	
	//Getter y Setter:
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero= numero;
	}
	
	@Override
	public String toString() {
		return "revista [C�digo=" + codigo + ", T�tulo=" + titulo +", A�o de publicaci�n=" + anioPublicacion + ", N�mero=" + numero + "]";
	};
	
	//M�todos que devuelven el c�digo y el a�o de publicaci�n:
	public void getCodigo(int codigo) {
		System.out.println("El c�digo de la revista es: " + codigo);
	}
	
	public void getAnioPublicacion(int anioPublicacion) {
		System.out.println("El a�o de publicaci�n de la revista es: " + anioPublicacion);
	}
	

}
