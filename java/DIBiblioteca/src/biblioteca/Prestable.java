package biblioteca;

public interface Prestable {
	
	public void prestar(boolean prestadoLibro);
	
	public void devolver();
	
	public boolean prestado(boolean prestado);


}
