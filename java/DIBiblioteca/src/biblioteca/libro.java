package biblioteca;

public class libro extends documento implements Prestable{
	public boolean prestadoLibro;
	
	//Constructor para crear un libro nuevo, que siempre inicia prestadoLibro a false:
	public libro (int codigo, String titulo, int anioPublicacion) {
		super(codigo, titulo, anioPublicacion);
		this.prestadoLibro = false;
	}
	//Constructor general:
	public libro (int codigo, String titulo, int anioPublicacion, boolean prestadoLibro) {
		super(codigo, titulo, anioPublicacion);
		this.prestadoLibro = prestadoLibro;
	}
	

	//Getter y Setter:
	public boolean getPrestadoLibro() {
		return prestadoLibro;
	}

	public void setPrestadoLibro(boolean prestadoLibro) {
		this.prestadoLibro = prestadoLibro;
	}
	
	@Override
	public String toString() {
		return "libro [C�digo=" + codigo + ", T�tulo=" + titulo +", A�o de publicaci�n=" + anioPublicacion + ", Prestado=" + prestadoLibro + "]";
	}

	//M�todos de la interfaz:

	@Override
	public void prestar(boolean prestadoLibro) {
		prestado(prestadoLibro);
		if(prestado(prestadoLibro))
			System.out.println("El libro ya est� prestado.\n");
		else 
			System.out.println("El libro est� disponible.\n");
		
	}

	@Override
	public void devolver() {
		// Comprobar�a que el c�digo del libro que me devuelven existe en mi base de datos y si es as�, cambiar�a el estado de prestadoLibro a false.
		
	}

	@Override
	public boolean prestado(boolean prestadoLibro) {
		return prestadoLibro;
		
	};
	

}
