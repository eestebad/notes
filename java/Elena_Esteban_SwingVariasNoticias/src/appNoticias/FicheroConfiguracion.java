package appNoticias;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class FicheroConfiguracion {
	protected File ficheroConfiguracion;
	
	public FicheroConfiguracion()
	{
		ficheroConfiguracion = new File("configuracion.txt");
	}
	
	//M�todo para abrir fichero en escritura:
	public BufferedWriter abrirFichero(String nombreFichero)
	{
		FileWriter ficheroFW = null;
		BufferedWriter ficheroBW = null;
		
		try {
			File fichero = new File(nombreFichero);
		// 1. Comprobar si el fichero existe:
			if(!fichero.exists()) //Si no existe, crearlo:
			{
				fichero.createNewFile();
					System.out.println("Se ha creado el fichero\n");
			}else
			{ // 2. Si ya existe, abrirlo:
					ficheroFW = new FileWriter(fichero);
					ficheroBW = new BufferedWriter(ficheroFW);
				}
				// No cerramos el fichero porque vamos a operar sobre �l.
				// Se cerrar� en el m�todo que corresponda, con el m�todo cerrar.
			}catch (Exception e) {
				e.printStackTrace();
			}
		return ficheroBW;
	}
	
	//M�todo para abrir fichero en lectura:
			public BufferedReader abrirFicheroLectura(String nombreFichero)
			{
				FileReader ficheroFR = null;
				BufferedReader ficheroBR = null;
				
				try {
					File fichero = new File(nombreFichero);
				// 1. Comprobar si el fichero existe:
					if(!fichero.exists()) //Si no existe, crearlo:
					{
						fichero.createNewFile();
							System.out.println("Se ha creado el fichero\n");
					}else
					{ // 2. Si ya existe, abrirlo:
							ficheroFR = new FileReader(fichero);
							ficheroBR = new BufferedReader(ficheroFR);
						}
						// No cerramos el fichero porque vamos a operar sobre �l.
						// Se cerrar� en el m�todo que corresponda
					}catch (Exception e) {
						e.printStackTrace();
					}
				return ficheroBR;
			}

}
