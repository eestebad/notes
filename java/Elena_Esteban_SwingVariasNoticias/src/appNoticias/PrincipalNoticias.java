package appNoticias;

import java.awt.EventQueue;
import javax.swing.event.*;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JList;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.eclipse.swt.widgets.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;

import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.print.attribute.standard.DateTimeAtCreation;
import javax.swing.AbstractAction;
import javax.swing.AbstractListModel;
import javax.swing.Action;
import javax.swing.BoundedRangeModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.Panel;
import javax.swing.border.EmptyBorder;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.Toolkit;
import java.awt.TextField;
import javax.swing.JPasswordField;
import javax.swing.JCheckBox;
import javax.swing.ScrollPaneConstants;

/**
 * Aplicaci�n a la que se accede con usuario y contrase�a, permite configurar qu� noticias se quieren.
 * @author Elena Esteban.
 *
 */

public class PrincipalNoticias{

	private JFrame frame;
	JPanel panelCarga = new JPanel() {
		@Override
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			Image imagen = null;
			try
			{
				imagen = ImageIO.read(new File("src/fondo_carga.jpg"));
			}catch(IOException e)
			{
				JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
				System.exit(0);
			}
			g.drawImage(imagen, 0, 0, getWidth(), getHeight(), null);
		}
	};
	JPanel panelLogin = new JPanel();
	JPanel panelOpciones = new JPanel();
	JPanel panelNoticiasActuales = new JPanel();
	JPanel panelConfigurar = new JPanel();
	JPanel panelNoticiasAntiguas = new JPanel();
	private JPasswordField passwordField;
	private JButton btnConfigurar;
	private JTextArea txtANDeportes1;
	private JTextArea txtANDeportes2;
	private JTextArea txtANDeportes3;
	private JTextArea txtANDeportes4;
	private JTextArea txtAEconomia1;
	private JTextArea txtAEconomia2;
	private JTextArea txtAEconomia3;
	private JTextArea txtAEconomia4;
	private JTextArea txtAInternacional1;
	private JTextArea txtAInternacional2;
	private JTextArea txtAInternacional3;
	private JTextArea txtAInternacional4;
	private JTextArea txtANacional1;
	private JTextArea txtANacional2;
	private JTextArea txtANacional3;
	private JTextArea txtANacional4;
	JTextArea textAreaMuestraAntiguas = new JTextArea();
	Usuario usuarios = new Usuario();
	Timer tiempo;
	int contador = 0;
	String stringUsuario = new String();
	String stringPassword = new String();
	String stringFecha = new String();
	int cuentaAccesos = 0;
	Configuracion configuracion = new Configuracion();
	Configuracion tablaConfiguracion[] = new Configuracion[4];
	FicheroNoticiasAntiguas ficheroNoticiasAntiguas = new FicheroNoticiasAntiguas();
	int avanceFuentes= 0;
	TablaConfiguraciones tablaConfiguraciones = new TablaConfiguraciones();
	//tabla para guardar todas las fuentes.
	String fuentes[] = new String[2000];
	int tamanoFuentes;
	String idUser;
	String noticiaD1, noticiaD2, noticiaD3, noticiaD4;
	String noticiaE1, noticiaE2, noticiaE3, noticiaE4;
	String noticiaI1, noticiaI2, noticiaI3, noticiaI4;
	String noticiaN1, noticiaN2, noticiaN3, noticiaN4;
	String categoriaD1, categoriaD2, categoriaD3, categoriaD4;
	String categoriaE1, categoriaE2, categoriaE3, categoriaE4;
	String categoriaI1, categoriaI2, categoriaI3, categoriaI4;
	String categoriaN1, categoriaN2, categoriaN3, categoriaN4;
	String fuenteD1, fuenteD2, fuenteD3, fuenteD4;
	String fuenteE1, fuenteE2, fuenteE3, fuenteE4;
	String fuenteI1, fuenteI2, fuenteI3, fuenteI4;
	String fuenteN1, fuenteN2, fuenteN3, fuenteN4;
	String tablaCategorias[] = {"Deportes", "Econom�a", "Internacional", "Nacional"}; //Tabla que contiene todas las categor�as.
	String tablaFuentes[] = {"Super Deporte", "El As", "Sport", "Mundo Deportivo", //Tabla que contiene todas las fuentes.
			"Expansi�n", "El Economista", "Cinco d�as", "Econom�a Digital",
			"BBC", "El Mundo Internacional", "20 Minutos Internacional", "ABC",
			"RTVE", "20 Minutos Nacional", "Ok diario", "El Mundo Nacional"};
	
	String noticia, categoria, fuente;
	LocalDate fechaActual = LocalDate.now();
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");//Objeto para convertir despu�s la fecha a string.
	File fichero = new File("log.txt");
	private JTextField textFieldMostrarPorFecha;

	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalNoticias window = new PrincipalNoticias();
					window.frame.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
					System.exit(0);
				}
			}
		});

	}

	/**
	 * Create the application.
	 */
	public PrincipalNoticias() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(PrincipalNoticias.class.getResource("/appNoticias/icono_altavoz.png")));
		frame.setResizable(false);
		frame.setBounds(100, 100, 788, 539);
		//Preguntar al cerrar pulsando en el aspa:
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				int option = JOptionPane.showConfirmDialog(
					frame, "�Desea salir?", "Confirmar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(option == JOptionPane.YES_OPTION)
				{
					System.exit(0);
				}
			}
		});
		frame.setLocationRelativeTo(null); //Oculta el borde del frame.
		frame.setUndecorated(true);
		
		
		/**
		 * Men� Acerca de.
		 */
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu unMenu = new JMenu("Men�");
		menuBar.add(unMenu);
		
		JMenuItem infoMenu = new JMenuItem("Acerca de");
		unMenu.add(infoMenu);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		infoMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,"Elena Esteban", "Autora", JOptionPane.OK_OPTION);
			}
		});
		
		/**
		 * Ventana de carga de la aplicaci�n.
		 */
		panelCarga.setBorder(null);
		
		frame.getContentPane().add(panelCarga, BorderLayout.NORTH);
		panelCarga.setLayout(null);
		
		/**
		 * Barra de progreso.
		 */
		JProgressBar progressBar = new JProgressBar(0, 100);
		progressBar.setBounds(0, 11, 782, 14);
		progressBar.setValue (0);
		progressBar.setStringPainted (true);
		panelCarga.add(progressBar);
		panelCarga.add( new JLabel( "Cargando informaci�n..." ) );
		
		/**
		 * Bot�n para iniciar la carga en el panelCarga y que la BARRA DE PROGRESO avance:
		 */
		JButton btnIniciarBarraP = new JButton("Iniciar carga");
		btnIniciarBarraP.setBounds(0, 25, 810, 23);
		panelCarga.add(btnIniciarBarraP);
		btnIniciarBarraP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			      
			    	  tiempo.start();	
			}
			
			      });
		//Inicializo timer para controlar cada cu�nto tiempo muestra el avance de la barra.
	    tiempo = new Timer(1000, new ActionListener() {
  	      public void actionPerformed (ActionEvent evt) {
	    	progressBar.setValue(progressBar.getValue()+20);
	    //Comprobar cada 20, si existe alguno de los fichero y si no, dar mensaje y salir del sistema.	
	    	if (progressBar.getValue()==20)
	    		comprobarFichero("usuarios.txt");
	    	if (progressBar.getValue()==40)
	    		comprobarFichero("configuracion.txt");
	    	if (progressBar.getValue()==60)
	    		comprobarFichero("logNoticias.txt");
	    	// Cuenta el n� de veces, cuando llega a 5, para el timer y da mensaje.
	    	contador++;
	    	if(contador == 5)
	    	{
	    		tiempo.stop();
	    		JOptionPane.showMessageDialog(null,"Carga finalizada", "", JOptionPane.OK_OPTION);
	    		// El borde se vuelva a hacer visible en la siguiente ventana.
				frame.dispose();
				frame.setUndecorated(false);
				frame.setVisible(true);
				panelCarga.setVisible(false);
				panelLogin.setVisible(true);
	    	}

  	      }
  	    
  	  });
		
		/**
		 * Ventana de inicio de sesi�n para los usuarios.
		 */
		frame.getContentPane().add(panelLogin, BorderLayout.NORTH);
		panelLogin.setLayout(null);
		JLabel lbl0 = new JLabel("LOGIN");
		lbl0.setBounds(328, 11, 70, 14);
		lbl0.setHorizontalAlignment(SwingConstants.CENTER);
		panelLogin.add(lbl0);
		
		//Creo el campo para introducir el usuario:
		TextField textFieldUsuario = new TextField();
		textFieldUsuario.setBounds(328, 159, 82, 22);		
		panelLogin.add(textFieldUsuario);
		//Creo el campo para introducir la contrase�a:
		passwordField = new JPasswordField();
		passwordField.setBounds(328, 221, 82, 20);
		panelLogin.add(passwordField);
		
		JButton btn1 = new JButton("Iniciar sesi\u00F3n");
		btn1.setBounds(475, 365, 113, 21);
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Guardo lo que introduzca el usuario, en stringUsuario:
				stringUsuario = textFieldUsuario.getText();
				//Guardo lo que introduzca el usuario, en stringPassword:
				String stringPassword = new String(passwordField.getPassword());
				//Si el usuario est� registrado en el fichero le doy acceso.
				validarUsuarioContrasena("usuarios.txt", stringUsuario , stringPassword);
			}
		});
		btn1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		btn1.setBackground(Color.WHITE);
		btn1.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelLogin.add(btn1);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(208, 170, 46, 14);
		panelLogin.add(lblUsuario);
		
		JLabel lblContrasena = new JLabel("Contrase\u00F1a");
		lblContrasena.setBounds(208, 227, 70, 14);
		panelLogin.add(lblContrasena);
		
		frame.getContentPane().add(panelOpciones, BorderLayout.SOUTH);
		panelOpciones.setLayout(new GridLayout(0, 1, 0, 0));
		JLabel lblOpciones = new JLabel("OPCIONES");
		lblOpciones.setHorizontalAlignment(SwingConstants.CENTER);
		lblOpciones.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panelOpciones.add(lblOpciones);
		
		/**
		 * Bot�n para ver las noticias actuales:
		 */
		JButton btnNoticiasActuales = new JButton("1- Ver noticias actuales.");
		btnNoticiasActuales.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNoticiasActuales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelNoticiasActuales.setVisible(true);
				panelOpciones.setVisible(false);
			}
		});
		panelOpciones.add(btnNoticiasActuales);
		
		
		/**
		 * Bot�n para configurar las noticias deseadas:
		 */
		btnConfigurar = new JButton("2- Configurar noticias.");
		btnConfigurar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnConfigurar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelConfigurar.setVisible(true);
				panelOpciones.setVisible(false);
			}
		});
		panelOpciones.add(btnConfigurar);
		
		
		/**
		 * Bot�n para ir a la venta de las noticias antiguas que est�n guardadas:
		 */
		JButton btnNoticiasAntiguas = new JButton("3- Ver noticias antiguas.");
		btnNoticiasAntiguas.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNoticiasAntiguas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelNoticiasAntiguas.setVisible(true);
				panelOpciones.setVisible(false);
				//Comprueba si el fichero logNoticias est� vac�o y si lo est�, da mensaje y vuelve a opciones.
				if(ficheroNoticiasAntiguas.ficheroVacio(ficheroNoticiasAntiguas.ficheroAntiguas))
				{
					JOptionPane.showMessageDialog(null,"Este usuario no tiene noticias guardadas", "", JOptionPane.OK_OPTION);
					panelNoticiasAntiguas.setVisible(false);
					panelOpciones.setVisible(true);
				}
			}
		});
		panelOpciones.add(btnNoticiasAntiguas);
		

		
		/**
		 * Bot�n salir de ventana opciones.
		 */
		JButton btnSalirOpciones = new JButton("SALIR");
		btnSalirOpciones.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panelOpciones.add(btnSalirOpciones);
		btnSalirOpciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int option = JOptionPane.showConfirmDialog(
						frame, "�Desea salir?", "Confirmar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.YES_OPTION)
					{
						System.exit(0);
					}
			}
		});
		
		
		panelNoticiasActuales.setFont(new Font("SansSerif", Font.PLAIN, 25));
		frame.getContentPane().add(panelNoticiasActuales, BorderLayout.WEST);
		
		JButton btnGuardaNoticiasActuales = new JButton("Guardar");
		btnGuardaNoticiasActuales.setBounds(642, 135, 89, 23);
		btnGuardaNoticiasActuales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardarNoticias(txtANDeportes1, noticiaD1, categoriaD1, fuenteD1);
				guardarNoticias(txtANDeportes2, noticiaD2, categoriaD2, fuenteD2);
				guardarNoticias(txtANDeportes3, noticiaD3, categoriaD3, fuenteD3);
				guardarNoticias(txtANDeportes4, noticiaD4, categoriaD4, fuenteD4);
				guardarNoticias(txtAEconomia1, noticiaE1, categoriaE1, fuenteE1);
				guardarNoticias(txtAEconomia2, noticiaE2, categoriaE2, fuenteE2);
				guardarNoticias(txtAEconomia3, noticiaE3, categoriaE3, fuenteE3);
				guardarNoticias(txtAEconomia4, noticiaE4, categoriaE4, fuenteE4);
				guardarNoticias(txtAInternacional1, noticiaI1, categoriaI1, fuenteI1);
				guardarNoticias(txtAInternacional2, noticiaI2, categoriaI2, fuenteI2);
				guardarNoticias(txtAInternacional3, noticiaI3, categoriaI3, fuenteI3);
				guardarNoticias(txtAInternacional4, noticiaI4, categoriaI4, fuenteI4);
				guardarNoticias(txtANacional1, noticiaN1, categoriaN1, fuenteN1);
				guardarNoticias(txtANacional2, noticiaN2, categoriaN2, fuenteN2);
				guardarNoticias(txtANacional3, noticiaN3, categoriaN3, fuenteN3);
				guardarNoticias(txtANacional4, noticiaN4, categoriaN4, fuenteN4);
				
				JOptionPane.showMessageDialog(null,"Se han guardado las noticias", "", JOptionPane.OK_OPTION);
				
				btnGuardaNoticiasActuales.setEnabled(false);//Deshabilita el bot�n para guardar las noticias actuales, para que s�lo se pueda guardar una vez por sesi�n.
			}
		});
		
		JButton btn21 = new JButton("Volver a opciones");
		btn21.setBounds(615, 87, 146, 23);
		btn21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelNoticiasActuales.setVisible(false);
				panelOpciones.setVisible(true);
			}
		});
		panelNoticiasActuales.setLayout(null);
		panelNoticiasActuales.add(btn21);
		
		JLabel lblNoticiasActuales = new JLabel("NOTICIAS ACTUALES");
		lblNoticiasActuales.setBounds(632, 18, 129, 14);
		panelNoticiasActuales.add(lblNoticiasActuales);
		panelNoticiasActuales.add(btnGuardaNoticiasActuales);
		
		JButton btnSalirNoticiasActuales = new JButton("SALIR");
		btnSalirNoticiasActuales.setBounds(655, 375, 77, 23);
		btnSalirNoticiasActuales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int option = JOptionPane.showConfirmDialog(
						frame, "�Desea salir?", "Confirmar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.YES_OPTION)
					{
						System.exit(0);
					}
			}
		});
		panelNoticiasActuales.add(btnSalirNoticiasActuales);
		/**
		 * Bot�n para mostrar las noticias.
		*/
		JButton btnObtener = new JButton("Obtener Noticias");
		btnObtener.setBounds(614, 43, 147, 23);
		btnObtener.addActionListener(new ActionListener() {
			/**
			 * Methods for the Button show the notices.
			*/
			public void actionPerformed(ActionEvent e) {
				
				leerCompleto("configuracion.txt"); //Lee fichero y extrae fuentes a array fuentes.
				mostrarDeportes(txtANDeportes1, txtANDeportes2, txtANDeportes3, txtANDeportes4);//recibe el textField donde escribe la palabra y la caja donde aparecer� la noticia.
				mostrarEconomia(txtAEconomia1, txtAEconomia2, txtAEconomia3, txtAEconomia4);
				mostrarInternacional(txtAInternacional1, txtAInternacional2, txtAInternacional3, txtAInternacional4);
				mostrarNacional(txtANacional1, txtANacional2, txtANacional3, txtANacional4);
			}
		});
		panelNoticiasActuales.add(btnObtener);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(85, 35, 233, 38);
		panelNoticiasActuales.add(scrollPane);
		// JTextField para mostrar las noticias:
		txtANDeportes1 = new JTextArea();
		scrollPane.setViewportView(txtANDeportes1);
		txtANDeportes1.setEditable(false);
		txtANDeportes1.setBackground(Color.WHITE);
		txtANDeportes1.setText("Deportes 1");
		txtANDeportes1.setColumns(20);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(85, 135, 234, 38);
		panelNoticiasActuales.add(scrollPane_2);
		
		txtANDeportes3 = new JTextArea();
		scrollPane_2.setViewportView(txtANDeportes3);
		txtANDeportes3.setEditable(false);
		txtANDeportes3.setText("Deportes 3");
		txtANDeportes3.setColumns(20);
		txtANDeportes3.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_7 = new JScrollPane();
		scrollPane_7.setBounds(353, 36, 233, 37);
		panelNoticiasActuales.add(scrollPane_7);
		
		//Parte derecha:
		txtAInternacional1 = new JTextArea();
		scrollPane_7.setViewportView(txtAInternacional1);
		txtAInternacional1.setEditable(false);
		txtAInternacional1.setText("Internacional 1");
		txtAInternacional1.setColumns(20);
		txtAInternacional1.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_9 = new JScrollPane();
		scrollPane_9.setBounds(353, 88, 233, 37);
		panelNoticiasActuales.add(scrollPane_9);
		
		txtAInternacional2 = new JTextArea();
		scrollPane_9.setViewportView(txtAInternacional2);
		txtAInternacional2.setEditable(false);
		txtAInternacional2.setText("Internacional 2");
		txtAInternacional2.setColumns(20);
		txtAInternacional2.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_10 = new JScrollPane();
		scrollPane_10.setBounds(353, 136, 233, 37);
		panelNoticiasActuales.add(scrollPane_10);
		
		txtAInternacional3 = new JTextArea();
		scrollPane_10.setViewportView(txtAInternacional3);
		txtAInternacional3.setEditable(false);
		txtAInternacional3.setText("Internacional 3");
		txtAInternacional3.setColumns(20);
		txtAInternacional3.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_11 = new JScrollPane();
		scrollPane_11.setBounds(353, 184, 233, 36);
		panelNoticiasActuales.add(scrollPane_11);
		
		txtAInternacional4 = new JTextArea();
		scrollPane_11.setViewportView(txtAInternacional4);
		txtAInternacional4.setEditable(false);
		txtAInternacional4.setText("Internacional 4");
		txtAInternacional4.setColumns(20);
		txtAInternacional4.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_12 = new JScrollPane();
		scrollPane_12.setBounds(353, 234, 233, 38);
		panelNoticiasActuales.add(scrollPane_12);
		
		txtANacional1 = new JTextArea();
		scrollPane_12.setViewportView(txtANacional1);
		txtANacional1.setEditable(false);
		txtANacional1.setText("Nacional 1");
		txtANacional1.setColumns(20);
		txtANacional1.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_13 = new JScrollPane();
		scrollPane_13.setBounds(353, 286, 233, 37);
		panelNoticiasActuales.add(scrollPane_13);
		
		txtANacional2 = new JTextArea();
		scrollPane_13.setViewportView(txtANacional2);
		txtANacional2.setEditable(false);
		txtANacional2.setText("Nacional 2");
		txtANacional2.setColumns(20);
		txtANacional2.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_14 = new JScrollPane();
		scrollPane_14.setBounds(353, 345, 233, 36);
		panelNoticiasActuales.add(scrollPane_14);
		
		txtANacional3 = new JTextArea();
		scrollPane_14.setViewportView(txtANacional3);
		txtANacional3.setEditable(false);
		txtANacional3.setText("Nacional 3");
		txtANacional3.setColumns(20);
		txtANacional3.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_15 = new JScrollPane();
		scrollPane_15.setBounds(353, 399, 233, 38);
		panelNoticiasActuales.add(scrollPane_15);
		
		txtANacional4 = new JTextArea();
		scrollPane_15.setViewportView(txtANacional4);
		txtANacional4.setEditable(false);
		txtANacional4.setText("Nacional 4");
		txtANacional4.setColumns(20);
		txtANacional4.setBackground(Color.WHITE);
		
		JLabel lblDeportes1 = new JLabel("Deportes: Super Deporte");
		lblDeportes1.setBounds(85, 21, 146, 14);
		panelNoticiasActuales.add(lblDeportes1);
		
		JLabel lblDeportes2 = new JLabel("Deportes: El As");
		lblDeportes2.setBounds(85, 65, 146, 29);
		panelNoticiasActuales.add(lblDeportes2);
		
		JLabel lblDeportes3 = new JLabel("Deportes: Sport");
		lblDeportes3.setBounds(85, 123, 146, 14);
		panelNoticiasActuales.add(lblDeportes3);
		
		JLabel lblDeportes4 = new JLabel("Deportes: Mundo Deportivo");
		lblDeportes4.setBounds(85, 170, 179, 14);
		lblDeportes4.setHorizontalAlignment(SwingConstants.LEFT);
		panelNoticiasActuales.add(lblDeportes4);
		
		JLabel lblEconomia1 = new JLabel("Econom\u00EDa: Expansi\u00F3n");
		lblEconomia1.setBounds(85, 220, 146, 14);
		panelNoticiasActuales.add(lblEconomia1);
		
		JLabel lblEconomia2 = new JLabel("Econom\u00EDa: El Economista");
		lblEconomia2.setBounds(85, 272, 146, 14);
		panelNoticiasActuales.add(lblEconomia2);
		
		JLabel lblEconomia3 = new JLabel("Econom\u00EDa: Cinco d\u00EDas");
		lblEconomia3.setBounds(85, 326, 146, 18);
		panelNoticiasActuales.add(lblEconomia3);
		
		JLabel lblEconomia4 = new JLabel("Econom\u00EDa: Econom\u00EDa Digital");
		lblEconomia4.setBounds(85, 384, 201, 14);
		panelNoticiasActuales.add(lblEconomia4);
		
		JLabel lblInternacional1 = new JLabel("Internacional: BBC");
		lblInternacional1.setBounds(353, 21, 146, 14);
		panelNoticiasActuales.add(lblInternacional1);
		
		JLabel lblInternacional2 = new JLabel("Internacional: El Mundo");
		lblInternacional2.setBounds(353, 72, 146, 14);
		panelNoticiasActuales.add(lblInternacional2);
		
		JLabel lblInternacional3 = new JLabel("Internacional: 20 Minutos");
		lblInternacional3.setBounds(353, 123, 146, 14);
		lblInternacional3.setHorizontalAlignment(SwingConstants.LEFT);
		panelNoticiasActuales.add(lblInternacional3);
		
		JLabel lblInternacional4 = new JLabel("Internacional: ABC");
		lblInternacional4.setBounds(353, 170, 146, 14);
		lblInternacional4.setHorizontalAlignment(SwingConstants.LEFT);
		panelNoticiasActuales.add(lblInternacional4);
		
		JLabel lblNaciona1 = new JLabel("Nacional: RTVE");
		lblNaciona1.setBounds(353, 220, 146, 14);
		lblNaciona1.setHorizontalAlignment(SwingConstants.LEFT);
		panelNoticiasActuales.add(lblNaciona1);
		
		JLabel lblNacional2 = new JLabel("Nacional: 20 Minutos");
		lblNacional2.setBounds(353, 272, 146, 14);
		lblNacional2.setHorizontalAlignment(SwingConstants.LEFT);
		panelNoticiasActuales.add(lblNacional2);
		
		JLabel lblNacional3 = new JLabel("Nacional: Ok diario");
		lblNacional3.setBounds(353, 328, 146, 14);
		lblNacional3.setHorizontalAlignment(SwingConstants.LEFT);
		panelNoticiasActuales.add(lblNacional3);
		
		JLabel lblNacional4 = new JLabel("Nacional: El Mundo");
		lblNacional4.setBounds(353, 384, 146, 14);
		lblNacional4.setHorizontalAlignment(SwingConstants.LEFT);
		panelNoticiasActuales.add(lblNacional4);
		
		
		txtANDeportes2 = new JTextArea();
		
		txtANDeportes2.setCaretColor(Color.BLACK);
		txtANDeportes2.setText("Deportes 2");
		txtANDeportes2.setEditable(false);
		txtANDeportes2.setColumns(20);
		txtANDeportes2.setBackground(Color.WHITE);
		JScrollPane scrollPane_1 = new JScrollPane(txtANDeportes2);
		scrollPane_1.setBounds(85, 87, 233, 38);
		scrollPane_1.setViewportView(txtANDeportes2);
		panelNoticiasActuales.add(scrollPane_1);
		
		JScrollPane scrollPane_8 = new JScrollPane();
		scrollPane_8.setBounds(85, 184, 231, 36);
		panelNoticiasActuales.add(scrollPane_8);
		
		txtANDeportes4 = new JTextArea();
		scrollPane_8.setViewportView(txtANDeportes4);
		txtANDeportes4.setEditable(false);
		txtANDeportes4.setText("Deportes 4");
		txtANDeportes4.setColumns(20);
		txtANDeportes4.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(85, 286, 231, 37);
		panelNoticiasActuales.add(scrollPane_4);
		
		txtAEconomia2 = new JTextArea();
		scrollPane_4.setViewportView(txtAEconomia2);
		txtAEconomia2.setEditable(false);
		txtAEconomia2.setText("Econom\u00EDa 2");
		txtAEconomia2.setColumns(20);
		txtAEconomia2.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(85, 345, 231, 36);
		panelNoticiasActuales.add(scrollPane_5);
		
		txtAEconomia3 = new JTextArea();
		scrollPane_5.setViewportView(txtAEconomia3);
		txtAEconomia3.setEditable(false);
		txtAEconomia3.setText("Econom\u00EDa 3");
		txtAEconomia3.setColumns(20);
		txtAEconomia3.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_6 = new JScrollPane();
		scrollPane_6.setBounds(86, 399, 232, 38);
		panelNoticiasActuales.add(scrollPane_6);
		
		txtAEconomia4 = new JTextArea();
		scrollPane_6.setViewportView(txtAEconomia4);
		txtAEconomia4.setEditable(false);
		txtAEconomia4.setText("Econom\u00EDa 4");
		txtAEconomia4.setColumns(20);
		txtAEconomia4.setBackground(Color.WHITE);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(85, 234, 229, 38);
		panelNoticiasActuales.add(scrollPane_3);
		
		txtAEconomia1 = new JTextArea();
		scrollPane_3.setViewportView(txtAEconomia1);
		txtAEconomia1.setEditable(false);
		txtAEconomia1.setText("Econom\u00EDa 1");
		txtAEconomia1.setColumns(20);
		txtAEconomia1.setBackground(Color.WHITE);
		
			
		/*panelConfigurar:
		 * 
		 */
		frame.getContentPane().add(panelConfigurar, BorderLayout.EAST);
		GridBagLayout gbl_panelConfigurar = new GridBagLayout();
		gbl_panelConfigurar.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		gbl_panelConfigurar.columnWidths = new int[] {40, 40, 40, 67, 40, 40, 0, 68, 40, 40, 0, 40, 40, 40, 82, 71, 40, 40, 40, 40, 40, 40};
		gbl_panelConfigurar.rowHeights = new int[] {30, 30, 30, 30, 30, 0, 0, 0, 30, 30, 0, 30, 30, 30, 30, 0, 30, 30, 30, 30, 0, 30, 30, 30, 30};
		panelConfigurar.setLayout(gbl_panelConfigurar);
		
		JLabel lblConfigurar = new JLabel("CONFIGURACI\u00D3N DE NOTICIAS");
		GridBagConstraints gbc_lblConfigurar = new GridBagConstraints();
		gbc_lblConfigurar.insets = new Insets(0, 0, 5, 5);
		gbc_lblConfigurar.gridheight = 2;
		gbc_lblConfigurar.gridwidth = 6;
		gbc_lblConfigurar.gridx = 8;
		gbc_lblConfigurar.gridy = 4;
		panelConfigurar.add(lblConfigurar, gbc_lblConfigurar);
		
		JCheckBox chckbxDeportes = new JCheckBox("Deportes");
		
		GridBagConstraints gbc_chckbxDeportes = new GridBagConstraints();
		gbc_chckbxDeportes.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxDeportes.gridx = 7;
		gbc_chckbxDeportes.gridy = 6;
		panelConfigurar.add(chckbxDeportes, gbc_chckbxDeportes);
		
		JCheckBox chckbxDeportes_1 = new JCheckBox("Super Deporte");
		GridBagConstraints gbc_chckbxDeportes_1 = new GridBagConstraints();
		gbc_chckbxDeportes_1.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxDeportes_1.gridx = 8;
		gbc_chckbxDeportes_1.gridy = 6;
		panelConfigurar.add(chckbxDeportes_1, gbc_chckbxDeportes_1);
		
		JCheckBox chckbxEconoma = new JCheckBox("Econom\u00EDa");
		GridBagConstraints gbc_chckbxEconoma = new GridBagConstraints();
		gbc_chckbxEconoma.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxEconoma.gridx = 13;
		gbc_chckbxEconoma.gridy = 6;
		panelConfigurar.add(chckbxEconoma, gbc_chckbxEconoma);
		
		JCheckBox chckbxEconoma_1 = new JCheckBox("Expansi\u00F3n");
		GridBagConstraints gbc_chckbxEconoma_1 = new GridBagConstraints();
		gbc_chckbxEconoma_1.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxEconoma_1.gridx = 14;
		gbc_chckbxEconoma_1.gridy = 6;
		panelConfigurar.add(chckbxEconoma_1, gbc_chckbxEconoma_1);
		
		JCheckBox chckbxDeportes_2 = new JCheckBox("El As");
		GridBagConstraints gbc_chckbxDeportes_2 = new GridBagConstraints();
		gbc_chckbxDeportes_2.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxDeportes_2.gridx = 8;
		gbc_chckbxDeportes_2.gridy = 7;
		panelConfigurar.add(chckbxDeportes_2, gbc_chckbxDeportes_2);
		
		JCheckBox chckbxEconoma_2 = new JCheckBox("El Economista");
		GridBagConstraints gbc_chckbxEconoma_2 = new GridBagConstraints();
		gbc_chckbxEconoma_2.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxEconoma_2.gridx = 14;
		gbc_chckbxEconoma_2.gridy = 7;
		panelConfigurar.add(chckbxEconoma_2, gbc_chckbxEconoma_2);
		
		JCheckBox chckbxDeportes_3 = new JCheckBox("Sport");
		GridBagConstraints gbc_chckbxDeportes_3 = new GridBagConstraints();
		gbc_chckbxDeportes_3.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxDeportes_3.gridx = 8;
		gbc_chckbxDeportes_3.gridy = 8;
		panelConfigurar.add(chckbxDeportes_3, gbc_chckbxDeportes_3);
		
		JCheckBox chckbxEconoma_3 = new JCheckBox("Cinco d\u00EDas");
		GridBagConstraints gbc_chckbxEconoma_3 = new GridBagConstraints();
		gbc_chckbxEconoma_3.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxEconoma_3.gridx = 14;
		gbc_chckbxEconoma_3.gridy = 8;
		panelConfigurar.add(chckbxEconoma_3, gbc_chckbxEconoma_3);
		
		JCheckBox chckbxDeportes_4 = new JCheckBox("Mundo Deportivo");
		GridBagConstraints gbc_chckbxDeportes_4 = new GridBagConstraints();
		gbc_chckbxDeportes_4.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxDeportes_4.gridx = 8;
		gbc_chckbxDeportes_4.gridy = 9;
		panelConfigurar.add(chckbxDeportes_4, gbc_chckbxDeportes_4);
		
		JCheckBox chckbxEconoma_4 = new JCheckBox("Econom\u00EDa Digital");
		GridBagConstraints gbc_chckbxEconoma_4 = new GridBagConstraints();
		gbc_chckbxEconoma_4.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxEconoma_4.gridx = 14;
		gbc_chckbxEconoma_4.gridy = 9;
		panelConfigurar.add(chckbxEconoma_4, gbc_chckbxEconoma_4);
		
		JCheckBox chckbxInternacional = new JCheckBox("Internacional");
		GridBagConstraints gbc_chckbxInternacional = new GridBagConstraints();
		gbc_chckbxInternacional.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxInternacional.gridx = 7;
		gbc_chckbxInternacional.gridy = 11;
		panelConfigurar.add(chckbxInternacional, gbc_chckbxInternacional);
		
		JCheckBox chckbxInternacional_1 = new JCheckBox("BBC");
		GridBagConstraints gbc_chckbxInternacional_1 = new GridBagConstraints();
		gbc_chckbxInternacional_1.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxInternacional_1.gridx = 8;
		gbc_chckbxInternacional_1.gridy = 11;
		panelConfigurar.add(chckbxInternacional_1, gbc_chckbxInternacional_1);
		
		JCheckBox chckbxNacional = new JCheckBox("Nacional");
		GridBagConstraints gbc_chckbxNacional = new GridBagConstraints();
		gbc_chckbxNacional.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNacional.gridx = 13;
		gbc_chckbxNacional.gridy = 11;
		panelConfigurar.add(chckbxNacional, gbc_chckbxNacional);
		
		JCheckBox chckbxNacional_1 = new JCheckBox("RTVE");
		GridBagConstraints gbc_chckbxNacional_1 = new GridBagConstraints();
		gbc_chckbxNacional_1.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNacional_1.gridx = 14;
		gbc_chckbxNacional_1.gridy = 11;
		panelConfigurar.add(chckbxNacional_1, gbc_chckbxNacional_1);
		
		JCheckBox chckbxInternacional_2 = new JCheckBox("El Mundo Internacional");
		GridBagConstraints gbc_chckbxInternacional_2 = new GridBagConstraints();
		gbc_chckbxInternacional_2.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxInternacional_2.gridx = 8;
		gbc_chckbxInternacional_2.gridy = 12;
		panelConfigurar.add(chckbxInternacional_2, gbc_chckbxInternacional_2);
		
		JCheckBox chckbxNacional_2 = new JCheckBox("20 Minutos Nacional");
		GridBagConstraints gbc_chckbxNacional_2 = new GridBagConstraints();
		gbc_chckbxNacional_2.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNacional_2.gridx = 14;
		gbc_chckbxNacional_2.gridy = 12;
		panelConfigurar.add(chckbxNacional_2, gbc_chckbxNacional_2);
		
		JCheckBox chckbxInternacional_3 = new JCheckBox("20 Minutos Internacional");
		GridBagConstraints gbc_chckbxInternacional_3 = new GridBagConstraints();
		gbc_chckbxInternacional_3.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxInternacional_3.gridx = 8;
		gbc_chckbxInternacional_3.gridy = 13;
		panelConfigurar.add(chckbxInternacional_3, gbc_chckbxInternacional_3);
		
		JCheckBox chckbxNacional_3 = new JCheckBox("Ok diario");
		GridBagConstraints gbc_chckbxNacional_3 = new GridBagConstraints();
		gbc_chckbxNacional_3.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNacional_3.gridx = 14;
		gbc_chckbxNacional_3.gridy = 13;
		panelConfigurar.add(chckbxNacional_3, gbc_chckbxNacional_3);
		
		JCheckBox chckbxInternacional_4 = new JCheckBox("ABC");
		GridBagConstraints gbc_chckbxInternacional_4 = new GridBagConstraints();
		gbc_chckbxInternacional_4.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxInternacional_4.gridx = 8;
		gbc_chckbxInternacional_4.gridy = 14;
		panelConfigurar.add(chckbxInternacional_4, gbc_chckbxInternacional_4);
		
		JCheckBox chckbxNacional_4 = new JCheckBox("El Mundo Nacional");
		GridBagConstraints gbc_chckbxNacional_4 = new GridBagConstraints();
		gbc_chckbxNacional_4.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNacional_4.gridx = 14;
		gbc_chckbxNacional_4.gridy = 14;
		panelConfigurar.add(chckbxNacional_4, gbc_chckbxNacional_4);
		
		
		
		
		//Bot�n para volver a Opciones desde Configuraci�n, sin guardar.
		JButton btnVolverOpcionesSinGuardar = new JButton("Volver sin guardar");
		btnVolverOpcionesSinGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelConfigurar.setVisible(false);
				panelOpciones.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnVolverOpcionesSinGuardar = new GridBagConstraints();
		gbc_btnVolverOpcionesSinGuardar.gridwidth = 2;
		gbc_btnVolverOpcionesSinGuardar.insets = new Insets(0, 0, 5, 5);
		gbc_btnVolverOpcionesSinGuardar.gridx = 8;
		gbc_btnVolverOpcionesSinGuardar.gridy = 20;
		panelConfigurar.add(btnVolverOpcionesSinGuardar, gbc_btnVolverOpcionesSinGuardar);
		
		// Bot�n que guarda los cambios de la configuraci�n en el fichero y vuelve a Opciones.
		JButton btnGuardarVolver = new JButton("Guardar y Volver");
		btnGuardarVolver.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if((chckbxDeportes.isSelected()==false) && (chckbxEconoma.isSelected())==false && (chckbxInternacional.isSelected())==false && (chckbxNacional.isSelected())==false)
				{
					JOptionPane.showMessageDialog(null,"Tiene que seleccionar al menos una categor�a", "", JOptionPane.OK_OPTION);
				}else {
						if (chckbxDeportes.isSelected()) {
							Configuracion configuracion1 = new Configuracion();
							configuracion1.idUser = idUser;
				        	configuracion1.categoria = "Deportes";
				        	if((chckbxDeportes_1.isSelected()==false) && (chckbxDeportes_2.isSelected())==false && (chckbxDeportes_3.isSelected())==false && (chckbxDeportes_4.isSelected())==false)
							{
								JOptionPane.showMessageDialog(null,"Tiene que seleccionar al menos una fuente", "", JOptionPane.OK_OPTION);
							}else {
				        	
				        			if(chckbxDeportes_1.isSelected()) {
				    					configuracion1.tablaFuentes[configuracion1.nFuentes++] = "Super Deporte";
				    				}
				        			if(chckbxDeportes_2.isSelected()) {
				        				configuracion1.tablaFuentes[configuracion1.nFuentes++] = "El As";
				    				}
				        			if(chckbxDeportes_3.isSelected()) {
				        				configuracion1.tablaFuentes[configuracion1.nFuentes++] = "Sport";
				    				}
				        			if(chckbxDeportes_4.isSelected()) {
				        				configuracion1.tablaFuentes[configuracion1.nFuentes++] = "Mundo Deportivo";
				    				}
				        			tablaConfiguraciones.tabla[tablaConfiguraciones.tamano++] = configuracion1;
						}
						}
				//Si marca la categor�a Econom�a:
								if (chckbxEconoma.isSelected()) {
									Configuracion configuracion2 = new Configuracion();
						        	configuracion2.categoria = "Econom�a";
						        	if((chckbxEconoma_1.isSelected()==false) && (chckbxEconoma_2.isSelected())==false && (chckbxEconoma_3.isSelected())==false && (chckbxEconoma_4.isSelected())==false)
									{
										JOptionPane.showMessageDialog(null,"Tiene que seleccionar al menos una fuente", "", JOptionPane.OK_OPTION);
									}else {
						        	
						        			if(chckbxEconoma_1.isSelected()) {
						    					configuracion2.tablaFuentes[configuracion2.nFuentes++] = "Expansi�n";
						    				}
						        			if(chckbxEconoma_2.isSelected()) {
						    					configuracion2.tablaFuentes[configuracion2.nFuentes++] = "El Economista";
						    				}
						        			if(chckbxEconoma_3.isSelected()) {
						    					configuracion2.tablaFuentes[configuracion2.nFuentes++] = "Cinco d�as";
						    				}
						        			if(chckbxEconoma_4.isSelected()) {
						    					configuracion2.tablaFuentes[configuracion2.nFuentes++] = "Econom�a Digital";
						    				}
	
						        			tablaConfiguraciones.tabla[tablaConfiguraciones.tamano++] = configuracion2;
									}
								}
						
						//Si marca la categor�a Internacional:
								if (chckbxInternacional.isSelected()) {
									Configuracion configuracion3 = new Configuracion();
						        	configuracion3.categoria = "Internacional";
						        	if((chckbxInternacional_1.isSelected()==false) && (chckbxInternacional_2.isSelected())==false && (chckbxInternacional_3.isSelected())==false && (chckbxInternacional_4.isSelected())==false)
									{
										JOptionPane.showMessageDialog(null,"Tiene que seleccionar al menos una fuente", "", JOptionPane.OK_OPTION);
									}else {
						        	
						        			if(chckbxInternacional_1.isSelected()) {
						    					configuracion3.tablaFuentes[configuracion3.nFuentes++] = "BBC";
						    				}
						        			if(chckbxInternacional_2.isSelected()) {
						    					configuracion3.tablaFuentes[configuracion3.nFuentes++] = "El Mundo Internacional";
						    				}
						        			if(chckbxInternacional_3.isSelected()) {
						    					configuracion3.tablaFuentes[configuracion3.nFuentes++] = "20 Minutos Internacional";
						    				}
						        			if(chckbxInternacional_4.isSelected()) {
						    					configuracion3.tablaFuentes[configuracion3.nFuentes++] = "ABC";
						    				}
						    
						        			tablaConfiguraciones.tabla[tablaConfiguraciones.tamano++] = configuracion3;
									}
								}
						
						//Si marca la categor�a Nacional:
								if (chckbxNacional.isSelected()) {
									Configuracion configuracion4 = new Configuracion();
						        	configuracion4.categoria = "Nacional";
						        	if((chckbxNacional_1.isSelected()==false) && (chckbxNacional_2.isSelected())==false && (chckbxNacional_3.isSelected())==false && (chckbxNacional_4.isSelected())==false)
									{
										JOptionPane.showMessageDialog(null,"Tiene que seleccionar al menos una fuente", "", JOptionPane.OK_OPTION);
									}else {
						        	
						        			if(chckbxNacional_1.isSelected()) {
						    					configuracion4.tablaFuentes[configuracion4.nFuentes++] = "RTVE";
						    				}
						        			if(chckbxNacional_2.isSelected()) {
						    					configuracion4.tablaFuentes[configuracion4.nFuentes++] = "20 Minutos Nacional";
						    				}
						        			if(chckbxNacional_3.isSelected()) {
						    					configuracion4.tablaFuentes[configuracion4.nFuentes++] = "Ok diario";
						    				}
						        			if(chckbxNacional_4.isSelected()) {
						    					configuracion4.tablaFuentes[configuracion4.nFuentes++] = "El Mundo Nacional";
						    				}
						        		
						        			tablaConfiguraciones.tabla[tablaConfiguraciones.tamano++] = configuracion4;
									}
								}
				guardarConfiguracion(tablaConfiguraciones, idUser);
				btnGuardarVolver.setEnabled(false); //Deshabilito el bot�n de guardar la configuraci�n , para que s�lo se pueda hacer una vez en la misma sesi�n.
				panelConfigurar.setVisible(false);
				panelOpciones.setVisible(true);
				}
			}

		});

		GridBagConstraints gbc_btnGuardarVolver = new GridBagConstraints();
		gbc_btnGuardarVolver.gridwidth = 4;
		gbc_btnGuardarVolver.insets = new Insets(0, 0, 5, 5);
		gbc_btnGuardarVolver.gridx = 12;
		gbc_btnGuardarVolver.gridy = 20;
		panelConfigurar.add(btnGuardarVolver, gbc_btnGuardarVolver);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int option = JOptionPane.showConfirmDialog(
						frame, "�Desea salir?", "Confirmar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.YES_OPTION)
					{
						frame.dispose();
						System.exit(0);
					}
			}
		});
		GridBagConstraints gbc_btnSalir = new GridBagConstraints();
		gbc_btnSalir.gridwidth = 3;
		gbc_btnSalir.insets = new Insets(0, 0, 5, 5);
		gbc_btnSalir.gridx = 16;
		gbc_btnSalir.gridy = 20;
		panelConfigurar.add(btnSalir, gbc_btnSalir);
		
		frame.getContentPane().add(panelNoticiasAntiguas, BorderLayout.EAST);
		GridBagLayout gbl_panelNoticiasAntiguas = new GridBagLayout();
		gbl_panelNoticiasAntiguas.columnWidths = new int[] {70, 70, 92, 0, 70, 0, 0, 70};
		gbl_panelNoticiasAntiguas.rowHeights = new int[] {16, 0, 0, 0, 16, 16, 16};
		gbl_panelNoticiasAntiguas.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE, 0.0, 1.0};
		gbl_panelNoticiasAntiguas.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0};
		panelNoticiasAntiguas.setLayout(gbl_panelNoticiasAntiguas);
		
		JLabel lblNoticiasAntiguas = new JLabel("NOTICIAS ANTIGUAS");
		GridBagConstraints gbc_lblNoticiasAntiguas = new GridBagConstraints();
		gbc_lblNoticiasAntiguas.gridwidth = 2;
		gbc_lblNoticiasAntiguas.insets = new Insets(0, 0, 5, 5);
		gbc_lblNoticiasAntiguas.gridx = 2;
		gbc_lblNoticiasAntiguas.gridy = 1;
		panelNoticiasAntiguas.add(lblNoticiasAntiguas, gbc_lblNoticiasAntiguas);
		
		JButton btnSalirNoticiasAntiguas = new JButton("SALIR");
		btnSalirNoticiasAntiguas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int option = JOptionPane.showConfirmDialog(
						frame, "�Desea salir?", "Confirmar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(option == JOptionPane.YES_OPTION)
					{
						frame.dispose();
						System.exit(0);
					}
			}
		});
		
		JButton btnVolverOpcionesDesdeAntiguas = new JButton("Volver a Opciones");
		btnVolverOpcionesDesdeAntiguas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelNoticiasAntiguas.setVisible(false);
				panelOpciones.setVisible(true);
			}
		});
		
		JButton btnMostrarAntiguas = new JButton("Mostrar");
		btnMostrarAntiguas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostrarAntiguas(ficheroNoticiasAntiguas.ficheroAntiguas, textAreaMuestraAntiguas);
			}
		});
		
		//Bot�n para mostrar noticias antiguas seg�n la fecha indicada por el usuario.
		JButton btnMostrarPorFecha = new JButton("Mostrar por fecha aaaa-mm-dd");
		GridBagConstraints gbc_btnMostrarPorFecha = new GridBagConstraints();
		gbc_btnMostrarPorFecha.insets = new Insets(0, 0, 5, 5);
		gbc_btnMostrarPorFecha.gridx = 6;
		gbc_btnMostrarPorFecha.gridy = 2;
		panelNoticiasAntiguas.add(btnMostrarPorFecha, gbc_btnMostrarPorFecha);
		btnMostrarPorFecha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stringFecha = textFieldMostrarPorFecha.getText();
				mostrarPorFecha(ficheroNoticiasAntiguas.ficheroAntiguas, stringFecha, textAreaMuestraAntiguas);
			}
		});
		
		GridBagConstraints gbc_btnMostrarAntiguas = new GridBagConstraints();
		gbc_btnMostrarAntiguas.gridwidth = 2;
		gbc_btnMostrarAntiguas.insets = new Insets(0, 0, 5, 5);
		gbc_btnMostrarAntiguas.gridx = 2;
		gbc_btnMostrarAntiguas.gridy = 3;
		panelNoticiasAntiguas.add(btnMostrarAntiguas, gbc_btnMostrarAntiguas);
		
		
		//TextArea para mostrar noticias antiguas.
		GridBagConstraints gbc_textAreaMuestraAntiguas = new GridBagConstraints();
		gbc_textAreaMuestraAntiguas.gridwidth = 4;
		gbc_textAreaMuestraAntiguas.insets = new Insets(0, 0, 5, 5);
		gbc_textAreaMuestraAntiguas.fill = GridBagConstraints.BOTH;
		gbc_textAreaMuestraAntiguas.gridx = 1;
		gbc_textAreaMuestraAntiguas.gridy = 3;
		
		textFieldMostrarPorFecha = new JTextField();
		GridBagConstraints gbc_textFieldMostrarPorFecha = new GridBagConstraints();
		gbc_textFieldMostrarPorFecha.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldMostrarPorFecha.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldMostrarPorFecha.gridx = 6;
		gbc_textFieldMostrarPorFecha.gridy = 3;
		panelNoticiasAntiguas.add(textFieldMostrarPorFecha, gbc_textFieldMostrarPorFecha);
		textFieldMostrarPorFecha.setColumns(10);
		textAreaMuestraAntiguas.setEditable(false);
		//panelNoticiasAntiguas.add(textAreaMuestraAntiguas, gbc_textAreaMuestraAntiguas);
		
		JScrollPane scrollNoticiasAntiguas = new JScrollPane();//A�ado scroll
		GridBagConstraints gbc_scrollNoticiasAntiguas = new GridBagConstraints();
		gbc_scrollNoticiasAntiguas.gridwidth = 5;
		gbc_scrollNoticiasAntiguas.insets = new Insets(0, 0, 5, 5);
		gbc_scrollNoticiasAntiguas.fill = GridBagConstraints.BOTH;
		gbc_scrollNoticiasAntiguas.gridx = 1;
		gbc_scrollNoticiasAntiguas.gridy = 4; 
		panelNoticiasAntiguas.add(scrollNoticiasAntiguas, gbc_scrollNoticiasAntiguas);
		scrollNoticiasAntiguas.setViewportView(textAreaMuestraAntiguas);
		
		//panelNoticiasAntiguas.add(scrollNoticiasAntiguas, gbc_scrollNoticiasAntiguas);
		GridBagConstraints gbc_btnVolverOpcionesDesdeAntiguas = new GridBagConstraints();
		gbc_btnVolverOpcionesDesdeAntiguas.insets = new Insets(0, 0, 5, 5);
		gbc_btnVolverOpcionesDesdeAntiguas.gridx = 1;
		gbc_btnVolverOpcionesDesdeAntiguas.gridy = 5;
		panelNoticiasAntiguas.add(btnVolverOpcionesDesdeAntiguas, gbc_btnVolverOpcionesDesdeAntiguas);
		GridBagConstraints gbc_btnSalirNoticiasAntiguas = new GridBagConstraints();
		gbc_btnSalirNoticiasAntiguas.insets = new Insets(0, 0, 5, 5);
		gbc_btnSalirNoticiasAntiguas.gridx = 4;
		gbc_btnSalirNoticiasAntiguas.gridy = 5;
		panelNoticiasAntiguas.add(btnSalirNoticiasAntiguas, gbc_btnSalirNoticiasAntiguas);
		

	}
	
	/**
	 * M�todo para mostrar las noticias de deportes.
	 * 
	*/
	
	
		protected void mostrarDeportes (JTextArea txtANDeportes1,JTextArea txtANDeportes2, JTextArea txtANDeportes3, JTextArea txtANDeportes4) { 
			//Noticia de Deportes1: Super Deporte.
			Document docDeportes1=null;
			String webDeportes1 = "https://www.superdeporte.es/";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("Super Deporte"))
				{
					try {
						docDeportes1 = Jsoup.connect(webDeportes1).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en la variable tipo Document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}			
					Elements noticiaDeportes1 = docDeportes1.select("h1.normal"); // En noticia mete varios elementos filtrados con select. Elements es un array.
					txtANDeportes1.setText(noticiaDeportes1.get(0).text());//Fija la noticia en el textField. Como noticiaDeportes1 es un array, con get(0), le digo que coja s�lo el primer elemento del array.
					noticiaD1 = txtANDeportes1.getText();
					categoriaD1 = tablaCategorias[0]; //Guarda la categor�a en variable.
					fuenteD1 = tablaFuentes[0];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtANDeportes1.setText("");
				}
				
			}
			
			//Noticia de Deportes2: El As.
			Document docDeportes2=null;
			String webDeportes2 = "https://as.com/";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("El As"))
				{
					try {
						docDeportes2 = Jsoup.connect(webDeportes2).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
				
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaDeportes2 = docDeportes2.select("h2.title");//Trae el titular
					txtANDeportes2.setText(noticiaDeportes2.get(0).text());//Fija la noticia en el textField
					noticiaD2 = txtANDeportes2.getText();
					categoriaD2 = tablaCategorias[0]; //Guarda la categor�a en variable.
					fuenteD2 = tablaFuentes[1];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtANDeportes2.setText("");
				}
				
			}
		
			//Noticia de Deportes3: Sport.
			Document docDeportes3=null;
			String webDeportes3 = "https://sport.es/es/";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("Sport"))
				{
					try {
						docDeportes3 = Jsoup.connect(webDeportes3).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					
					Elements noticiaDeportes3 = docDeportes3.select("h2.title"); // En noticia mete varios elementos filtrados con select(en este caso uno).
					txtANDeportes3.setText(noticiaDeportes3.get(0).text());//Fija la noticia en el textField
					noticiaD3 = txtANDeportes3.getText();
					categoriaD3 = tablaCategorias[0]; //Guarda la categor�a en variable.
					fuenteD3 = tablaFuentes[2];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtANDeportes3.setText("");
				}
			}
			//Noticia de Deportes4: Mundo Deportivo.
			Document docDeportes4=null;
			String webDeportes4 = "https://mundodeportivo.com/";
			
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("Mundo Deportivo"))
				{
					try {
						docDeportes4 = Jsoup.connect(webDeportes4).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en la variable tipo Document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaDeportes4 = docDeportes4.select(".last-hour-text"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo el selector de css.
					txtANDeportes4.setText(noticiaDeportes4.get(0).text());//Fija la noticia en el textField
					noticiaD4 = txtANDeportes4.getText();
					categoriaD4 = tablaCategorias[0]; //Guarda la categor�a en variable.
					fuenteD4 = tablaFuentes[3];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtANDeportes4.setText("");
				}
			}
			
	}
		
		/**
		 * M�todo para mostrar las noticias de econom�a.
		*/
		
		protected void mostrarEconomia (JTextArea txtAEconomia1,JTextArea txtAEconomia2, JTextArea txtAEconomia3, JTextArea txtAEconomia4) 
		{ 
			//Noticia de Economia1: Expansi�n.
			Document docEconomia1=null;
			String webEconomia1 = "https://www.expansion.com/";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("Expansi�n"))
				{
					try {
						docEconomia1 = Jsoup.connect(webEconomia1).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.				
						
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaEconomia1 = docEconomia1.select(".ue-c-cover-content__link"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo el selector de css.
					txtAEconomia1.setText(noticiaEconomia1.get(0).text());//Fija la noticia en el textField
					noticiaE1 = txtAEconomia1.getText();
					categoriaE1 = tablaCategorias[1]; //Guarda la categor�a en variable.
					fuenteE1 = tablaFuentes[4];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtAEconomia1.setText("");
				}
			}
			
			//Noticia de Economia2: El Economista.
			Document docEconomia2=null;
			String webEconomia2 = "https://www.eleconomista.es/";
			
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("El Economista"))
				{
					try {
						docEconomia2 = Jsoup.connect(webEconomia2).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}	
					Elements noticiaEconomia2 = docEconomia2.select ("h2.h1");
					txtAEconomia2.setText(noticiaEconomia2.get(0).text());//Fija la noticia en el textField
					noticiaE2 = txtAEconomia2.getText();
					categoriaE2 = tablaCategorias[1]; //Guarda la categor�a en variable.
					fuenteE2 = tablaFuentes[5];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtAEconomia2.setText("");
				}
			}
		
			//Noticia de Economia3: Cinco d�as.
			Document docEconomia3=null;
			String webEconomia3 = "https://cincodias.elpais.com/";
			
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("Cinco d�as"))
				{
					try {
						docEconomia3 = Jsoup.connect(webEconomia3).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaEconomia3 = docEconomia3.select(".articulo-titulo"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo los selectores.
					txtAEconomia3.setText(noticiaEconomia3.get(0).text());//Fija la noticia en el textField
					noticiaE3 = txtAEconomia3.getText();
					categoriaE3 = tablaCategorias[1]; //Guarda la categor�a en variable.
					fuenteE3 = tablaFuentes[6];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtAEconomia3.setText("");
				}
			}
			
			//Noticia de Economia4: Econom�a Digital.
			Document docEconomia4=null;
			String webEconomia4 = "https://www.economiadigital.es/";
			
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("Econom�a Digital"))
				{
					try {
						docEconomia4 = Jsoup.connect(webEconomia4).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en la variable tipo Document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaEconomia4 = docEconomia4.select("h2"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo los selectores.
					txtAEconomia4.setText(noticiaEconomia4.get(0).text());//Fija la noticia en el textField
					noticiaE4 = txtAEconomia4.getText();
					categoriaE4 = tablaCategorias[1]; //Guarda la categor�a en variable.
					fuenteE4 = tablaFuentes[7];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtAEconomia4.setText("");
				}
			}
	}
		
		/**
		 * M�todo para mostrar las noticias internacionales.
		*/
		
		protected void mostrarInternacional (JTextArea txtAInternacional1,JTextArea txtAInternacional2, JTextArea txtAInternacional3, JTextArea txtAInternacional4) { 
			//Noticia de Internacional1: BBC.
			Document docInternacional1=null;
			String webInternacional1 = "https://www.bbc.com/mundo";
			
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("BBC"))
				{
					try {
						docInternacional1 = Jsoup.connect(webInternacional1).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaInternacional1 = docInternacional1.select(".css-wla3o-Link"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo el selector de css.
					txtAInternacional1.setText(noticiaInternacional1.get(0).text());//Fija la noticia en el textField
					noticiaI1 = txtAInternacional1.getText();
					categoriaI1 = tablaCategorias[2]; //Guarda la categor�a en variable.
					fuenteI1 = tablaFuentes[8];//Guarda la fuente en variable.
					break;
				}
				else
				{
					txtAInternacional1.setText("");
				}
			}
			
			//Noticia de Internacional2: El Mundo Internacional.
			Document docInternacional2=null;
			String webInternacional2 = "https://www.elmundo.es/internacional.html";
			
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("El Mundo Internacional"))
				{
					try {
						docInternacional2 = Jsoup.connect(webInternacional2).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaInternacional2 = docInternacional2.select (".ue-c-cover-content__headline");
					txtAInternacional2.setText(noticiaInternacional2.get(0).text());//Fija la noticia en el textField
					noticiaI2 = txtAInternacional2.getText();
					categoriaI2 = tablaCategorias[2]; //Guarda la categor�a en variable.
					fuenteI2 = tablaFuentes[9];//Guarda la fuente en variable
					break;
				}
				else
				{
					txtAInternacional2.setText("");
				}
			}
		
			//Noticia de Internacional3: 20 Minutos Internacional.
			Document docInternacional3=null;
			String webInternacional3 = "https://www.20minutos.es/internacional/";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("20 Minutos Internacional"))
				{
					try {
						docInternacional3 = Jsoup.connect(webInternacional3).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaInternacional3 = docInternacional3.select(".media-content"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo los selectores.
					txtAInternacional3.setText(noticiaInternacional3.get(0).text());//Fija la noticia en el textField
					noticiaI3 = txtAInternacional3.getText();
					categoriaI3 = tablaCategorias[2]; //Guarda la categor�a en variable.
					fuenteI3 = tablaFuentes[10];//Guarda la fuente en variable
					break;
				}
				else
				{
					txtAInternacional3.setText("");
				}
			}
			
			//Noticia de Internacional4: ABC.
			Document docInternacional4=null;
			String webInternacional4 = "https://www.abc.es/internacional/";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("ABC"))
				{
					try {
						docInternacional4 = Jsoup.connect(webInternacional4).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en la variable tipo Document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaInternacional4 = docInternacional4.select(".lead-title"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo los selectores.
					txtAInternacional4.setText(noticiaInternacional4.get(0).text());//Fija la noticia en el textField
					noticiaI4 = txtAInternacional4.getText();
					categoriaI4 = tablaCategorias[2]; //Guarda la categor�a en variable.
					fuenteI4 = tablaFuentes[11];//Guarda la fuente en variable
					break;
				}
				else
				{
					txtAInternacional4.setText("");
				}
			}
	}
		
		/**
		 * M�todo para mostrar las noticias nacionales.
		*/
		protected void mostrarNacional (JTextArea txtANacional1,JTextArea txtANacional2, JTextArea txtANacional3, JTextArea txtANacional4) { 
			//Noticia de Nacional1: RTVE.
			Document docNacional1=null;
			String webNacional1 = "https://www.rtve.es/noticias/s/espana/";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("RTVE"))
				{
					try {
						docNacional1 = Jsoup.connect(webNacional1).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaNacional1 = docNacional1.select(".maintitle"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo el selector de css.
					txtANacional1.setText(noticiaNacional1.get(0).text());//Fija la noticia en el textField
					noticiaN1 = txtANacional1.getText();
					categoriaN1 = tablaCategorias[3]; //Guarda la categor�a en variable.
					fuenteN1 = tablaFuentes[12];//Guarda la fuente en variable
					break;
				}
				else
				{
					txtANacional1.setText("");
				}
			}
			
			//Noticia de Nacional2: 20 Minutos Nacional.
			Document docNacional2=null;
			String webNacional2 = "https://20minutos.es/nacional";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("20 Minutos Nacional"))
				{
					try {
						docNacional2 = Jsoup.connect(webNacional2).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaNacional2 = docNacional2.select (".breaking-news");
					txtANacional2.setText(noticiaNacional2.get(0).text());//Fija la noticia en el textField
					noticiaN2 = txtANacional2.getText();
					categoriaN2 = tablaCategorias[3]; //Guarda la categor�a en variable.
					fuenteN2 = tablaFuentes[13];//Guarda la fuente en variable
					break;
				}
				else
				{
					txtANacional2.setText("");
				}
			}
		
			//Noticia de Nacional3: Ok diario.
			Document docNacional3=null;
			String webNacional3 = "https://okdiario.com/espana/";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("Ok diario"))
				{
					try {
						docNacional3 = Jsoup.connect(webNacional3).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaNacional3 = docNacional3.select(".article-title"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo los selectores.
					txtANacional3.setText(noticiaNacional3.get(0).text());//Fija la noticia en el textField
					noticiaN3 = txtANacional3.getText();
					categoriaN3 = tablaCategorias[3]; //Guarda la categor�a en variable.
					fuenteN3 = tablaFuentes[14];//Guarda la fuente en variable
					break;
				}
				else
				{
					txtANacional3.setText("");
				}
			}
			
			//Noticia de Nacional4: El Mundo Nacional.
			Document docNacional4=null;
			String webNacional4 = "https://www.elmundo.es/espana.html";
			
			for(int c = 0; c < tamanoFuentes; c++)
			{
				if(fuentes[c].equals("El Mundo Nacional"))
				{
					try {
						docNacional4 = Jsoup.connect(webNacional4).get(); // Se conecta a la web creando una conexi�n. Aplicamos el m�todo get que devuelve la p�gina en html y lo guarda en la variable tipo Document.
						
					} catch(IOException e) {
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					Elements noticiaNacional4 = docNacional4.select(".ue-c-cover-content__link"); // En noticia mete varios elementos filtrados con select(en este caso uno). Utilizo los selectores.
					txtANacional4.setText(noticiaNacional4.get(0).text());//Fija la noticia en el textField
					noticiaN4 = txtANacional4.getText();
					categoriaN4 = tablaCategorias[3]; //Guarda la categor�a en variable.
					fuenteN4 = tablaFuentes[15];//Guarda la fuente en variable
					break;
				}
				else
				{
					txtANacional4.setText("");
				}
				
			}
	}
		
		//M�todo para comprobar que los ficheros existen y si no, mostrar mensaje y salir.
		public boolean comprobarFichero(String nombreFichero)
		{
			File fichero = null;
			try {
				fichero = new File(nombreFichero);
			//Comprobar si el fichero existe y si no, crearlo.
				while(!fichero.exists())
				{
						JOptionPane.showMessageDialog(null,"No se encuentra alg�n fichero, no es posible lanzar la app.", "", JOptionPane.OK_OPTION);
						System.exit(0);
				}
				
			}catch (Exception e) {
				JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
				System.exit(0);
			}
			return true;
		}
		
		//M�todo para abrir fichero en escritura (sobreescribe):
		public BufferedWriter abrirFichero(String nombreFichero)
		{
			FileWriter ficheroFW = null;
			BufferedWriter ficheroBW = null;
			
			try {
				File fichero = new File(nombreFichero);
			// 1. Comprobar si el fichero existe:
				if(!fichero.exists()) //Si no existe, crearlo:
				{
					fichero.createNewFile();
						System.out.println("Se ha creado el fichero\n");
				}else
				{ // 2. Si ya existe, abrirlo:
						ficheroFW = new FileWriter(fichero);
						ficheroBW = new BufferedWriter(ficheroFW);
					}
					// No cerramos el fichero porque vamos a operar sobre �l.
					// Se cerrar� en el m�todo que corresponda.
				}catch (Exception e) {
					JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
					System.exit(0);
				}
			return ficheroBW;
		}
		
		//M�todo para abrir fichero en escritura (agrega):
		public BufferedWriter abrirFicheroAgrega(String nombreFichero)
		{
			FileWriter ficheroFW = null;
			BufferedWriter ficheroBW = null;
			
			try {
				File fichero = new File(nombreFichero);
			// 1. Comprobar si el fichero existe:
				if(!fichero.exists()) //Si no existe, crearlo:
				{
					fichero.createNewFile();
						System.out.println("Se ha creado el fichero\n");
				}else
				{ // 2. Si ya existe, abrirlo:
						ficheroFW = new FileWriter(fichero, true);
						ficheroBW = new BufferedWriter(ficheroFW);
					}
					// No cerramos el fichero porque vamos a operar sobre �l.
					// Se cerrar� en el m�todo que corresponda.
				}catch (Exception e) {
					JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
					System.exit(0);
				}
			return ficheroBW;
		}

		//M�todo para guardar las opciones seleccionadas por el usuario en la ventana Configuraci�n.
		public void guardarConfiguracion(TablaConfiguraciones tablaConfiguraciones, String idUser)
		{
			//1. Abro fichero en lectura:
			BufferedReader ficheroBR = abrirFicheroLectura("configuracion.txt");
			
			//tabla para meter las configuraciones al leerlas y poder comparar los usuarios.
			TablaConfiguraciones tablaC = null;
			tablaC = new TablaConfiguraciones();
					try
					{
					String linea="";
					String idUsuario= "";
					//2. Leo del fichero y voy guardando en los objetos cada configuraci�n de una categor�a:
					while(((linea = ficheroBR.readLine()) != null))
					{ 
						idUsuario = linea;
						//Creo un objeto Configuraci�n y lo relleno con la informaci�n
						Configuracion configuracion = null;
						configuracion = new Configuracion();
						configuracion.idUser = idUsuario;
						linea = ficheroBR.readLine();
						configuracion.categoria = linea;
						linea = ficheroBR.readLine();
						int nFuentes = Integer.valueOf(linea);
						configuracion.nFuentes = nFuentes;
						for(int i = 0; i < configuracion.nFuentes; i++)
					    {
					    		configuracion.tablaFuentes[i] = linea = ficheroBR.readLine();				    	
					    }
						//Guardo cada objeto Configuraci�n en la tabla.
						tablaC.tabla[tablaC.tamano] = configuracion;
						tablaC.tamano++;
					}
					//3. Cerrar el fichero.
					ficheroBR.close();
					}catch (IOException e)
					{
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					
					//1. Abro fichero en sobrescritura.
					BufferedWriter ficheroBWr = abrirFichero("configuracion.txt");
					try
					{
						//2. Lo vac�o.
						ficheroBWr.write("");
						//3. Lo cierro.
						ficheroBWr.close();
					}catch(IOException e)
					{
					JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
					System.exit(0);
					}	
					
					//Recorro la tabla de configuraciones:
					for(int i = 0; i < tablaC.tamano; i++)
					{
						//Si coinciden id de usuario con el del usuario logado, borro la configuraci�n:
						if(tablaC.tabla[i].idUser.equals(idUser))
						{
							tablaC.tabla[i] = null;							
						}
						else 
						{							
							//1. Abro fichero para a�adir:
							BufferedWriter ficheroBW = abrirFicheroAgrega("configuracion.txt");
							try
							{
								//2. Escribe el objeto configuraci�n en el fichero porque es de otro usuario:
								ficheroBW.write(tablaC.tabla[i].idUser);
								ficheroBW.newLine();
								ficheroBW.write(tablaC.tabla[i].categoria);
								ficheroBW.newLine();
								int entero = tablaC.tabla[i].nFuentes;
								String lee = Integer.toString(entero);
								ficheroBW.write(lee);
								ficheroBW.newLine();
								for(int c = 0; c < tablaC.tabla[i].nFuentes; c++)
							    {
							    	if(tablaC.tabla[i] != null)
							    	{
							    		ficheroBW.write(tablaC.tabla[i].tablaFuentes[c]);
							    		ficheroBW.newLine();
							    	}					    	
							    }
								//3. Cierro fichero.
								ficheroBW.close();
							}catch
							(IOException e)
							{
								JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
								System.exit(0);
							}												
						}
					}
			
			// Ahora incluimos la nueva configuraci�n del usuario en el fichero configuraci�n:
			String categoria;
			int nFuentes;
			String tablaFuentes[] = new String[4];
			
			//Crear objeto auxiliar
			Configuracion configuracionGuarda = new Configuracion();
			// 1. Abrir fichero para guardar (no a�ade, sobreescribe).
			BufferedWriter ficheroBW = abrirFicheroAgrega("configuracion.txt");
			try {
			    // 2. Escribir datos de la tabla al fichero.
					//Recorro la tabla de configuraciones:
			    for(int c = 0; c < tablaConfiguraciones.tamano; c++)//tamano es el n� de posiciones ocupadas de la tabla.
			    {
			    	//Si la posici�n c de la tabla no es null:
			    	if(tablaConfiguraciones.tabla[c] != null)
			    	{
			    		//Copio el objeto Configuraci�n de la posici�n actual de la tabla en el objeto Configuracion auxiliar.
			    		configuracionGuarda = tablaConfiguraciones.tabla[c];
					    categoria = configuracionGuarda.categoria;
					    nFuentes = configuracionGuarda.nFuentes;
					    
					    for(int d = 0; d < configuracionGuarda.nFuentes; d++)
					    {
					    	if(configuracionGuarda.tablaFuentes[d] != null)
					    	tablaFuentes[d] = configuracionGuarda.tablaFuentes[d];
					    }
					    
					    //Escribo en el fichero los valores de las variables auxiliares.

					    String linea = idUser;
					    ficheroBW.write(linea);
					    ficheroBW.newLine();
					    ficheroBW.write(categoria);
					    ficheroBW.newLine();
					    int numeroFuentes = nFuentes;
					    //Convierto el int nFuentes a string para poder escribirlo en el fichero.
					    String linea2 = Integer.toString(nFuentes);
					    ficheroBW.write(linea2);
					    ficheroBW.newLine();
					    for(int j = 0; j < numeroFuentes; j++)
					    {
					    	ficheroBW.write(tablaFuentes[j]);
					    	ficheroBW.newLine();
					    }
			    	}
			    	
			    }
			    // 3. Cerrar el fichero
			    ficheroBW.close();
			}catch (IOException e) {
				JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
				System.exit(0);
			}
		}
		
		//M�todo para leer el fichero de configuraci�n, que guarde las fuentes en un string, para saber qu� noticias mostrar:
		public void leerCompleto(String fichero)
		{
			//1. Abrir fichero:
			BufferedReader ficheroBR =configuracion.abrirFicheroLectura("configuracion.txt");
			
			try
			{
			//2. Leer y cargar datos del fichero en la tabla:
			String linea="";
			int entero;
			//Leo del fichero y voy guardando en un objeto Configuracion:
			while((linea = ficheroBR.readLine()) != null)
			{ 
				Configuracion configuracionG = new Configuracion();
				configuracionG.idUser = linea;
				if(configuracionG.idUser.equals(idUser))
				{
					linea = ficheroBR.readLine();
					configuracionG.categoria = linea;
					linea = ficheroBR.readLine();
					configuracionG.nFuentes = entero = (Integer.valueOf(linea));
					configuracionG.tablaFuentes = new String[entero];
					//Guardo las fuentes en tablaFuentes del objeto:
					
					for(int c = 0; c < entero; c++)
					{
						configuracionG.tablaFuentes[c] = linea = ficheroBR.readLine();
						//Guardo cada fuente en la tabla fuentes global.
						fuentes[tamanoFuentes++] = configuracionG.tablaFuentes[c];
					}
				}else
				{
					ficheroBR.readLine();
					String lee= ficheroBR.readLine();
					int numero= (Integer.valueOf(lee));
					for(int c = 0; c < numero; c++)
					{
						linea = ficheroBR.readLine();
					}
				}
				
				
			}
			
			//3. Cerrar el fichero.
			ficheroBR.close();
			}catch (IOException e)
			{
				JOptionPane.showMessageDialog(null,"Error de la aplicaci�n", "", JOptionPane.OK_OPTION);
				System.exit(0);
			}
			
		}
		
		//M�todo para guardar las noticias actuales en el fichero logNoticias y poder mostrarlas en la Ventana Noticias Antiguas:
		public void guardarNoticias(JTextArea textArea, String noticia, String categoria, String fuente)
		{
			
			//1. Si la noticia est� vac�a:
				if(noticia == null)
				{
					boolean vacio = true;
				}else
				{
					//1. Abro fichero y lo dejo preparado para escritura:
					BufferedWriter ficheroBW = abrirFicheroAgrega("logNoticias.txt");
					
					try
					{
						String linea = idUser;
						ficheroBW.write(linea);
					    ficheroBW.newLine();	
					    String fecha = fechaActual.toString();//Convierto fecha de LocalDate a String
					    ficheroBW.write(fecha);
					    ficheroBW.newLine();
					    ficheroBW.write(categoria);
					    ficheroBW.newLine();
					    ficheroBW.write(fuente);
					    ficheroBW.newLine();
					    ficheroBW.write(textArea.getText());
					    ficheroBW.newLine();
		
					    // 3. Cerrar el fichero
					    ficheroBW.close();
					}catch(IOException e)
					{
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					noticia = "";
					categoria = "";
					fuente = "";
				}		
		}
		
		
		public void guardarNoticias1(JTextArea textArea, String noticia, String categoria, String fuente)
		{
			
			//1. Si la noticia est� vac�a:
				if(noticia == null)
				{
					boolean vacio = true;
				}else
				{
					//1. Abro fichero y lo dejo preparado para escritura:
					BufferedWriter ficheroBW = abrirFicheroAgrega("logNoticias.txt");
					
					try
					{
						String linea = idUser;
						ficheroBW.write(linea);
					    ficheroBW.newLine();	
					    String fecha = fechaActual.toString();//Convierto fecha de LocalDate a String
					    ficheroBW.write(fecha);
					    ficheroBW.newLine();
					    ficheroBW.write(categoria);
					    ficheroBW.newLine();
					    ficheroBW.write(fuente);
					    ficheroBW.newLine();
					    ficheroBW.write(textArea.getText());
					    ficheroBW.newLine();
		
					    // 3. Cerrar el fichero
					    ficheroBW.close();
					}catch(IOException e)
					{
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					noticia = "";
					categoria = "";
					fuente = "";
				}		
		}
		
	//M�todo para mostrar las noticias antiguas, cuando las tiene:
	public void mostrarAntiguas(File fichero, JTextArea textAreaMuestraAntiguas)
	{
		String texto = "";
		//1. Abro fichero en lectura:
		BufferedReader ficheroBR = ficheroNoticiasAntiguas.abrirFicheroLectura("logNoticias.txt");
		
		try
		{
		//2. Leer y comparar datos del fichero:
		String linea="";
		String aux= "";
		//Leo del fichero y voy guardando en aux para hacer la comparaci�n con el id del usuario:
		while(((linea = ficheroBR.readLine()) != null))
		{ 
			aux = linea;
			if(aux.equals(idUser)) //Si el id de usuario leido es igual al del usuario logado, imprime sus noticias.
			{
				for(int i = 0; i < 4; i++)
				{
					texto += linea + "\n";
					linea = ficheroBR.readLine();
				}
				texto += linea + "\n";
			}
			else
			{
				ficheroBR.readLine();
				ficheroBR.readLine();
				ficheroBR.readLine();
				ficheroBR.readLine();
			}
								
		}
		//3. Cerrar el fichero.
		ficheroBR.close();
		}catch (IOException e)
		{
			JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
			System.exit(0);
		}
		textAreaMuestraAntiguas.setText(texto);
	}
	
	//M�todo para si el usuario tiene configuraci�n guardada:
	public boolean comprobarPrimerAcceso(String nombreFichero)
	{
		fichero = new File(nombreFichero);
		BufferedReader ficheroBR = abrirFicheroLectura("configuracion.txt");
		try {

				try
				{
					String linea = "";
					//Leo del fichero
					while((linea = ficheroBR.readLine()) != null)
					{ 
						if(linea.equals(idUser))
						{
							ficheroBR.close();
							return true;
						}

					}
					
				    // 3. Cerrar el fichero
				    ficheroBR.close();
				    return false;
				    
				}catch(IOException e)
				{
					JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
					System.exit(0);
				}
				return true;
			
		}catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
			System.exit(0);
		}
		return false;
	}
	
	//M�todo para abrir fichero en lectura:
	public BufferedReader abrirFicheroLectura(String nombreFichero)
	{
		FileReader ficheroFR = null;
		BufferedReader ficheroBR = null;
		
		try {
			File fichero = new File(nombreFichero);
		
					ficheroFR = new FileReader(fichero);
					ficheroBR = new BufferedReader(ficheroFR);
				// No cerramos el fichero porque vamos a operar sobre �l.
				// Se cerrar� en el m�todo que corresponda.
			}catch (Exception e) {
				JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
				System.exit(0);
			}
		return ficheroBR;
	}
	
	//M�todo para abrir el fichero de usuarios, leer y validar si el usuario y contrase�a introducidos son correctos.
		public void validarUsuarioContrasena(String nombreFichero, String stringUsuario , String stringPassword)
		{
			File fichero = null;
			FileReader ficheroFR = null;
			BufferedReader ficheroBR = null;
			
			try
			{
					fichero = new File("usuarios.txt");
					ficheroFR = new FileReader(fichero);
					ficheroBR = new BufferedReader(ficheroFR);
					String linea="";
					// 2. Lectura del fichero con tres usuarios.		
						while ((linea = ficheroBR.readLine()) !=null)
						{
							//int id = (Integer.parseInt(linea));
							String id = linea;
							idUser = id;
							linea = ficheroBR.readLine();
							String usuario = linea;
							linea = ficheroBR.readLine();
							String contrasena = linea;
							if((usuario.equals(stringUsuario)) && (contrasena.equals(stringPassword)))
							{
								JOptionPane.showMessageDialog(null,"Datos de usuario correctos", "", JOptionPane.OK_OPTION);
								ficheroFR.close();
								ficheroBR.close();
								//Comprueba si el usuario tiene configuraciones guardadas.
								if(comprobarPrimerAcceso("configuracion.txt"))
								{
									panelLogin.setVisible(false);
									panelOpciones.setVisible(true);
								}else
								{
									panelLogin.setVisible(false);
									panelConfigurar.setVisible(true);
								}	
								break;
								
								
							}
							if((linea == null) && ((usuario.equals(stringUsuario)) || (contrasena.equals(stringPassword))))
							{
								JOptionPane.showMessageDialog(null,"Datos de usuario incorrectos", "", JOptionPane.OK_OPTION);
								panelLogin.setVisible(true);
								panelOpciones.setVisible(false);
							}

						}
										
				//3. Cierre del fichero.
				ficheroFR.close();
				ficheroBR.close();
			}catch(IOException e)
			{
				JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
				System.exit(0);
			}
			}
		
		//M�todo para mostrar las noticias de la fecha indicada por el usuario.
		public void mostrarPorFecha(File fichero, String stringFecha, JTextArea textAreaMuestraAntiguas)
		{
			String texto= "";
			//1. Abro fichero en lectura:
			BufferedReader ficheroBR = ficheroNoticiasAntiguas.abrirFicheroLectura("logNoticias.txt");
			//tabla para meter las noticias al leerlas y poder comparar las fechas.
			NoticiasAntiguas tablaAntiguas[] = null;
			tablaAntiguas = new NoticiasAntiguas[50000];
			int tamanoTablaAntiguas = 0;
					try
					{
					//2. Leer y comparar datos del fichero:
					String linea="";
					String idUsuario= "";
					String fecha= "";
					//Leo del fichero y voy guardando en los objetos:
					while(((linea = ficheroBR.readLine()) != null))
					{ 
						idUsuario = linea;
						fecha = linea = ficheroBR.readLine();
						//Creo un objeto NoticiasAntiguas y lo relleno con la inform
						NoticiasAntiguas noticiaAntigua = null;
						noticiaAntigua = new NoticiasAntiguas();
						noticiaAntigua.id = idUsuario;
						noticiaAntigua.fechaAntiguas = fecha;
						noticiaAntigua.categoriaAntiguas = linea = ficheroBR.readLine();
						noticiaAntigua.fuenteAntiguas = linea = ficheroBR.readLine();
						noticiaAntigua.titularAntiguas = linea = ficheroBR.readLine();
						//Guardo cada objeto NoticiasAntiguas en la tabla.
						tablaAntiguas[tamanoTablaAntiguas] = noticiaAntigua;
						tamanoTablaAntiguas++;
					}
					//3. Cerrar el fichero.
					ficheroBR.close();
					}catch (IOException e)
					{
						JOptionPane.showMessageDialog(null,"Error en la aplicaci�n, se cerrar�.", "", JOptionPane.OK_OPTION);
						System.exit(0);
					}
					
					//Recorro la tabla de noticias antiguas:
					for(int i = 0; i < tamanoTablaAntiguas; i++)
					{
						//Si coinciden id de usuario y fecha con la indicada por el usuario, guarda el objeto en texto:
						if((tablaAntiguas[i].id.equals(idUser)) && (tablaAntiguas[i].fechaAntiguas.equals(stringFecha)))
						{
							texto += tablaAntiguas[i].fechaAntiguas + "\n" + tablaAntiguas[i].categoriaAntiguas + "\n" + tablaAntiguas[i].fuenteAntiguas  + "\n" + tablaAntiguas[i].titularAntiguas;
						}
					}
					//Muestra texto en el textArea:
					textAreaMuestraAntiguas.setText(texto);
					//Si el textArea est� vac�o, no hay noticias de esa fecha y se avisa con mensaje:
					if(texto.equals(""))
					{
						JOptionPane.showMessageDialog(null,"No hay noticias de esa fecha", "", JOptionPane.OK_OPTION);
					}
				}
		}
