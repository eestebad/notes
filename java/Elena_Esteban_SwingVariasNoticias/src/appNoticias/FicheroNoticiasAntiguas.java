package appNoticias;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import javax.swing.JOptionPane;

public class FicheroNoticiasAntiguas {
	protected File ficheroAntiguas;
	
	public FicheroNoticiasAntiguas()
	{
		ficheroAntiguas = new File("logNoticias.txt");
	}
	

	
	//M�todo para comprobar si el fichero de logNoticias est� vac�o y avisar al usuario.
	public boolean ficheroVacio(File ficheroAntiguas)
	{
		if(ficheroAntiguas.length() == 0)//Si el fichero tiene cero l�neas.
		{
			return true;
		}else
		{
			return false;
		}
	}
	
	//M�todo para abrir fichero en lectura:
	public BufferedReader abrirFicheroLectura(String nombreFichero)
	{
		FileReader ficheroFR = null;
		BufferedReader ficheroBR = null;
		
		try {
			File fichero = new File(nombreFichero);
		// 1. Comprobar si el fichero existe:
			if(!fichero.exists()) //Si no existe, crearlo:
			{
				fichero.createNewFile();
					System.out.println("Se ha creado el fichero\n");
			}else
			{ // 2. Si ya existe, abrirlo:
					ficheroFR = new FileReader(fichero);
					ficheroBR = new BufferedReader(ficheroFR);
				}
				// No cerramos el fichero porque vamos a operar sobre �l.
				// Se cerrar� en el m�todo que corresponda, con el m�todo cerrar.
			}catch (Exception e) {
				e.printStackTrace();
			}
		return ficheroBR;
	}
	
	
	
	
	
}
