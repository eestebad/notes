package Ejercicios6_7;

public class Administracion extends Persona{
	
	//dni, nombre, apellidos y salario los hereda de Persona.
	private String estudios;
	private double antiguedad;
	
	//Constructor:
	public Administracion(String dni, String nombre, String apellidos, double salario, String estudios, double antiguedad) {
		super(dni, nombre, apellidos, salario); //Hereda de la clase Persona.
		this.setEstudios(estudios);
		this.setAntiguedad(antiguedad);
	}

	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public double getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(double antiguedad) {
		this.antiguedad = antiguedad;
	}
	
	@Override
	public String toString() {
		return "Administracion [DNI=" + dni + ", Nombre=" + nombre + ", Apellidos=" + apellidos +", Salario=" + salario + ", Estudios=" + estudios + ", AntigŁedad =" + antiguedad + "]";
	}

}
