package Ejercicios6_7;

public class Profesor extends Persona{

	//dni, nombre, apellidos y salario los hereda de Persona.
	private int nAsignaturas;
	private boolean tutor;
	
	//Constructor:
	public Profesor(String dni, String nombre, String apellidos, double salario, int nAsignaturas, boolean tutor) {
		super(dni, nombre, apellidos, salario); //Hereda de la clase Persona.
		this.setnAsignaturas(nAsignaturas);
		this.setTutor(tutor);
	}

	public int getnAsignaturas() {
		return nAsignaturas;
	}

	public void setnAsignaturas(int nAsignaturas) {
		this.nAsignaturas = nAsignaturas;
	}

	public boolean isTutor() {
		return tutor;
	}

	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}
	
	@Override
	public String toString() {
		return "Profesor [DNI=" + dni + ", Nombre=" + nombre + ", Apellidos=" + apellidos +", Salario=" + salario + ", N� asignaturas=" + nAsignaturas + ", Tutor =" + tutor + "]";
	};
	
}
