package Ejercicios6_7;

public class Persona {
	// Superclase con los atributos dni, nombre, apellidos y salario, que son comunes a las 3 clases.
		protected String dni;
		protected String nombre;
		protected String apellidos;
		protected double salario;
		
		//Constructor:
		public Persona(String dni, String nombre, String apellidos, double salario) {
			this.dni = dni;
			this.nombre = nombre;
			this.apellidos = apellidos;
			this.salario = salario;
		}
		// Getter y Setter:
		public String getDni() {
			return dni;
		}
		public void setDni(String dni ) {
			this.dni = dni;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre ) {
			this.nombre = nombre;
		}
		public String getApellidos() {
			return apellidos;
		}
		public void setApellidos(String apellidos) {
			this.apellidos = apellidos;
		}
		public double getSalario() {
			return salario;
		}
		public void setSalario(double salario) {
			this.salario = salario;
		}
		// M�todo toString:
		public String toString() {
			return "Persona [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", salario=" + salario + "]";
		}
}
