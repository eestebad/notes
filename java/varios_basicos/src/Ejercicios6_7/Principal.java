package Ejercicios6_7;

import Ejercicios6_7.Directivo.tipoTurno;

/* Practicas1DAM/ Ejercicios 6 y 7: 
 *  Ejercicio 6: El centro quiere informatizar a sus trabajadores en un software implementado en Java. A partir de
 *  los datos que se muestran a continuaci�n (Profesor, Administraci�n; Directivo), programar el software y crear 6
 *  objetos de prueba mostrando su informaci�n por pantalla.
 *  Ejercicio 7: Modificar el programa anterior aplicando herencia. (Se incluye directamente).
 *  @author Elena Esteban D�ez.
 *  @return void por pantalla.
 */

public class Principal {


	public static void main(String[] args) {
		tipoTurno turno = null; //Inicializaci�n el enum a null.
		
		//Inicializaci�n de 6 objetos de los tipos creados:
		Profesor uno = new Profesor("12345678K", "Eva", "Gonz�lez", 1300.80, 5, true);
		Profesor dos = new Profesor("12345678T", "Carlos", "L�pez", 1200.50, 6, false);
		
		Administracion a = new Administracion("12345678R", "Ignacio", "Dorado", 1000.80, "BUP", 15.6);
		Administracion b = new Administracion("12345678J", "�ngel", "Calle", 1000.60, "FP Administraci�n", 10.3);
		
		Directivo directivoUno = new Directivo("12345678X", "Laura", "G�mez", 1700, false, turno = tipoTurno.valueOf("Ma�ana"));
		Directivo directivoDos = new Directivo("12345678V", "Isabel", "Mart�nez", 1700, false, turno = tipoTurno.valueOf("Tarde"));
		
		// Se imprimen los seis objetos:
		System.out.println("Profesores:\n\t"+ uno.toString() +" \n\t"+ dos.toString());
		System.out.println("Administraci�n:\n\t"+ a.toString() +" \n\t"+ b.toString());
		System.out.println("Directivos:\n\t"+ directivoUno.toString() +" \n\t"+ directivoDos.toString());

	}

}
