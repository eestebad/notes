package Ejercicios6_7;

public class Directivo extends Persona{
	
	//dni, nombre, apellidos y salario los hereda de Persona.
	private boolean salesiano;
	protected enum tipoTurno {Ma�ana, Tarde};
	tipoTurno turno;
	
	//Constructor:
	public Directivo(String dni, String nombre, String apellidos, double salario, boolean salesiano, tipoTurno turno) {
		super(dni, nombre, apellidos, salario);
		this.setSalesiano(salesiano);
		this.setTurno(turno);
	}


	public boolean isSalesiano() {
		return salesiano;
	}

	public void setSalesiano(boolean salesiano) {
		this.salesiano = salesiano;
	}
	
	public tipoTurno getTurno() {
		return turno;
	}

	public void setTurno(tipoTurno turno) {
		this.turno = turno;
	}
	
	@Override
	public String toString() {
		return "Directivo [DNI=" + dni + ", Nombre=" + nombre + ", Apellidos=" + apellidos +", Salario=" + salario + ", Salesiano=" + salesiano + ", Turno =" + turno + "]";
	}
}
