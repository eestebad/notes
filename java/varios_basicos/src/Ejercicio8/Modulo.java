package Ejercicio8;

public class Modulo {
	private String nombreModulo;
	private int nHorasModulo;
	private String profesorModulo;
	private boolean convalidableModulo;
	
	//Constructor:
	public Modulo(String nombreModulo, int nHorasModulo, String profesorModulo, boolean convalidableModulo) {
		this.setNombreModulo(nombreModulo);
		this.setnHorasModulo(nHorasModulo);
		this.setProfesorModulo(profesorModulo);
		this.setConvalidableModulo(convalidableModulo);
	}

	public String getNombreModulo() {
		return nombreModulo;
	}

	public void setNombreModulo(String nombreModulo) {
		this.nombreModulo = nombreModulo;
	}

	public int getnHorasModulo() {
		return nHorasModulo;
	}

	public void setnHorasModulo(int nHorasModulo) {
		this.nHorasModulo = nHorasModulo;
	}

	public String getProfesorModulo() {
		return profesorModulo;
	}

	public void setProfesorModulo(String profesorModulo) {
		this.profesorModulo = profesorModulo;
	}

	public boolean isConvalidableModulo() {
		return convalidableModulo;
	}

	public void setConvalidableModulo(boolean convalidableModulo) {
		this.convalidableModulo = convalidableModulo;
	}
	
	@Override
	public String toString() {
		return "Modulo [Nombre m�dulo=" + nombreModulo + ", N� Horas m�dulo=" + nHorasModulo +", Profesor asignado=" + profesorModulo + ", M�dulo convalidable=" + convalidableModulo + "]";
	};

}
