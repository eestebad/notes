package Ejercicio8;

import java.util.Scanner;

import Ejercicio8.Alumno.tipoSexo;

/* Practicas1DAM/ Ejercicio 8: 
 *  Ejercicio 8: Implementar mediante POO el siguiente supuesto: Un m�dulo se caracteriza por tener un nombre, 
 *  un numero de horas, un profesor y si es o no convalidable. Un alumno se caracteriza por tener un DNI, un 
 *  nombre y apellidos, una fecha de nacimiento, su sexo y si es o no repetidor. Adem�s, cada alumno tiene 
 *  asociado un conjunto de m�dulos que son los que se ha matriculado. Una vez implementado este supuesto, crear
 *  los objetos necesarios, mostrando por pantalla la informaci�n de 2 alumnos junto con sus m�dulos matriculados.
 *  @author Elena Esteban D�ez.
 *  @return void por pantalla.
 */

public class Principal {

	public static void main(String[] args) {
		Scanner leerTeclado= new Scanner(System.in); // Variable para recoger datos
		int opcion;
				
				// T�tulo y Men� principal.
				System.out.println("*** MEN� ***\n"); 
				System.out.println("Men�: \n 1 -> Imprimir alumnos. \n 2 -> Salir.\n");
			
				opcion = leerTeclado.nextInt();
				boolean exit = false;
				
					do {
						Alumno alumnoUno = new Alumno ("12345678K", "Eva", "Gonz�lez", "22-07-2000", tipoSexo.valueOf("Mujer"), false);
						Modulo moduloUno = new Modulo ("Desarrollo de Interfaces", 140, "Sergio S�nchez-Crespo", true);
						Modulo moduloDos = new Modulo ("Sistemas Gesti�n Empresarial", 100, "Gonzalo Mart�nez", false);
						Modulo moduloTres = new Modulo ("Acceso a datos", 100, "Alicia P�rez", false);
						Alumno alumnoDos = new Alumno("12345678T", "Carlos", "L�pez", "18-04-2001", tipoSexo.valueOf("Hombre"), true);
						//Men�
						switch (opcion) {
							case 1: //Imprimir los alumnos y los m�dulos que tienen matriculados.					
						
								System.out.println("ALUMNOS:\n"+ "* ALUMNO uno: " + " \n\t" + alumnoUno.toString() + " \n\t");
								System.out.println ("\t M�dulos:" + " \n\t" + moduloUno.toString() + " \n\t" + moduloDos.toString());
								System.out.println("\n"+ "* ALUMNO dos: " + " \n\t" + alumnoDos.toString() + " \n\t");
								System.out.println ("\t M�dulos:" + " \n\t" + moduloUno.toString() + " \n\t" + moduloTres.toString() + " \n\t");
								break;
							
							case 2: 
								//leerTeclado.nextInt();
								
								System.out.println("Ha salido del sistema.\n");
								exit = true;
								break;

							default: // En caso de que el usuario escriba una opci�n que no sea 1 ni 2, da mensaje 
								// indicando qu� n�meros son v�lidos y repite el men�.
								System.out.println("Debe introducir 1 � 2. \n"); 
								break;
						}
						if(!exit) { // Si el programa devuelve true, es porque se ha ejecutado y muestra de nuevo el men�
							//al usuario antes de salir del do while.
							System.out.println("Men�: \n 1 -> Imprimir alumnos. \n 2 -> Salir.\n");
							opcion = leerTeclado.nextInt();
						}
					}while(exit==false); // Si el programa devuelve false, es porque el usuario ha elegido Salir.
					
				
				leerTeclado.close(); // Cerramos el Scanner.
				

	}

}
