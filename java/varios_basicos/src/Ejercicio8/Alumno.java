package Ejercicio8;


public class Alumno {
	private String dni;
	private String nombre;
	private String apellidos;
	private String fechaNacimiento;
	protected enum tipoSexo {Hombre, Mujer};
	tipoSexo sexo;
	private boolean repetidor;
	
	//Constructor:
	public Alumno(String dni, String nombre, String apellidos, String fechaNacimiento, tipoSexo sexo, boolean repetidor) {
		this.setDni(dni);
		this.setNombre(nombre);
		this.setApellidos(apellidos);
		this.setFechaNacimiento(fechaNacimiento);
		this.setSexo(sexo);
		this.setRepetidor(repetidor);
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public tipoSexo getSexo() {
		return sexo;
	}

	public void setSexo(tipoSexo sexo) {
		this.sexo = sexo;
	}
	public boolean isRepetidor() {
		return repetidor;
	}

	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}

	
	@Override
	public String toString() {
		return "Alumno [DNI=" + dni + ", Nombre=" + nombre + ", Apellidos=" + apellidos +", Fecha de nacimiento=" + fechaNacimiento + ", Sexo=" + sexo + ", Repetidor =" + repetidor + "]";
	}


}
