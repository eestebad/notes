package Ejercicio3;

/* Practicas1DAM/ Ejercicio3: 
 *  A partir de un array de enteros, recorrerlo y obtener por pantalla la cantidad total de 
 *  n�meros pares.
 *  @author Elena Esteban D�ez.
 *  @return void por pantalla.
 */

public class ejercicio3 {
	public static void main(String[] args) {
		// Creaci�n array de enteros:
		int array[];
		// Inicializaci�n del array con cuatro enteros:
		array = new int[] {25, 30, 13, 42};
		// Declaro e inicializo un contador para obtener la cantidad total de n�meros pares:
		int contador = 0;
	
		/* Impresi�n del array completo y de la cantidad total de n�meros pares contenidos en
		el array: */
		// Se imprimen todos los elementos del array y cu�ntos n�meros pares contiene:
		 for (int i = 0; i < array.length; i++)
		     { 
			 // Se imprimen todos los elementos del array:
	         System.out.println("El elemento " + i + " es " + array[i]);
	         // Si el resto del elemento entre 2 es 0, se considera al n� par y se suma 1 al contador:
	         if((array[i]%2) == 0)
	        	 contador += 1;
		      }
		 System.out.println("El array contiene " + contador + " n�meros pares.");

	}

}
