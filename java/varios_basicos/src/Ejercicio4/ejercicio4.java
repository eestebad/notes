package Ejercicio4;

import java.util.InputMismatchException;
import java.util.Scanner;

/* Practicas1DAM/ Ejercicio4: 
 *  Crear una matriz de enteros de n x n (tama�o lo decide el usuario), rellenar con n�meros
 *  aleatorios, recorrerla y dar la suma de todos sus n�meros.
 *  @author Elena Esteban D�ez.
 *  @return void por pantalla.
 */

public class ejercicio4 {

	public static void main(String[] args) {
		// Declaraci�n de variables:
		 Scanner leerTeclado1= new Scanner(System.in);
		 Scanner leerTeclado2= new Scanner(System.in);
	     int ancho, altura;
	     int resultado = 0;
		 int matrix[][] = null;
		 
	     try {
	    	
			// Se pide al usuario que nos d� las medidas de la matriz:
	        System.out.println( "Introduzca el ancho de la matriz: " );
	        ancho = leerTeclado1.nextInt();

	        System.out.printf( "Introduzca el largo de la matriz: " );
	        altura = leerTeclado2.nextInt();
	        
	        // Inicializaci�n del tama�o de la matriz con los n�meros introducidos por el usuario:
			matrix = new int[ancho][altura];
			
			leerTeclado1.close();   // Cerramos la variable Scanner.
			leerTeclado2.close();   // Cerramos la variable Scanner.
			
			// Rellenamos la matriz con n�meros alearios del 1 al 10:	
			for (int i = 0; i < ancho; i++)
				{
					for (int j = 0; j < altura; j++)
						matrix[i][j] = (int) (Math.random()*9+1);
				};
			
			//Imprimo el contenido de la matriz:
			for (int i = 0; i <ancho; i++)
				{
					for (int j = 0; j < altura; j++)
						System.out.print(matrix[i][j] + ", ");
				};
				
			System.out.println(); // Imprimo salto de l�nea.
				
			// Sumamos todos los n�meros de la matriz:
			for (int i = 0; i < ancho; i++)
			{
				for (int j = 0; j < altura; j++)
					resultado += matrix[i][j];
			};
			// Imprimo el resultado de la suma:
			System.out.println("La suma de los n�meros de la matriz es: " + resultado);
			
		}catch(InputMismatchException e) { // Si cuando se pide un n� el usuario mete una letra
			//da el mensaje siguiente:
			System.out.println("Debe introducir un n�mero, reinicie el programa.\n");
		}

}
}
