package Ejercicio5;

/* Practicas1DAM/ Ejercicio5: 
 * Calcular el factorial de un n�mero entero positivo a trav�s de la recursividad.
 *  @author Elena Esteban D�ez.
 *  @return void por pantalla.
 */

public class ejercicio5 {
	// Definici�n e inicializaci�n de constante, de la que se calcular� el factorial:
	final static int n = 6;
	
	public static void main(String[] args) {
		
		System.out.println("El factorial del n� " + n + " es " + factorial(n));


}
	// M�todo para calcular el factorial recursivamente:
	private static int factorial(int i) {
		if(i <1)
			return 1;
		
		return i * factorial(i-1);
	
	}
}
