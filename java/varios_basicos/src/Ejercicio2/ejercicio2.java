package Ejercicio2;

/* Practicas1DAM/ Ejercicio2: 
 * Crear un programa sencillo en el que se creen variables de tipo String, int, char, double,
 *  boolean. Asociarles un valor e imprimirlo por pantalla.
 *  @author Elena Esteban D�ez.
 *  @return void por pantalla.
 */

public class ejercicio2 {
	public static void main(String[] args) {
		// Creaci�n de variables y asociaci�n a un valor:
		String palabra = "soleado";
		int a = 13;
		char c = 'a';
		double d = 3.4;
		boolean b = true;
		// Se imprimen por pantalla:
			System.out.println("El String contiene: "+ palabra);
			System.out.println("El entero contiene: "+ a);
			System.out.println("El char contiene: "+ c);
			System.out.println("El double contiene: "+ d);
			System.out.println("El booleano contiene: "+ b);
	}

}
