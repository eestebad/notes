package Agencia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

//Programa para gestionar el funcionamiento de una agencia de viajes con fichero.

public class PrincipalAgencia {

	public static void main(String[] args) {
		FicheroItinerarios ficheroIt = new FicheroItinerarios();
		File fichero = new File("itinerarios.txt");
		File ficheroMaximo = new File("maximoDestinos.txt");
		File ficheroRestoDestinos = new File("restoDestinos.txt");
		ficheroIt.abrirFichero("itinerarios.txt");
		MenuAgencia menuA = new MenuAgencia();
		PrincipalAgencia principal = new PrincipalAgencia();
		Itinerario itinerario = new Itinerario();
		int opcion;
		int posicion = 0;
		int posicionb = 0;
		String nombreI = "";
		int numero = 0;
		String[] tablaDestinos;
		tablaDestinos = new String[3];

		Scanner leerTeclado = new Scanner(System.in);
		
		do
		{
			opcion = menuA.menu();
			
			switch(opcion) {
				case 1: //Leer fichero:
					principal.leer(ficheroIt);
					break;
				case 2: //Insertar itinerario:
					System.out.println("Introduzca el nombre del nuevo itinerario: ");
					nombreI = leerTeclado.nextLine();
					System.out.println("Introduzca el n� de destinos (m�ximo tres): ");
					numero = leerTeclado.nextInt();
					leerTeclado.nextLine();
					for( int c = 0; c < numero; c++)
					{
						System.out.println("Introduzca el destino: ");
						tablaDestinos[c] = leerTeclado.nextLine();
					}
					ficheroIt.escribir(fichero, nombreI, numero, tablaDestinos);
					break;
				case 3: //Borrar itinerario de la tabla:
					System.out.println("Introduzca la posici�n del itinerario a borrar: ");
					posicionb = leerTeclado.nextInt();
					principal.borrar(ficheroIt, posicionb);
					break;
				case 4: //Vaciar los itinerarios del programa, no del fichero:
					principal.vaciarTabla(ficheroIt);//Vaciar tabla.
					break;
				case 5: //Modificar itinerario sobre la tabla:
					System.out.println("Introduzca la posici�n del itinerario a modificar: ");
					posicionb = leerTeclado.nextInt();
					leerTeclado.nextLine();
					System.out.println("Introduzca el nombre nuevo: ");
					nombreI = leerTeclado.nextLine();
					System.out.println("Introduzca el n� de destinos (m�ximo tres): ");
					numero = leerTeclado.nextInt();
					leerTeclado.nextLine();
					for( int c = 0; c < numero; c++)
					{
						System.out.println("Introduzca el destino: ");
						tablaDestinos[c] = leerTeclado.nextLine();
					}
					principal.modificar(ficheroIt, posicionb, nombreI, numero, tablaDestinos);
					break;
				case 6: //Guardar datos de la tabla en el fichero:
					principal.guardar(ficheroIt, fichero, posicion, nombreI, numero, tablaDestinos, itinerario);
					break;
				case 7: //Mostrar contenido de la tabla:
					principal.mostrarTabla(ficheroIt);
					break;
				case 8: //Separar fichero:
					System.out.println("Introduzca el n� m�ximo de destinos: ");
					int maximo = leerTeclado.nextInt();
					leerTeclado.nextLine();
					principal.separar(fichero, ficheroIt, tablaDestinos, posicion, itinerario, nombreI, numero, maximo, ficheroMaximo, ficheroRestoDestinos);
					break;
				case 9: //Mostrar contenido fichero:
					principal.mostrar(fichero, ficheroIt);
					break;
				case 10: //Mostrar itinerarios de menor a mayor:
					
					break;
				case 11: //Mostrar itinerarios de mayor a menor:
					
					break;
				case 12: //Mostrar el destino m�s repetido de todos los itinerarios:
					
					break;
				case 13: // Salir del programa:
					System.out.println("Ha salido del programa.");
					break;
			}
			
		}while(opcion != 13);
		
		leerTeclado.close();
	}
	
	/* Opci�n 1 Leer del fichero un itinerario y cargarlo en la tabla.
	 * 
	 */
	public void leer(FicheroItinerarios ficheroIt)
	{
		//1. Lee un itinerario del fichero.
		ficheroIt.leerItinerario(ficheroIt.linea, ficheroIt.fichero, ficheroIt.ficheroBR);
		
		//2. Guarda cada variable que ha guardado el contenido de la lectura, en un itinerario:
		
		if(ficheroIt.nombreI != null)
		{
			Itinerario itinerario = new Itinerario();
			itinerario.nombreI = ficheroIt.nombreI;
			itinerario.numero = ficheroIt.numero;
			itinerario.tablaDestinos = new String[ficheroIt.numero];
			for(int c = 0; c < ficheroIt.numero; c++)
			{
				itinerario.tablaDestinos[c] = ficheroIt.tablaDestinos[c];
			}
			//3. Guarda el itinerario en la tabla.
			ficheroIt.tablaItin[ficheroIt.posicion++] = itinerario;
		}else
		{// Si la 1� variable de lectura es null, finalFichero devolver� true, indicando que el fichero ha finalizado.
			ficheroIt.finalFichero(ficheroIt.linea);
				//break;
			}
		ficheroIt.tamano = ficheroIt.posicion; //Guardamos el tama�o de la tabla.
			
	}
		
	/* Opci�n 3 Borrar itinerario de la tabla.
	 * 
	 */
	public void borrar(FicheroItinerarios ficheroIt, int posicionb)
		{
			ficheroIt.tablaItin[posicionb] = null;
		}
	
	/* Opci�n 5 Modificar itinerario de la tabla.
	 * 
	 */
	public void modificar(FicheroItinerarios ficheroIt, int posicionb, String nombreI, int numero, String tablaDestinos[])
	{
		//Creo nuevo itinerario, guardo lo que indica el usuario en cada atributo.
		Itinerario modifica = new Itinerario (nombreI, numero, tablaDestinos);
		for(int c = 0; c < modifica.tablaDestinos.length; c++)
		{
			modifica.tablaDestinos[c] = tablaDestinos[c];
		}
		//Guardo el itinerario en la posici�n del que quer�a modificar.
		ficheroIt.tablaItin[posicionb] = modifica;
		
	}
	
	/* Opci�n 6 Guardar datos de la tabla en el fichero.
	 * 
	 */
	public void guardar(FicheroItinerarios ficheroIt, File fichero, int posicion, String nombreI, int numero, String tablaDestinos[], Itinerario itinerario)
	{
		// 1. Abrir fichero para guardar (no a�ade, sobreescribe).
		BufferedWriter ficheroBW = ficheroIt.abrirFichero("itinerarios.txt", "sobrescribe", "escribe");
		try {
		    // 2. Escribir datos de la tabla al fichero.
				//Recorro la tabla de itinerarios:
		    for(int c = 0; c < ficheroIt.tamano; c++)//tamano es el n� de posiciones ocupadas de la tabla.
		    {
		    	//Si la posici�n de la tabla no es null:
		    	if(ficheroIt.tablaItin[c] != null)
		    	{
		    		//Copio el itinerario de la posici�n actual de la tabla en el objeto itinerario auxiliar.
			    	itinerario = ficheroIt.tablaItin[c];
				    nombreI = itinerario.nombreI;
				    numero = itinerario.numero;
				    for(int d = 0; d < numero; d++)
				    {
				    	tablaDestinos[d] = itinerario.tablaDestinos[d];
				    }
				    
				    //Escribo en el fichero los valores de las variables auxiliares.
				    ficheroBW.write(nombreI);
				    ficheroBW.newLine();
				    int numeroDestinos = numero;
				    //Convierto el int numero a string para poder escribirlo en el fichero.
				    String linea = Integer.toString(numero);;
				    ficheroBW.write(linea);
				    ficheroBW.newLine();
				    for(int j = 0; j < numeroDestinos; j++)
				    {
				    	ficheroBW.write(tablaDestinos[j]);
				    	ficheroBW.newLine();
				    }
				    //Creo nuevo itinerario para la siguiente iteraci�n.
				    itinerario = new Itinerario(itinerario.nombreI, itinerario.numero, itinerario.tablaDestinos);// Creo un nuevo itinerario para el siguiente, si no, machaca el primero.
		    	}
		    	
		    }
		    // 3. Cerrar el fichero
		    ficheroIt.cerrar(fichero, "escritura", null, null, ficheroBW);
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* Opci�n 8 Separar fichero, de modo que en uno tenga los itinerarios con
	 * n� de destinos menor que el n� dado por el usuario, y en otro el resto.
	 */
	private void separar(File fichero, FicheroItinerarios ficheroIt, String tablaDestinos[], int posicion, Itinerario itinerario, String nombreI, int numero, int maximo, File ficheroMaximo, File ficheroRestoDestinos)
	{
		int d;
		//1. Cargar el fichero en tablaItin:
		leerCompleto(fichero, ficheroIt, posicion, itinerario);
		try {
		    // 2. Se recorre la tabla de itinerarios:
		    for(int c = 0; c < ficheroIt.tamano; c++)//tamano es el n� de posiciones ocupadas de la tabla.
		    {
		    	//3. Si esa posici�n no es null:
		    	if(ficheroIt.tablaItin[c] != null)
		    	{
		    		//Relleno los atributos del itinerario auxiliar, con el objeto de la tabla que est� en la posici�n c.
			    	itinerario = ficheroIt.tablaItin[c];
				    nombreI = itinerario.nombreI;
				    numero = itinerario.numero;
				    for(d = 0; d < numero; d++)
				    {
				    	tablaDestinos[d] = itinerario.tablaDestinos[d];
				    }
				    //4. Si el n� de destinos del itinerario es menor o igual que el m�ximo indicado por el usuario, se escribe en otro fichero.
				    if(itinerario.numero <= maximo)
				    {
				    	//Creaci�n y apertura del fichero:
				    	crearFichero("maximoDestinos.txt");
				    	FileWriter ficheroFW = new FileWriter(ficheroMaximo, true);
					    BufferedWriter ficheroBW = new BufferedWriter(ficheroFW);
					  
					    //Escritura del itinerario en el fichero maximoDestinos.txt:
					    ficheroBW.write(nombreI);
					    ficheroBW.newLine();
					    int numeroDestinos = numero;//Guardo el n� de destinos para luego recorrer en la tabla, s�lo las posiciones ocupadas.
					    //Convierto el int numero a string para poder escribirlo en el fichero.
					    String linea = Integer.toString(numero);
					    ficheroBW.write(linea);
					    ficheroBW.newLine();
					    for(int j = 0; j < numeroDestinos; j++)
					    {
					    	ficheroBW.write(tablaDestinos[j]);
					    	ficheroBW.newLine();
					    }
					    //6. Cierro el fichero mayorDestinos.txt:
					    ficheroIt.cerrar(ficheroMaximo, "escritura", null, null, ficheroBW);    
				    } 
				    else
				    {
				    	//Si el n� de destinos del itinerario es mayor que m�ximo, lo escribimos
				    	//en el fichero restoDestinos.txt.
				    	// Creaci�n y apertura del fichero:
				    	crearFichero("restoDestinos.txt");
				    	FileWriter ficheroFW = new FileWriter(ficheroRestoDestinos, true);
					    BufferedWriter ficheroBW = new BufferedWriter(ficheroFW);
					  
					    //Escritura del itinerario en el fichero maximoDestinos.txt:
					    ficheroBW.write(nombreI);
					    ficheroBW.newLine();
					    int numeroDestinos = numero;
					    //Convierto el int numero a string para poder escribirlo en el fichero.
					    String linea = Integer.toString(numero);;
					    ficheroBW.write(linea);
					    ficheroBW.newLine();
					    for(int j = 0; j < numeroDestinos; j++)
					    {
					    	ficheroBW.write(tablaDestinos[j]);
					    	ficheroBW.newLine();
					    }
					    //5. Borro ese itinerario de la tablaItin:
					    ficheroIt.tablaItin[d] = null;
					    //6. Cierro el fichero mayorDestinos.txt:
					    ficheroIt.cerrar(ficheroRestoDestinos, "escritura", null, null, ficheroBW);    
				    } 
		
				    //Creo nuevo itinerario para recoger los datos de la siguiente posici�n de tablaItin:
				    itinerario = new Itinerario(itinerario.nombreI, itinerario.numero, itinerario.tablaDestinos);// Creo un nuevo itinerario para el siguiente, si no, machaca el primero.
		    	}
		    	
		    }
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* Opci�n 9 Mostrar contenido del fichero.
	 * 
	 */
	public void mostrar(File fichero, FicheroItinerarios ficheroIt)
	{
		//1. Abrir fichero:
		BufferedReader ficheroBR = ficheroIt.abrirFichero("itinerarios.txt", "lectura");
		
				try
				{
					//2. Lectura avanzada del fichero:
					String linea="";
					linea = ficheroBR.readLine();
					while(linea != null)
					{ 
						System.out.println(linea);
						linea = ficheroBR.readLine();
					}

					//3. Cerrar el fichero:
					ficheroIt.cerrar(fichero, "lectura", null, ficheroBR, null);
				}catch (IOException e)
				{
					System.out.println("No se puede leer.\n");
				}
	}
	
	//M�todo para mostrar el contenido de la tabla de itinerarios:
    private void mostrarTabla(FicheroItinerarios ficheroIt)
    {
    	for(int c = 0; c  < ficheroIt.tamano ; c++)
		{
    		if(ficheroIt.tablaItin[c] != null)//Para que s�lo muestre las posiciones con datos.
    			System.out.println(ficheroIt.tablaItin[c].toString());//Ver lo que se ha insertado en la tabla.
		}
    }
    
    //M�todo para vaciar la tabla:
    private void vaciarTabla(FicheroItinerarios ficheroIt)
    {
    	for(int c = 0; c  < ficheroIt.tamano ; c++)
		{
        	ficheroIt.tablaItin[c] = null;
		}

    }
    
  // M�todo privado para comprobar si existe el ficheroMaximo, que puede ser maximoDestinos.txt o restoDestinos.txt y si no, crearlo.
  	private File crearFichero(String nombreFichero)
  	{
  		File ficheroMaximo = null;
  		try {
  			ficheroMaximo = new File(nombreFichero);
  		//Comprobar si el fichero existe y si no, crearlo.
  			if(!ficheroMaximo.exists())
  			{
  				ficheroMaximo.createNewFile();
  					System.out.println("Se ha creado el fichero\n");
  			}
  			
  		}catch (Exception e) {
  			e.printStackTrace();
  		}
  		return ficheroMaximo;
  	}

  	//M�todo para leer el fichero completo:
  	public void leerCompleto(File fichero, FicheroItinerarios ficheroIt, int posicion, Itinerario itinerario)
	{
		//1. Abrir fichero:
		BufferedReader ficheroBR = ficheroIt.abrirFichero("itinerarios.txt", "lectura");
		
		try
		{
		//2. Leer y cargar datos del fichero en la tabla:
		String linea="";
		int entero;
		while((linea = ficheroBR.readLine()) != null)
		{ 
			itinerario.nombreI = linea;
			linea = ficheroBR.readLine();
			itinerario.numero = entero = (Integer.parseInt(linea));
			itinerario.tablaDestinos = new String[entero];
			for(int c = 0; c < entero; c++)
			{
				itinerario.tablaDestinos[c] = linea = ficheroBR.readLine();
			}
			ficheroIt.tablaItin[posicion++] = itinerario;
			itinerario = new Itinerario(itinerario.nombreI, itinerario.numero, itinerario.tablaDestinos);// Creo un nuevo itinerario para el siguiente, si no, machaca el primero.
		}
		ficheroIt.tamano = posicion; //Guardamos el tama�o de la tabla.
		//3. Cerrar el fichero.
		ficheroIt.cerrar(fichero, "lectura", null, ficheroBR, null);
		}catch (IOException e)
		{
			System.out.println("No se puede leer.\n");
		}
		
	}
}

