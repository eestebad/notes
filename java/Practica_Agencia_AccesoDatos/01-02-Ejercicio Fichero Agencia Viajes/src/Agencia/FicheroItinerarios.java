package Agencia;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FicheroItinerarios {
	protected Itinerario tablaItin[];
	protected int tamano = 0;
	protected int posicion;
	protected int agrega;
	protected File fichero;
	FileReader ficheroFR = null;
	BufferedReader ficheroBR = null;
	protected String linea="";
	protected String nombreI;
	protected int numero;
	protected String[] tablaDestinos;
	
	public FicheroItinerarios()
	{
		tablaItin = new Itinerario[20];
		tamano = 0;
		fichero = new File("itinerarios.txt");
		try {
			ficheroFR = new FileReader(fichero);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ficheroBR = new BufferedReader(ficheroFR);
		this.tablaDestinos = new String[3];
	}
	
	
	/* 1- Abrir fichero: m�todo sobrecargado, que comprueba si el fichero existe y si no, lo crea (primer m�todo). Si 
	 * existe lo abre para lectura, agregar o sobrescribirlo seg�n los par�metros que se indiquen.
	 * */
	//1.a. S�lo comprueba si existe el fichero y si no, lo crea.
	public File abrirFichero(String nombreFichero)
	{
		File fichero = null;
		try {
			fichero = new File(nombreFichero);
		//Comprobar si el fichero existe y si no, crearlo.
			if(!fichero.exists())
			{
				fichero.createNewFile();
					System.out.println("Se ha creado el fichero\n");
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return fichero;
	}
	
	//1.b. Abrir fichero en lectura:
	public BufferedReader abrirFichero(String nombreFichero, String metodoLectura)
	{
		FileReader ficherorR = null;
		BufferedReader ficheroBR = null;
		
		try {
			File fichero = new File(nombreFichero);
		// 1. Comprobar si el fichero existe:
			if(!fichero.exists()) //Si no existe, crearlo:
			{
				fichero.createNewFile();
					System.out.println("Se ha creado el fichero\n");
			}else
			{ // 2. Si ya existe, abrirlo en lectura:
				if(metodoLectura == "lectura")
				{
					ficherorR = new FileReader(fichero);
					ficheroBR = new BufferedReader(ficherorR);
				}
				// No cerramos el fichero porque vamos a operar sobre �l.
				// Se cerrar� en el m�todo que corresponda, con el m�todo cerrar.
			}
			}catch (Exception e2) {
				e2.printStackTrace();
			}
		return ficheroBR;
	}
	
	//1.c. Abrir fichero en escritura (agrega).
	public BufferedWriter abrirFichero(String nombreFichero, String metodoAgrega, int agrega)
	{
		FileWriter ficheroFW = null;
		BufferedWriter ficheroBW = null;
		
		try {
			File fichero = new File(nombreFichero);
		// 1. Comprobar si el fichero existe:
			if(!fichero.exists()) //Si no existe, crearlo:
			{
				fichero.createNewFile();
					System.out.println("Se ha creado el fichero\n");
			}else
			{ // 2. Si ya existe, abrirlo:
				if(metodoAgrega == "agrega")
				{
					ficheroFW = new FileWriter(fichero, true);//true para que a�ada.
					ficheroBW = new BufferedWriter(ficheroFW);
				}
				// No cerramos el fichero porque vamos a operar sobre �l.
				// Se cerrar� en el m�todo que corresponda, con el m�todo cerrar.
			}
			}catch (Exception e) {
				e.printStackTrace();
			}
		return ficheroBW;
	}
	
	//1.d. Abrir fichero en escritura (sobrescribe). Se utiliza en el m�todo del main Guardar.
		public BufferedWriter abrirFichero(String nombreFichero, String metodoSobrescribe, String escribe)
		{
			FileWriter ficheroFW = null;
			BufferedWriter ficheroBW = null;
			
			try {
				File fichero = new File(nombreFichero);
			// 1. Comprobar si el fichero existe:
				if(!fichero.exists()) //Si no existe, crearlo:
				{
					fichero.createNewFile();
						System.out.println("Se ha creado el fichero\n");
				}else
				{ // 2. Si ya existe, abrirlo:
						ficheroFW = new FileWriter(fichero);
						ficheroBW = new BufferedWriter(ficheroFW);
					}
					// No cerramos el fichero porque vamos a operar sobre �l.
					// Se cerrar� en el m�todo que corresponda, con el m�todo cerrar.
				}catch (Exception e) {
					e.printStackTrace();
				}
			return ficheroBW;
		}
		
	
	/* 2- Leer itinerario: lee del fichero un itinerario y guarda cada l�nea en un atributo auxiliar.
	 */
	public File leerItinerario(String linea, File fichero, BufferedReader ficheroBR)
	{	
		try
		{	
			int entero;
			if((linea = ficheroBR.readLine()) != null)
			{ 
				nombreI = linea;
				linea = ficheroBR.readLine();
				numero = entero = (Integer.parseInt(linea));
				for(int c = 0; c < entero; c++)
				{
					tablaDestinos[c] = linea = ficheroBR.readLine();
				}
				
			}		
			if(linea == null) // Si la l�nea es null ejecuta finalFichero y cierra.
			{	finalFichero(linea);
				nombreI = null;
				cerrar(fichero, "lectura", null, ficheroBR, null);
			}
		
		}catch (IOException e)
		{
			System.out.println("El fichero ha finalizado.\n");
		}
		return fichero;
	}
	
	
	/* 3- Escribir en el fichero un itinerario.
	 */
	public void escribir( File fichero, String nombreI, int numero, String tablaDestinos[])
	{
		// 1. Crear el fichero de escritura.
		BufferedWriter ficheroBW = abrirFichero("itinerarios.txt", "agrega", agrega);
		try {
		    // 2. Escribir datos en el fichero
			int numeroDestinos = numero;
		    ficheroBW.write(nombreI);
		    ficheroBW.newLine();	
		  //Convierto el int numero a string para poder escribirlo en el fichero.
		    String linea = Integer.toString(numero);;
		    ficheroBW.write(linea);
		    ficheroBW.newLine();
		    for(int c = 0; c < numeroDestinos; c++)
		    {
			    ficheroBW.write(tablaDestinos[c]);
			    ficheroBW.newLine();
		    }
		    //3. Cerrar fichero.
		    cerrar(fichero, "escritura", null, null, ficheroBW);
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*4- Cerrar el fichero.
	*/
	public void cerrar(File fichero, String nombre, FileReader ficheroFR, BufferedReader ficheroBR, BufferedWriter ficheroBW)
	{
		if(nombre == "lectura")
		{
			try {	
				if (ficheroBR != null)
					ficheroBR.close();
			} catch (IOException e) {
				System.out.println("El fichero no est� abierto.\n");
			}
		}
		if(nombre == "escritura")
		{
			try {	
				if (ficheroBW != null)
					ficheroBW.close();

			} catch (IOException e) {
				System.out.println("El fichero no est� abierto.\n");
			}
		}
	}
	
	/*5. Final: m�todo que devuelva verdadero cuando se haya alcanzado el final
	 *  del fichero y si no, falso.
	 */
	public boolean finalFichero(String linea)
	{
		if(linea != "")
			return false;
		else
			return true;
		
	}
	
	/*6. Vaciar: m�todo para vaciar el fichero (Opci�n 4):
	 */
	public void vaciar(File fichero)
	{
		try {// 1. Crear fichero de escritura:
			FileWriter ficheroFW = new FileWriter(fichero);
			BufferedWriter ficheroBW = new BufferedWriter(ficheroFW);
		    // 2. Escribir una l�nea vac�a en el fichero:
		    ficheroBW.write("");
		    //3. Cerrarlo:
		}catch(IOException e) {
			e.printStackTrace();
		}
	}	
}
