package Agencia;

import java.util.Scanner;

public class MenuAgencia {
	
public int opcion;
	
	public MenuAgencia() {

	}
	
	public int menu()
	{
		System.out.println("MEN� AGENCIA VIAJES\n");
		
		Scanner leerTeclado = new Scanner(System.in);
		int opcion;
		
		System.out.println(" 1. Leer fichero."
				+ "\n 2. Insertar itinerario."
				+ "\n 3. Borrar itinerario."
				+ "\n 4. Vaciar itinerarios."
				+ "\n 5. Modificar itinerario. "
				+ "\n 6. Guardar datos de la tabla al fichero. "
				+ "\n 7. Mostrar contenido tabla. "
				+ "\n 8. Separar fichero. "
				+ "\n 9. Mostrar contenido fichero. "
				+ "\n 10. Mostrar itinerarios de menor a mayor."
				+ "\n 11. Mostrar itinerarios de mayor a menor."
				+ "\n 12. Mostrar el destino m�s repetido de todos los itinerarios."
				+ "\n 13. Salir.");
		
		opcion = leerTeclado.nextInt();
		leerTeclado.nextLine();
		
		return opcion;		
	}

}

