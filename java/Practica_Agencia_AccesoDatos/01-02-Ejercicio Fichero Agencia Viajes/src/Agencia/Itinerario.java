package Agencia;

import java.io.File;

/*Clase para crear cada itinerario*/

public class Itinerario {
	protected String nombreI;
	protected int numero;
	protected String[] tablaDestinos;

	public Itinerario()
	{
	}

	public Itinerario(String nombreI, int numero, String[] tablaDestinos) {
		this.nombreI = nombreI;
		this.numero = numero;
		this.tablaDestinos = new String[3];
		
	}

	//M�todo para mostrar los datos de Itinerario:
    public String toString()
    {
    	String destinosAuxiliar = ""; //Inicializo string para ir guardando cada destino de la tablaDestinos.
    	for(int c = 0; c < tablaDestinos.length; c++)
    	{
    		//Guardo en el string cada destino separados por comas.
    		destinosAuxiliar = destinosAuxiliar + ", " + tablaDestinos[c];
    	}
        return	
        nombreI+" n� destinos: "+
        numero+" tabla de destinos: "+
        destinosAuxiliar + "\n";
    }
	

	

}