package PracticasOctubre;

public class Numero {
	protected int numero; //En el UML aparece como private, pero se pone protected para que sea visible para la clase TablaNumeros.
	
	//Constructor:
	public Numero(int numero) {
		this.numero = numero;
	}
	//M�todos getter/getValor y setter:
	public int getValor() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	//M�todo toString/imprimir:
	public String imprimir() {
		return "Numero [numero=" + numero + "]";
	}

	public String toString() {
		return "Numero [numero=" + numero + "]";
	}
}
