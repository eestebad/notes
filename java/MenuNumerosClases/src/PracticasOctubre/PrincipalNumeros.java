package PracticasOctubre;

import java.util.Scanner;
import java.util.InputMismatchException;

/* AccesoDatos.
 * Programa que lea n�meros enteros del teclado y presente un men�. 
 * �Qu� quieres hacer?
 * 1.	Meter n�.
 * 2.	Escribir los n�meros por pantalla.
 * 3.	Escribir el n� m�s alto.
 * 4.	Escribir el n� m�s bajo.
 * 5.	Borrar n�mero.
 * 6.	Salir.
 * Incluyo una opci�n m�s para imprimir a la vez el n� m�s alto y el m�s bajo, que podr�a sustituir 
 * a las opciones 3 y 4.
 *  @author Elena Esteban D�ez.
 *  @return void por pantalla.
 */

public class PrincipalNumeros {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner leerTeclado = new Scanner(System.in);
		int opcion;
		int numero = 0, posicion = 0, longitud = 5;
		int numeros[] = new int[longitud];
		TablaNumeros tablaUno = new TablaNumeros(numero, numeros, posicion, longitud);

		
		System.out.println("*** �Qu� desea hacer? ***\n"); 
		System.out.println("Men�: \n 1 -> Meter n�mero. \n 2 -> Escribir los n�meros por pantalla.\n "
				+ "3 -> Escribir el n� m�s alto. \n 4 -> Escribir el n� m�s bajo. \n 5 -> Escribir el n� m�s bajo y el m�s alto.\n"
				+ " 6 -> Borrar n�mero. \n"
				+ " 7 -> Salir. \n");
		
		opcion = leerTeclado.nextInt();
		boolean exit = false;
				
		try
		{
			do {
				//Men� principal
				switch(opcion) {
				case 1: 
					//Guarda los 5 n�meros dados por teclado, en un array:
					tablaUno.insertarNumero(leerTeclado);
					break;
					
				case 2:
					//Imprime los n�meros:
					tablaUno.imprimir(numeros);
					break;
					
				case 3:
					// Muestra el n�mero mayor:
					tablaUno.mayor(numeros);
					break;
					
				case 4:
					// Muestra el n�mero menor:
					tablaUno.menor(numeros);
					break;
				
				case 5:
					// Muestra ambos:
					tablaUno.menorMayor(numeros);
					break;
					
				case 6:
					// Borra un n�:
					tablaUno.borrar(numeros);
					break;
					
				case 7:
					System.out.println("Ha salido del programa.\n");
					exit = true;
					break;
				default:
					System.out.println("Introduzca una de las opciones del men�: ");
					
				}
				if(!exit) { // Si el programa devuelve true, es porque se ha ejecutado y muestra de nuevo el men�
					//al usuario para que siga introduciendo datos, antes de salir del do while.
					System.out.println("Men�: \n 1 -> Meter n�mero. \n 2 -> Escribir los n�meros por pantalla.\n "
							+ "3 -> Escribir el n� m�s alto. \n 4 -> Escribir el n� m�s bajo. \n 5 -> Escribir el n� m�s bajo y el m�s alto.\n"
							+ " 6 -> Borrar n�mero. \n"
							+ " 7 -> Salir. \n");
					opcion = leerTeclado.nextInt();
				}
			}while(exit==false); // Si el programa devuelve false, es porque el usuario ha elegido Salir.
			}catch(InputMismatchException e) { // Si en alguno de los campos num�ricos se introduce un tipo de dato distinto, da error.
		}
		leerTeclado.close();

	}

}
