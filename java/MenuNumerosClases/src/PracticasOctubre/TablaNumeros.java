package PracticasOctubre;

import java.util.Arrays;
import java.util.Scanner;

public class TablaNumeros extends Numero{
	private static int numero;
	private int tabla[];
	private int posicion;
	protected int longitud;

	//Constructor:
	public TablaNumeros(int numero, int[] tabla, int posicion, int longitud) {
		super(numero);
		this.tabla = tabla;
		this.posicion = posicion; 
		this.longitud = longitud;
	}
	
	public TablaNumeros(int tabla[]) {
		super(numero);
		this.tabla = tabla;
	}
	
	//M�todo toString:
	@Override
	public String toString() {
		return "TablaNumeros [tabla=" + Arrays.toString(tabla) + ", posicion=" + posicion + ", longitud=" + longitud
				+ "]";
	}

	//M�todos getter y setter (incluye el m�todo getPosicion):
	public int[] getTabla() {
		return tabla;
	}
	public void setTabla(int[] tabla) {
		this.tabla = tabla;
	}
	public int getPosicion() {
		return posicion;
	}
	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}
	public int getLongitud() {
		return longitud;
	}
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	
	// M�todos borrar, imprimir, insertar n�, mayor, n� menor y ambos:
	// Borrar un n� de la tabla:
	public void borrar(int tabla[]) {
		int posicion = 0;
		tabla[4] = posicion;
		posicion--;
		System.out.println("Se ha borrado un n�mero.\n");
	}
	// Imprimir todos los n�meros de la tabla:
	public void imprimir(int tabla[]) {
		for(int c = 0; c < tabla.length; c++)
			System.out.println(tabla[c] + "\n");
	}
	// Guardar en la tabla los n�meros facilitados por teclado:
	public void insertarNumero(Scanner leerTeclado) {
		leerTeclado.nextLine(); //Vac�o la variable.

		System.out.println("Introduzca 5 n�meros pulsando intro cada vez: ");
		
		for(int i = 0; i < tabla.length; i++)
			tabla[i] = leerTeclado.nextInt();
		
		System.out.println("\n");
	}
	// Mostrar el n� mayor:
	public void mayor(int tabla[]) {
		System.out.println("\n El n� m�s alto: \n");
		// Usamos ordenaci�n de burbuja para obtener el n� m�s alto.
		int mayor = 0;
		for(int i = 0; i < tabla.length - 1; i++) {
			for(int j = 0; j < tabla.length - 1; j++)
				if(tabla[j] < tabla[j+1])
				{
					mayor = tabla[j+1];
					tabla[j+1] = tabla[j];
					tabla[j] = mayor;
				}
		}
		System.out.println(mayor + "\n");
	}
	// Mostrar el n� menor:
	public void menor(int tabla[]) {
		System.out.println("\n El n� m�s bajo: \n");
		// Usamos ordenaci�n de burbuja para obtener el n� m�s bajo.
		int menor = 1;
		for(int i = 0; i < tabla.length - 1; i++) {
			for(int j = 0; j < tabla.length - 1; j++)
				if(tabla[j] > tabla[j+1])
				{
					menor = tabla[j+1];
					tabla[j+1] = tabla[j];
					tabla[j] = menor;
				}
		}
		System.out.println(menor+ "\n");	
	}
	// Mostrar tanto el menor como el mayor:
	public void menorMayor(int tabla[]) {
		System.out.println("\n El n� m�s bajo y el m�s alto: \n");
		// Usamos ordenaci�n de burbuja para obtener el n� m�s alto y el m�s bajo.
				int mayor = 0;
				for(int i = 0; i < tabla.length - 1; i++) {
					for(int j = 0; j < tabla.length - 1; j++)
						if(tabla[j] < tabla[j+1])
						{
							mayor = tabla[j+1];
							tabla[j+1] = tabla[j];
							tabla[j] = mayor;
						}
				}
				System.out.println("El n� m�s bajo es: " +tabla[4]+ "\n");
				System.out.println("El n� m�s alto es: " +mayor+ "\n");
	}
	
}