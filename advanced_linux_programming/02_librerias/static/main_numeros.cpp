/*
 * =====================================================================================
 *
 *       Filename:  main_numeros.cpp
 *
 *    Description: Create a static library. Main. 
 *
 *        Version:  1.0
 *        Created:  17/10/20 18:24:31
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "numeros.h"

#define N 6

int main (int args, char* argv[])
{
    int s1 = 4;
    int s2 = 6;

    printf("El factorial de %i es: %i.\n", N, factorial(N));
    printf("%i + %i = %i\n", s1, s2, suma(s1, s2));

    return EXIT_SUCCESS;
}
