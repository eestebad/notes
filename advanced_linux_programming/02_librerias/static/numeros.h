/*
 * =====================================================================================
 *
 *       Filename:  numeros.h
 *
 *    Description: Create a static library. File .h with the functions proto * type. 
 *
 *        Version:  1.0
 *        Created:  17/10/20 17:51:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#ifndef __NUMEROS_H__
#define __NUMEROS_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif
    int factorial(int);
    int suma (int, int);

#ifdef __cplusplus
}
#endif

#endif
