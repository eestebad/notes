/*
 * =====================================================================================
 *
 *       Filename:  factorial.cpp
 *
 *    Description: Create a static library. 
 *
 *        Version:  1.0
 *        Created:  17/10/20 17:15:31
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */


/* Función para calcular el factorial de un número recursivamente*/
#include "numeros.h"

int factorial (int n){
    if (n <= 0)
        return 1;
    return n * factorial (n-1);
}
