/*
 * =====================================================================================
 *
 *       Filename:  06_fork_exec.cpp
 *
 *    Description: Using fork and exec (Advanced Linux Programming). 
 *
 *        Version:  1.0
 *        Created:  15/10/20 10:12:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

/* Se genera un proceso hijo ejecutando un nuevo programa. ARG_LIST es una lista de caracteres string terminada en NULL, para ser pasada como lista de argumentos del programa. Devuelve el ID del proceso generado.*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int spawn (char* program, char** arg_list)
{
    pid_t child_pid;

    child_pid = fork(); //Duplicamos el proceso.
    if (child_pid != 0)
        return child_pid; // Si el proceso devuelve algo distinto de cero, es el padre.
    else {
        execvp (program, arg_list); // Ejecuta program, buscándolo en la ruta.
        fprintf (stderr, "an error occurred in execvp\n"); //execvp sólo tiene valor de retorno si se da error.
        abort();
    }
}

int main ()
{
    //La lista de argumentos para pasar al comando ls:
    char* arg_list[] = {
        "ls", //El nombre del programa argv[0]
        "-l",
        "/",
        NULL // La lista debe terminar en NULL.
    };
    // Se genera un proceso hijo ejecutando el comando ls. Ignora el ID devuelto por el hijo.
    spawn ("ls", arg_list);

    printf ("done with main program\n");

    return 0;
}
