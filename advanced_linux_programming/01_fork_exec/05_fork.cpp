/*
 * =====================================================================================
 *
 *       Filename:  05_for.cpp
 *
 *    Description: Using fork to duplicate a program's process (Advanced Lin *    ux Programming). 
 *
 *        Version:  1.0
 *        Created:  14/10/20 20:03:30
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    pid_t child_pid;// para referirnos al id de proceso, utilizamos pid_t(sys/types.h).

    printf("The main program process ID is %d\n", (int) getpid());

    child_pid = fork ();
    if (child_pid != 0) {
        printf("This is the parent process, with ID %d\n", (int) getpid());
        printf("The child's process ID is %d\n", (int) child_pid);
    }
    else
        printf("This is the child process, with ID %d\n", (int) getpid());

    return 0;
}

