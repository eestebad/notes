/*
 * =====================================================================================
 *
 *       Filename:  signal.cpp
 *
 *    Description: Using a signal handler. 
 *
 *        Version:  1.0
 *        Created:  27/10/20 19:26:25
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

/*Programa que mande señal y cuando termine, te diga cuántas ha recibido. Se crea un proceso infinito, que se detiene lanzando desde la terminal kill SIGUSR1 nºproceso. */

sig_atomic_t sigusr1_count = 0; //Variable global atómica, que no puede ser interrumpida.

void handler (int signal_number) //Función handler que va a incrementar sigusr1_count cada vez que reciba una señal.
{
    ++sigusr1_count;
}

int main ()
{
    struct sigaction sa; //Declaración del struct de tipo sigaction.
    memset (&sa, 0, sizeof (sa)); //Recibe sa por referencia y llena de ceros todo su tamaño.
    sa.sa_handler = &handler; //Guardar la función dentro del campo sa_handler del struct sa. De esta forma, podemos usar esta función con cualquier struct.
    printf("My pid is: %d\n", getpid());

/*    if (sigaction(SIGUSR1, &sa, NULL) < 0) {
        return 1;
    }*/

    while(sigusr1_count < 8)
    {
        handler(SIGUSR1);// simula que envía una señal, pero no lo hace.
        printf("This is a finite process.\n");
        usleep(80000);
    };


    printf ("SIGUSR1 se ha llamado %d veces.\n", sigusr1_count);
    return EXIT_SUCCESS;
}

