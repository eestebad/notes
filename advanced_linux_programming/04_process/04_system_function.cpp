/*
 * =====================================================================================
 *
 *       Filename:  04_system_function.cpp
 *
 *    Description: Example of the use of system function (Advanced Linux Pro *    gramming) 
 *
 *        Version:  1.0
 *        Created:  14/10/20 19:15:33
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <stdlib.h>

int main ()
{
    int return_value;
    return_value = system ("ls -l /");
    return return_value;
}
