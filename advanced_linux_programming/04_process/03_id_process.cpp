/*
 * =====================================================================================
 *
 *       Filename:  03_id_process.cpp
 *
 *    Description: Exercise to print the id process and the id parent proces *    s (Advanced Linux Programming). 
 *
 *        Version:  1.0
 *        Created:  14/10/20 18:07:24
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <unistd.h>

int main() {
    printf ("The process ID is %d\n", getpid ());
    printf ("The parent process ID is %d\n", getppid ());
    return 0;
}
