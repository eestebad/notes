/*
 * =====================================================================================
 *
 *       Filename:  thread_create.cpp
 *
 *    Description: Create a new thread (Advanced Linux Programming). 
 *
 *        Version:  1.0
 *        Created:  20/07/20 19:11:50
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <pthread.h>
#include <stdio.h>

void *print_xs (void *unused)
{
    while (1)
        fputc ('x', stderr);
    return NULL;
}

int main ()
{
    pthread_t thread_id;

    pthread_create (&thread_id, NULL, &print_xs, NULL);

    while (1)
        fputc ('o', stderr);
    return 0;
}

