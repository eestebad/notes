/*
 * =====================================================================================
 *
 *       Filename:  union_test.cpp
 *
 *    Description: union test by OpenWebinars. 
 *
 *        Version:  1.0
 *        Created:  21/07/20 19:00:48
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

void struct_example()
{
    MyFiS s;
    s.m_value = 10;
    s.m_repr = 's';
    std::cout << "MyFiS value" << std::endl;
    std::cout << s.m_value << std::endl;
    std::cout << s.m_repr << std::endl;
}

union Vector2D
{
    struct {float x, y};
    float vector[2];
/*Constructores: uno coge dos float y el otro un array*/
    Vector(const float &_x, const float &_y) : x(_x), y(_y){}
    Vector(const float vars[2])
    {
        vector[0] = vars[0];
        vector[1] = vars[1];
    }
};

void union_example()
{
    struct TestStruct
    {
        float x, y;
        float vector[2];
    };
    // To see the size of the struct and the union:
    std::cout << "TestStruct size: "<< sizeof(TestStruct) <<std::endl;
    std::cout << "Vector2D size: "<< sizeof(Vector2D) <<std::endl;
    // Creation of the unions:
    Vector2D u1(10, 10); // build by x, y constructor.
    Vector2D u2(50, 50); // build by array constructor.
    // Print:
    std::cout << "Union access by x, y constructor: " << u1.x << ", " << u1.y << std::endl;
    std::cout << "Union access by array constructor: " << u1.vector[0] << ", " << u1.vector[1] << std::endl;
    // Modificamos el valor de u1 utilizando el acceso por array, para comprobar que funciona. Lo igualamos con u2:
    u1.vector[0] = u2.vector[0];
    u1.vector[1] = u2.vector[1];
    // Print u1 with news values:
    std::cout << "News values of u1: " << u1.vector[0] << ", " << u1.vector[1] << std::endl;
}

void all_structures()
{
    std::cout << "Struct example: " << std::endl;
    struct _example();
    std::cout << "Union example: " << std::endl;
    union_example();
}

int main(int arg, const char *argv[])
{
    all_structures();
    return EXIT_SUCCESS:
}
