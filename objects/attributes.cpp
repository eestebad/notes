/*
 * =====================================================================================
 *
 *       Filename:  attributes.cpp
 *
 *    Description:  Attributes in OOP.
 *
 *        Version:  1.0
 *        Created:  21/07/20 20:14:34
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string>

class Person
{
    public:
        int dni;
        std::string name;
        std::string direction;

    private:
        unsigned age;
};

int main()
{
    Person p;
    p.dni = 000000;
    p.name = "Elena";
    p.direction = "Calle Mayor";
    p.age = 45; // When you compile, give you an error message because is a private attribute.
}
