/*
 * =====================================================================================
 *
 *       Filename:  memory_class.cpp
 *
 *    Description: Class in stack or heap. 
 *
 *        Version:  1.0
 *        Created:  21/07/20 20:04:58
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  eleste (), eestedma@gmail.com
 *   Organization:  eleste
 *
 * =====================================================================================
 */

class MyClass
{

};

int main()
{
    MyClass a; //In the stack. Dynamic memory.
    MyClass *b = new MyClass // In the heap memory.

        delete b; //Free heap memory.
}

