function Jugar() /* Función que hace que las imágenes cambien a la vez y aleatoriamente, sumando
la puntuación obtenida a Tirada actual */
{
	var imagen,
		numero,
		numero1,
		numero2,
		tirada,
		i

	tirada = parseInt(document.getElementById('tirada').innerHTML) /* Accede a Tirada actual
	y lo modifica a entero, deja de ser párrafo */
	imagen = document.getElementsByClassName('Imagen') /* Accede a la clase Imagen, que recoge
	las tres imágenes */

	for(i=0;i<imagen.length;i++) //Guarda las imágenes en el array imagen.
	{
		imagen[i]
	}

	numero = Math.round(Math.random() * (3 - 1) + 1); // Calcula un nº aleatorio entre 1 y 3.
	// Según el nº aleatorio, pondrá una imagen u otra.
	if (numero == 1)
		imagen[0].src = "imagenes/logoCSS.jpeg" //Accedo a la propiedad src del objeto imagen
	if (numero == 2)
		imagen[0].src = "imagenes/logoJs.png"
	if (numero == 3)
		imagen[0].src = "imagenes/logoHtml.jpeg"

	numero1 = Math.round(Math.random() * (3 - 1) + 1); // Calcula un nº aleatorio entre 1 y 3.
	// Según el nº aleatorio, pondrá una imagen u otra.
	if (numero1 == 1)
		imagen[1].src = "imagenes/logoCSS.jpeg" //Accedo a la propiedad src del objeto imagen
	if (numero1 == 2)
		imagen[1].src = "imagenes/logoJs.png"
	if (numero1 == 3)
		imagen[1].src = "imagenes/logoHtml.jpeg"

	numero2 = Math.round(Math.random() * (3 - 1) + 1); // Calcula un nº aleatorio entre 1 y 3.
	// Según el nº aleatorio, pondrá una imagen u otra.
	if (numero2 == 1)
		imagen[2].src = "imagenes/logoCSS.jpeg" //Accedo a la propiedad src del objeto imagen
	if (numero2 == 2)
		imagen[2].src = "imagenes/logoJs.png"
	if (numero2 == 3)
		imagen[2].src = "imagenes/logoHtml.jpeg"

	// Compara los números para calcular qué puntuación se ha obtenido y sumarla a Tirada actual.
	if ((numero == numero1) && (numero == numero2))
		document.getElementById('tirada').innerHTML = 1000
	else if (((numero == numero1) && (numero != numero2)) || 
			((numero1 == numero2) && (numero1 != numero)) ||
		 	((numero == numero2) && (numero != numero1)))
			document.getElementById('tirada').innerHTML = 500
		else
			document.getElementById('tirada').innerHTML = 0
}

function Cambiar1() //Función para cambiar la imagen 1 aleatoriamente.
{	
	var imagen,
		numero,
		tirada,
		i

	tirada = parseInt(document.getElementById('tirada').innerHTML)/* Accede a Tirada actual
	y lo modifica a entero */
	imagen = document.getElementsByClassName('Imagen')/* Accede a la clase Imagen, que recoge
	las tres imágenes */

	for(i=0;i<imagen.length;i++) //Guarda las imágenes en el array imagen.
	{
		imagen[i]
	}

	numero = Math.round(Math.random() * (3 - 1) + 1);
	// Según el nº aleatorio, cambiará la imagen 0 a una y otra.
	if (numero == 1)
		imagen[0].src = "imagenes/logoCSS.jpeg" //Accedo a la propiedad src del objeto imagen
	if (numero == 2)
		imagen[0].src = "imagenes/logoJs.png"
	if (numero == 3)
		imagen[0].src = "imagenes/logoHtml.jpeg"

	// Compara las imágenes para calcular qué puntuación se ha obtenido y sumarla a Tirada actual.
	if ((imagen[0].src == imagen[1].src) && (imagen[0].src == imagen[2].src))
		document.getElementById('tirada').innerHTML = 1000
	else if (((imagen[0].src == imagen[1].src) && (imagen[0].src != imagen[2].src)) || 
			((imagen[1].src == imagen[2].src) && (imagen[1].src != imagen[0].src)) ||
		 	((imagen[0].src == imagen[2].src) && (imagen[0].src != imagen[1].src)))
			document.getElementById('tirada').innerHTML = 500
		else
			document.getElementById('tirada').innerHTML = 0
}

function Cambiar2() //Función para cambiar la imagen 2 aleatoriamente.
{
	var imagen,
		numero,
		tirada,
		i

	tirada = parseInt(document.getElementById('tirada').innerHTML)/* Accede a Tirada actual
	y lo modifica a entero */
	imagen = document.getElementsByClassName('Imagen') /* Accede a la clase Imagen, que recoge
	las tres imágenes */

	for(i=0;i<imagen.length;i++) //Guarda las imágenes en el array imagen.
	{
		imagen[i]
	}

	numero = Math.round(Math.random() * (3 - 1) + 1);
	// Según el nº aleatorio, cambiará la imagen 1 a una y otra.
	if (numero == 1)
		imagen[1].src = "imagenes/logoCSS.jpeg" //Accedo a la propiedad src del objeto imagen
	if (numero == 2)
		imagen[1].src = "imagenes/logoJs.png"
	if (numero == 3)
		imagen[1].src = "imagenes/logoHtml.jpeg"

	// Compara las imágenes para calcular qué puntuación se ha obtenido y sumarla a Tirada actual.
	if ((imagen[0].src == imagen[1].src) && (imagen[0].src == imagen[2].src))
		document.getElementById('tirada').innerHTML = 1000
	else if (((imagen[0].src == imagen[1].src) && (imagen[0].src != imagen[2].src)) || 
			((imagen[1].src == imagen[2].src) && (imagen[1].src != imagen[0].src)) ||
		 	((imagen[0].src == imagen[2].src) && (imagen[0].src != imagen[1].src)))
			document.getElementById('tirada').innerHTML = 500
		else
			document.getElementById('tirada').innerHTML = 0
}

function Cambiar3() //Función para cambiar la imagen 3 aleatoriamente.
{
	var imagen,
		numero,
		tirada,
		i

	tirada = parseInt(document.getElementById('tirada').innerHTML) /* Accede a Tirada actual
	y lo modifica a entero */
	imagen = document.getElementsByClassName('Imagen') /* Accede a la clase Imagen, que recoge
	las tres imágenes */

	for(i=0;i<imagen.length;i++) //Guarda las imágenes en el array imagen.
	{
		imagen[i]
	}

	numero = Math.round(Math.random() * (3 - 1) + 1);
	// Según el nº aleatorio, cambiará la imagen 2 a una y otra.
	if (numero == 1)
		imagen[2].src = "imagenes/logoCSS.jpeg" //Accedo a la propiedad src del objeto imagen
	if (numero == 2)
		imagen[2].src = "imagenes/logoJs.png"
	if (numero == 3)
		imagen[2].src = "imagenes/logoHtml.jpeg"

	// Compara las imágenes para calcular qué puntuación se ha obtenido y sumarla a Tirada actual.
	if ((imagen[0].src == imagen[1].src) && (imagen[0].src == imagen[2].src))
		document.getElementById('tirada').innerHTML = 1000
	else if (((imagen[0].src == imagen[1].src) && (imagen[0].src != imagen[2].src)) || 
			((imagen[1].src == imagen[2].src) && (imagen[1].src != imagen[0].src)) ||
		 	((imagen[0].src == imagen[2].src) && (imagen[0].src != imagen[1].src)))
			document.getElementById('tirada').innerHTML = 500
		else
			document.getElementById('tirada').innerHTML = 0
}

function Reiniciar() // Función para poner los marcadores a 0.
{	
	var reinicio,
		i
	// Guardo la clase en reinicio para poner los marcadores a 0:
	reinicio = document.getElementsByClassName('Marcador')
	for(i=0;i<reinicio.length;i++)
	{
		reinicio[i].innerHTML = 0
	}

}

function GuardarTirada() // Función para sumar la Tirada al Marcador.
{
	var guarda,
		tirada
	// Acceder a tirada actual y marcador:
	guarda = parseInt(document.getElementById('acumula').innerHTML)
	tirada = parseInt(document.getElementById('tirada').innerHTML)
	// 
	document.getElementById('acumula').innerHTML = guarda + tirada /* guarda.innerHTML += tirada
	no funciona, porque cuando accedo a 'acumula' con parseInt, guarda deja de ser un objeto párrafo
	y se convierte en un entero.*/
}