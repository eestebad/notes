#!/bin/bash
if [[ $# -ne 3 ]]; then
    echo "El nº de parámetros es incorrecto"
    echo "Nº de parámetros a introducir para $0 es: <par1> <par2> <par3>"
else
    echo "Nº de parámetros correcto"
fi
