#!/bin/bash
#Use of IFS as argument separator with $*
echo "el script $0 recibe parámetros"

#show the arguments separated by spaces
echo "Los parámetros recibidos son $@"

#show the arguments separated by commas
IFS=','
echo "Los parámetros recibidos son $*"
