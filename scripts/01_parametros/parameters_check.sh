#!/bin/bash
#This script check the number of parameters given for the user is correct, and if not, it displays an error message
#The condition is: number of parameters are three
if [[ $# -ne 3 ]]; then
    #If parameters != 3 display this message and indicates the correct form
    echo "El nº de parámetros es incorrecto"
    echo "Nº de parámetros a introducir en $0 es: <par1> <par2> <par3>"
    #If parameters = 3 display this message
else
    echo "Número de parámetros correcto"
    #End of script
fi
