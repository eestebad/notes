#!/bin/bash
# Script de ejemplo definición y llamada funciones en bash

# Defino las funciones para imprimir Hola y para salir del script
salir () {
    exit
}

imprime () {
    echo "Hola"
}

# Llamo a las funciones
imprime
echo "Fin del script"
salir

