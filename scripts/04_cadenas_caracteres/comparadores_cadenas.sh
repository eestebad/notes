#!/bin/bash

if [[ $# -ne 2 ]];then
    echo "Escriba al menos dos parámetros"
    exit 1
fi

mensaje_error=$(expr $1 \* $2 2>&1)

if [[ $? -ne 0 ]]; then

    if [[ $1 = $2 ]]; then
        echo "$1 y $2 son iguales"
    else
        if [[ $1 > $2 ]]; then
            echo "$1 es mayor lexicográficamente que $2"
        else
            echo "$1 es menor lexicográficamente que $2"

        fi
    fi
else
    echo "Se esperan parámetros no numéricos"
fi
