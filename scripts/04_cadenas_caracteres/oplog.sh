#!/bin/bash
#Definidas tres variables tipo cadena, compararlas de forma que si var1 es distinta de var2, dará un mensaje indicándolo#
var1="hola"
var2="adios"
var3="que tal"

if [[ $var1 != $var2 && $var1 != $var3 ]]; then
    echo "${var1} es distinto de ${var2} y ${var3}"
fi

