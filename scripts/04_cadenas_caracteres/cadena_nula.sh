#!/bin/bash
# Script que comprueba el funcionamiento de la comparación de cadenas cuando es nula o no

# Declaro la variable H con contenido, no es nula
declare H="hola"
# Si no es nula, ejecuta el if y da el mensaje
if [[ -n $H ]]; then
    echo "La variable dada no es nula y contiene $H"
else
    echo "La variable $H es nula"
fi

# Declaro la variable G, sin contenido, es nula
declare G
# Si es nula, ejecuta el if y da el mensaje
if [[ -z $G ]]; then
    echo "La variable $G es nula"
else
    echo "La variable $G no es nula"
fi
