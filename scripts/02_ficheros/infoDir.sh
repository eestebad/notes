#!/bin/bash
#Fisrt parameter received is a directory path
echo "El script $0 recibe como parámetro el directorio ${1}"
#Show the files that begin with a vocal
vocalfiles=`ls ${1}/[a,e,i,o,u]*`
echo "Los ficheros que empiezan por vocal son $vocalfiles"
#Show the total number of files and directorys in the actual directory
totalnumber=`ls ${1} | wc -l`
echo "El número total de ficheros y directorios en ${1} es $totalnumber"
