#!/bin/bash
#Script que copia un fichero pasado como argumento, a un directorio tb pasado como argumento. Comprueba que ambos existan y que no exista un fichero con el mismo nombre en el directorio#

#Declaración variable
numArg=$#

#Si el nº de argumentos es distinto a dos, muestra mensaje con el uso del script#
if [[ $numArg != 2 ]]; then
    echo $0 "<fichero> <directorio>"
    #Y si no, comprueba si el fichero pasado como argumento1, existe#
else
    if [[ -e $1 ]]; then
        echo "El fichero $1 existe"
        if [[ -f $1 ]] #Comprueba si es un fichero y si lo es, lo indica
        then
            echo "$1 es un fichero"
        else #si no es un fichero, sale
            echo "$1 no es un fichero"
            exit
        fi
    else
        echo "$1 no existe"
    fi
fi

#Comprobaciones sobre el directorio pasado como 2º argumento
if [[ -e $2 ]] #pregunta si existe el directorio pasado y da mensaje
then
    echo "$2 si existe"
    if [[ -d $2 ]] #Comprueba si es un directorio y da el mensaje que corresponda#
    then
        echo "$2 es un directorio"
    else
        echo "$2 no es un directorio"
        exit
    fi
else
    echo "el directorio $2 no existe"
    exit
fi

#Comprobamos que no exista un fichero con el mismo nombre en el directorio
if [[ -a $2/$1 ]]; then
    echo "Existe ya un fichero con nombre $1 en el directorio $2"
else #Si no existe un fichero con ese nombre
    cp $1 $2/. #Copiamos el fichero en el directorio
fi



