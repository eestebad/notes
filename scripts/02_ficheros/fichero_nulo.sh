#!/bin/bash
# Script que comprueba si un fichero pasado por parámetro, existe y no es nulo. Si no lo es muestra su contenido y si lo es, da error

if [[ -s $1 ]]; then
    cat $1
else
    echo "El fichero no existe o no tiene contenido"
fi
