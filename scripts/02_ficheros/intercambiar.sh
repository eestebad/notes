#!/bin/bash
#This script accept two arguments that are files, whose content will be exchanged
echo "Los ficheros de los que intercambiaremos el contenido son ${1} y ${2}"
#Create file3 to save the content of file1
createfile3=`touch file3.txt`
echo "se crea un fichero intermedio $createfile, llamado file3"
#The content of file1.txt is saved in file3.txt
catfile1=`cat ${1} >file3.txt`
echo "Se copia el contenido de file1.txt a file3.txt ${catfile1}"
#The content of file2.txt is saved in file1.txt
catfile2=`cat ${2} >${1}`
echo "Se copia el contenido de file2 a file1 ${catfile2}"
#The content of file3.txt is saved in file2.txt
catfile3=`cat file3.txt >${2}`
echo "Se copia el contenido de file3 a file2"
#file3.txt is removed
rmfile3=`rm file3.txt`
echo "Se elimina el fichero file3.txt"
echo "Se ha intercambiado los contenidos de file1 y file2"
