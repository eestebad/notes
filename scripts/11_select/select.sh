#!/bin/bash

# Mostrar menú a usuario de forma que introduzca dos números y elija si quiere sumar, restar, multiplicar o dividir.

# Almacenamos las opciones del menú
OPCIONES="Sumar Restar Multiplicar Dividir Salir"

# Cambiamos prompt para que sea más descriptivo, cuando muestre las opciones del menú
PS3="Elija una opción: "
echo "Introduzca los dos números para operar: "
# Leemos dos números
read -p "Número 1: " n1
read -p "Número 2: " n2

# Con select, configuramos las acciones del menú:
select opt in $OPCIONES #opt es el nº del menú elegido por usuario.
do
    if [[ $opt = "Sumar" ]]
    then
        echo $((n1 + n2))
    elif [[ $opt = "Restar" ]]
    then
        echo $((n1 - n2))
    elif [[ $opt = "Multiplicar" ]]
    then
        echo $((n1 * n2))
    elif [[ $opt = "Dividir" ]]
    then
        echo $((n1 / n2))
    elif [[ $opt = "Salir" ]]
    then
        echo ---Fin---
        break #Como es un bucle infinito, para salir, utilizamos break.
    else
        echo "Opción errónea"
    fi
done

