#!/bin/bash

#Example: how to use until

i=20

until [[ $i -lt 10 ]]
do
    echo "Contador $i"
    let i=1
done

