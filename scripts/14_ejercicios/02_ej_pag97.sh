#!/bin/bash

#Script copiar. Acepta dos argumentos, que son 2 ficheros existentes. Se copiará el contenido el 1º en el 2º, comprobando: que el nº de argumentos sea correcto, si el primer fichero no existe se dará mensaje de error y se finalizará, si el 2º fichero ya existe, se dará error y se finalizará.#

#Compruebo que el nº de argumentos dados es correcto y si no, da error y sale#
if [[ $# -ne 2 ]]; then
    echo "Nº de argumentos incorrectos. USO: <arg1> <arg2>"
    exit
fi
#Compruebo si el primer fichero existe, si no existe, da error y sale. Si existe, ejecuta el bloque#
if [[ -e $1 ]]; then
    echo "El fichero $1 existe"
    #Comprueba si fichero2 existe y si ya existe, da error y sale
    if [[ -e $2 ]]; then
        echo "El fichero $2 ya existe. Debe usar otro nombre"
        exit
    #Si no existe, lo crea y copia el contenido de fichero1 a fichero2
    else
        touch $2
        cat $1 > $2
        echo "Se ha creado $2 y se ha copiado el contenido de $1"
        echo "Este es ahora el contenido de $2: `cat $2`"
    fi
else
    "El fichero $1 no existe"
    exit
fi



