#!/bin/bash

#Pide un nº por teclado y determina si es positivo, negativo o cero
echo "Introduzca un nº: "
read numero
#Compruebo que el nº de parámetros es correcto
if [[ $# -ne 1 ]]; then
    echo "Nº de argumentos incorrecto. USO: <numero>"
else
    #Compruebo si el nº es + - o 0 e imprimo mensaje
    if [[ $numero -lt 0 ]]; then
        echo "el nº $numero es negativo"
    elif [[ $numero -gt 0 ]]; then
        echo "el nº $numero es positivo"
    else
        echo "el nº $numero es un cero"
        exit
    fi
fi
