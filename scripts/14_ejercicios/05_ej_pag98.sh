#!/bin/bash
# Recibe un nº indeterminado de parámetros y los clasifica según sean ficheros o directorios, guardándolos en dos ficheros: fich.txt y dir.txt#
touch fich.txt dir.txt
if [[ -e $1 ]]; then
    echo "$1 es un fichero y lo guardamos en fich.txt"
    cat $1 > fich.txt
elif [[ -d $1 ]]; then
    echo "$1 es un directorio y lo guardamos en dir.txt"
else
    echo "$1 no es un fichero ni un directorio"
fi
