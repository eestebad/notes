#!/bin/bash
#Get the elements of a variable
#Assign elements to a variable
matriz=(uno dos tres)

#Get the element number 0
echo "El argumento nº 0 de la variable matriz es ${matriz[0]}"
#Get the element number 1
echo "El argumento nº 1 de la variable matriz es ${matriz[1]}"
#Get the element number 2
echo "El argumento nº 2 de la variable matriz es ${matriz[2]}"
