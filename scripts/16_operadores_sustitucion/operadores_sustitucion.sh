#!/bin/bash
#Crear script que imprima las líneas con el nº más alto. Recibe como primer argumento el fichero con los números, como 2º argumento el nº N de líneas que queremos que muestre. Si no se da N, por defecto será 5.

fichero_clientes=$1
fichero_clientes=${fichero_clientes:?'no suministrado'}
cuantos=$2
defecto=5

sort -nr $fichero_clientes | head -${cuantos:=$defecto}

