#!/bin/bash

#Example: how to use while

i=0
while [[ $i -lt 10 ]]
do
    echo "El contador es $i"
    let i=i+1
done
