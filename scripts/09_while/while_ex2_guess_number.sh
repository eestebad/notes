#!/bin/bash

#This bucle will execute while user don't guess de number

MINUM=8
while [[ 1  ]]; do
    echo "Introduzca un número: "
    read USER_NUM

    if [[ $USER_NUM -lt $MINUM ]]; then
        echo "El número introducido es menor que el mío"
        echo " "
      elif [[ $USER_NUM -gt $MINUM ]]; then
          echo "El número introducido es mayor que el mío"
          echo " "
      elif [[ $USER_NUM -eq $MINUM ]]; then
          echo "Acertaste: Mi número era $MINUM"
          break
      fi
  done
  #Exit of the script
  echo "El script salió del bucle. Terminando..."
  exit
