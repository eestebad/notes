#!/bin/bash
# Se solicita un nº del 1 al 5 al usuario
echo Introduce un nº del 1 al 5:
read n
#Comparo si ese nº coincide con alguno de los patrones y si es así, ejecuta el echo correspondiente a ese patrón. Si no es un nº del 1 al 5, muestra mensaje de error y sale.
case $n in
    1 | 5) echo "Uno o Cinco";;
    2) echo "Dos";;
    3) echo "Tres";;
    4) echo "Cuatro";;
    *) echo "Elemento incorrecto";;
esac

