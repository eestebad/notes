#!/bin/bash
# Se solicita un nº del 1 al 5 al usuario
echo Introduce hola adios o bye
read n
#Comparo si ese nº coincide con alguno de los patrones y si es así, ejecuta el echo correspondiente a ese patrón. Si no es un nº del 1 al 5, muestra mensaje de error y sale.
case $n in
    hola) echo "Has escrito hola";;
    adios) echo "Has escrito adios";;
    bye) echo "Has escrito bye";;
    *) echo "Elemento incorrecto";;
esac

